<?php
namespace Lib;

/**
 * Facade for CI validation
 */
class Validate
{
  #Custom validation rules
  #use ValidationRules;

  protected $ValidationRules = null;
  static $CI = null;
  static $Valid = null;
  static $errors = [];
  public static $rules = [];
  static $test = "TEST V";
  public static function init()
  {
    if (self::$CI === null) {
      self::$CI = &get_instance();
      #self::$CI->load->helper('form');
      self::$CI->load->library('form_validation');
      #self::$CI->form_validation->set_message('char_plus', 'Not allowed characters');
    }
  }
  # Validating form with custom rules
  public static function form($rule, $data = null)
  {
    self::init();
    //printr(self::$CI->input->post());
    if ($data) {
      self::$CI->form_validation->set_data($data);
    }
    $rules = array_merge($rule, self::$rules);
		self::$CI->form_validation->set_rules($rules);
		if (self::$CI->form_validation->run() == FALSE)
    {
    	#self::$CI->index();
      #self::$CI->load->view('home');
      #return false;
      //redirect(base_url().'home');
      self::$errors = self::$CI->form_validation->error_array();
      return false;
    }
    else
    {
    	//self::$CI->load->view('home');
      #return true;
      //echo "Validation passed!";
      return true;
    }
  }

  public static function showErrors()
  {
    self::init();
    if (!empty(self::$errors)) {
     echo validation_errors();
    }
  }

  /**
   * Accessing custom validation rules
   * @param string $rule (string,int,url)
   * @param mixed $data
   */
  # Implemented trait class
  /*public static function ValidateRule($rule, $data)
  {
    return ValidationRules::check($rule,$data);
  }*/
  
    public static function appendRules($rules) 
    {
        self::$rules = array_merge($rules, self::$rules);
    }
}



?>
