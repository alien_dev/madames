<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * New Girls
 */
class NewGirls extends MY_Controller
{
	public $tab_title = "New Girls";
	public function __construct()
    {
        parent::__construct();

    }

	public function index($paginate_start=0, $paginate_take=12)
	{
		$this->load->library('pagination');
        $this->load->model('service');
        $this->load->model('region');

        $config['base_url'] = base_url().'NewGirls/index';
        $config['total_rows'] = $this->girl_model->total_girls();
        $config['per_page'] = $paginate_take;
        $config['full_tag_open'] = '<div class="pagination">'; //Wraper oko paginacije
        $config['full_tag_close'] = '</div>';
        $config['attributes'] = array('class' => 'pagination-links');
        $this->pagination->initialize($config);

        $data['services'] = $this->service->all();
        $data['new_girls'] = $this->girl_model->get_new_girls_paginate($config["per_page"], $paginate_start);
        $data['regions'] = $this->region->all();
        $data['pagination_links'] = $this->pagination->create_links();
				$data['lives'] = $this->girl_model->findByAttributes(['chat_status'=>1]);

        $this->render('newgirls/index', $data);
	}
}

/* End of file NewGirls.php */
/* Location: ./application/controllers/NewGirls.php */
