<?php $this->session->set_userdata('referred_from', current_url()); ?>
<div class="row custom_profile_row">
    <div class="col-lg-4 order-lg-1 text-center img_div_pro">
        <img src="<?php echo base_url(); ?><?=$this->club_model->get_club_profile_image($club->slug)?>" class="mx-auto img-fluid img-circle d-block img_profile" alt="avatar">
        <?php if ($this->session->userdata('username')) : ?>
            <form action="<?= base_url(); ?>girls/update_profile_photo" class="profile_photo_edit" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <label class="fileUpload btn btn-primary bio_button filter_btn">
                    <span class="upload_span">Upload your club picture</span>
                    <input type="file" name="userfile" id="fileslika" class="upload upload_profile">
                </label></br>
                <input class="btn btn-primary show_upload_profile" style="margin:10px" type="submit" name="" value="Save">
            </form>
        <?php endif; ?>
        <!-- <div class="row">
                <?php if ($profile_owner) { ?>
                    <div class="profile_description">
                        <textarea class="form-control edit-description" rows="5" data-id="<?=$club->id?>"><?=$club->description?></textarea>
                        <p class="user-description"><?=$club->description?></p>
                        <img class="edit-description-img" src="<?php echo base_url(); ?>assets/images/editRed.png" alt="" style="float:right;width:24px;height:24px;" id="edit_desc">
                    </div>
                <?php } else if (!empty($club->description)){ ?>
                    <div class="profile_description">
                        <textarea class="form-control edit-description" rows="5" data-id="<?=$club->id?>"><?=$club->description?></textarea>
                        <p class="user-description"><?=$club->description?></p>
                    </div>
                <?php } ?>
        </div> -->
    </div>
    <div class="col-lg-8 order-lg-2 table_profile_tab">
        <ul class="nav nav-tabs">
            <li class="nav-item nav_item_profile">
                <a href="" data-target="#profile" data-toggle="tab" class="nav-link active nav_phone">Info</a>
            </li>
        </ul>
        <div class="tab-content py-4">
            <div class="tab-pane active" id="profile">
                <h3 class="mb-3 profile_h3"><?php echo $club->username; ?></h3>
                <table class="table profile_table">
                    <tbody class="profile_table">
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile">City</th>
                            <td><?php echo $club->city; ?></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile">Phone</th>
                            <td><?php echo $club->phone; ?></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile">Web</th>
                            <td><?php echo $club->web; ?></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile">Address</th>
                            <td><?php echo $club->address; ?></td>
                        </tr>
                    </tbody>
                </table>
                <div class="myframegmap_profile">
                      <div style="width: 300px">
                          <iframe width="300" height="150"
                              src="https://maps.google.com/maps?width=720&amp;height=250&amp;hl=en&amp;q=Untervaz,Untergasse,3+(madames)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"
                              frameborder="0"
                              scrolling="no"
                              marginheight="0"
                              marginwidth="0">
                              <a href="https://www.maps.ie/create-google-map/">
                                  Google map generator
                              </a>
                          </iframe>
                      </div>
                  </div>
                <?php if ($profile_owner) : ?>
                    <a class="btn btn-primary bio_button filter_btn" href="<?php echo base_url(); ?>girls/bio/<?php echo $club->slug; ?>">Edit your info <img class="profile_img_edit" src="<?php echo base_url(); ?>assets/images/siva.png" alt=""></a>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>
<script>

var alert = document.querySelector('.alert.alert-success');
if(alert){
  console.log('1');
    setTimeout(() => {
        var msg = $(alert);
        msg.slideToggle("slow", function(){
            msg.find('.close').click();
        });
    }, 3000);
}

    var profile_description = document.getElementsByClassName("profile_description")[0];
    var edit_desc = profile_description.querySelector("#edit_desc");
    var edit_img = profile_description.querySelector(".edit-description-img");
    if(edit_desc){ //Ako je ulogovan
        edit_desc.addEventListener('click', function(){
            var edit_description = document.getElementsByClassName('edit-description')[0];
            var user_description = document.getElementsByClassName('user-description')[0];
            edit_description.style.display = 'block';
            user_description.style.display = 'none';
            edit_img.style.display = 'none';

            edit_description.addEventListener('blur', function (event) {
                var model_id = this.dataset.id;
                AJAX.post({
                    url: base_url+"madames/handleForm/update/girl_model/"+model_id,
                    data: {
                        model_data: {
                            description: this.value
                        }
                    },
                    success: function(response){
                        edit_description.style.display = 'none';
                        user_description.style.display = 'block';
                        edit_img.style.display = 'block';
                        user_description.textContent = edit_description.value;
                    }
                })

            })
        });
    }


</script>

<!-- /.container -->
