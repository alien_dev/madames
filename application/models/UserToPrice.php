<?php 
/**
 * 
 */
class UserToPrice extends MY_Model
{	
	public $table_name = 'users_to_prices';

	public function table($value='')
	{
		$this->db->select(
			$this->table_name.'.id as id, time, price, user_id, time_type_id, user_slug, 
			times.name as time_name, times.id as time_id, times.abbr as time_abbr, 
			currencies.id as currency_id, currencies.name as currency_name, currencies.iso as currency
		');
		$this->db->join('users', 'users.id = users_to_prices.user_id');
		$this->db->join('times', 'times.id = users_to_prices.time_type_id');
		$this->db->join('currencies', 'currencies.id = users_to_prices.currency_id');
		$this->db->from($this->table_name);
		//$query = $this->db->get_where('users_to_prices', array('user_slug' => $slug));
	}

	public function rules()
	{
		return [
			[
                'field' => 'user_id',
                'label' => 'user id',
                'rules' => 'required|numeric',
            ],
            [
                'field' => 'user_slug',
                'label' => 'User slug',
                'rules' => 'required|char_plus|max_length[64]|min_length[4]',
			],
			[
                'field' => 'time',
                'label' => 'User slug',
                'rules' => 'required|numeric',
			],
			[
                'field' => 'time_type_id',
                'label' => 'User slug',
                'rules' => 'required|numeric|max_length[3]',
			],
			[
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'required|numeric',
			],
			[
                'field' => 'currency_id',
                'label' => 'Currency',
                'rules' => 'required|numeric|max_length[3]',
            ],
		];
	}

}


 ?>