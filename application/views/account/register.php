<?php use Lib\Validate; ?>

<div class="tab_container_register">
    <input id="tab1" type="radio" name="tabs" checked>
    <label class="label_tab" for="tab1" style="border-top-left-radius:6px;"><img class="price_girl_pic" src="<?php echo base_url(); ?>assets/images/girl.png" alt=""><span>Girls</span></label>

    <input id="tab2" type="radio" name="tabs">
    <label class="label_tab" for="tab2" style="border-top-right-radius:6px;"><img class="price_girl_pic" src="<?php echo base_url(); ?>assets/images/glass.png" alt=""><span>Clubs</span></label>

    <section id="content1" class="tab-content">
        <?php #echo validation_errors(); ?>
        <div class="login-form register_form">
            <?php
            echo form_open_multipart('account/store_user');
            ?>
            <input type="hidden" class="form-control" name="group_id" value="2" >
            <h2 class="text-center">Sign up</h2>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Username" name="username" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="password" class="form-control" placeholder="Confirm password" name="password_confirm" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Phone" name="phone" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Email" name="email" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Age" name="age" >
                </div>
            </div>
            <div class="form-group">
                <label for="ts">TS:</label>
                <select class="form-control" id="sel2"  name="ts">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </div>
            <div class="form-group">
                <label for="region_id">Regions:</label>
                <select class="form-control" id="sel1"  name="region_id">
                    <?php foreach ($regions as $region) : ?>
                        <option value="<?php echo $region->id; ?>"><?php echo $region->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Select Profile Image</div>
                <div class="panel-body" align="center">
                    <input type="file" name="userfile" id="profile_picture" accept="image/*" size="20" />
                    <!-- <input type="hidden" name="cropped_b64" id="cropped_image"> -->
                    <input type="hidden" name="cropped_blob" id="cropped_image">
                    <br />
                    <div id="uploaded_image"></div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block aa bor_btn">Sign up</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </section>

    <section id="content2" class="tab-content">
        <?php echo validation_errors(); ?>
        <div class="login-form register_form">
            <?php
            echo form_open_multipart('account/store_user');
            ?>
            <input type="hidden" class="form-control" name="group_id" value="3" >
            <h2 class="text-center">Sign up</h2>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Club Name" name="username" required="required">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" required="required">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="password" class="form-control" placeholder="Confirm password" name="password_confirm" required="required">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Club name" name="club_name" required="required">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Phone" name="phone" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="type" class="form-control" placeholder="Address" name="address" required="required">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Email" name="email" required="required">
                </div>
            </div>
            <div class="form-group">
                <label for="region_id">Regions:</label>
                <select class="form-control" id="sel1"  name="region_id">
                    <?php foreach ($regions as $region) : ?>
                        <option value="<?php echo $region->id; ?>"><?php echo $region->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Select Profile Image</div>
                <div class="panel-body" align="center">
                    <input type="file" name="userfile" id="club_picture" accept="image/*" size="20" />
                    <!-- <input type="hidden" name="cropped_b64" id="cropped_image_two"> -->
                    <br />
                    <div id="uploaded_image"></div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block aa bor_btn">Sign up</button>
            </div>
            <?php echo form_close(); ?>
    </section>
</div>

<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10 text-center">
                        <div id="image_cropper" style="width:350px; margin-top:30px ; height:400px"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-crop" style="float:left;"><button type="button" class="btn btn-default" id="crop_close_btn" style="border-radius:rpx;" data-dismiss="modal">Close</button></div>
                <button class="btn btn-success crop_image" id="crop_img_btn">Crop</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/croppie.js"></script>
<script src="<?=base_url()?>assets/js/exif.js"></script>
<script>
var input_file = document.getElementById("profile_picture");
var button_crop = document.getElementById("crop_img_btn");
var button_close = document.getElementById("crop_close_btn");
var image_cropper = document.getElementById("image_cropper");
var crop = new Croppie(image_cropper, {
    enableExif: true,
    viewport: {
      width:350,
      height:450,
      type:'square' //circle
    },
    boundary:{
      width:600,
      height:450
    }
});

input_file.onchange = function(){
    var reader = new FileReader();
    reader.onload = function (event) {
        crop.bind({
        url: event.target.result
    }).then(function(){
        console.log('jQuery bind complete');
    });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').show();
}

button_crop.onclick = function(event){
    crop.result({
        //type: 'canvas', //retruns b64
        type: 'blob',
        size: 'viewport',
        format: 'png',
        quality: 1
    }).then(function(response){
        //$("#cropped_image").val(response);
        console.log(response);
        var fd = new FormData();
        fd.append('fname', 'test.txt');
        fd.append('cropped_image', response);
        $.ajax({
            type: 'POST',
            url: '<?=base_url()?>account/storeTempImg',
            data: fd,
            processData: false,
            contentType: false
        }).done(function(data) {
            console.log(data);
        });
        $('#uploadimageModal').hide();
    })
}
button_close.onclick = function(e){
    $("#uploadimageModal").hide();
}


// var input_file = document.getElementById("club_picture");
// var button_crop = document.getElementById("crop_img_btn");
// var button_close = document.getElementById("crop_close_btn");
// var image_cropper = document.getElementById("image_cropper");
// input_file.onchange = function(){
//     var reader = new FileReader();
//     reader.onload = function (event) {
//         crop.bind({
//         url: event.target.result
//     }).then(function(){
//         console.log('jQuery bind complete');
//     });
//     }
//     reader.readAsDataURL(this.files[0]);
//     $('#uploadimageModal').show();
// }
//
// button_crop.onclick = function(event){
//     crop.result({
//         type: 'canvas',
//         size: 'viewport',
//         format: 'jpg'
//     }).then(function(response){
//         $("#cropped_image_two").val(response);
//         $('#uploadimageModal').hide();
//     })
// }
// button_close.onclick = function(e){
//     $("#uploadimageModal").hide();
// }
if(window.location.href == "https://www.madames.ch/account/register/1"){
  $('#tab1').prop("checked", true);
  $('#tab2').prop("checked", false);
}

if(window.location.href == "https://www.madames.ch/account/register/2"){
  $('#tab1').prop("checked", false);
  $('#tab2').prop("checked", true);
}

</script>
