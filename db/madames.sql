-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 12, 2019 at 06:35 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


--
-- Database: `madames`
--

-- --------------------------------------------------------

--
-- Table structure for table `clubs`
--

DROP TABLE IF EXISTS `clubs`;
CREATE TABLE IF NOT EXISTS `clubs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `region_id` int(7) NOT NULL,
  `address` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `club_photo` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(124) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clubs`
--

INSERT INTO `clubs` (`id`, `username`, `password`, `region_id`, `address`, `club_photo`, `slug`, `web`, `phone`, `email`, `ip_address`, `created_on`, `active`) VALUES
(1, 'Light', '1234', 3, 'Street 5', '', 'Light', NULL, '51', '', '', 0, 1),
(2, 'Pleasure', '1234', 1, 'Street 7', '', 'Pleasure', NULL, '12', '', '', 0, 0),
(3, 'Javier', '', 5, 'Street 67', '', 'Javier', NULL, '11111', '', '', 0, 0),
(4, 'Karin', '1234', 3, 'dasda 1', 'Karin2121312312111.jpg', 'Karin2121312312111', NULL, '2121312312111', 'zoran.djuro@hotmail.com', '', 0, 0),
(5, 'Haos', '1234', 3, 'dasda 1', 'Haos1111.jpg', 'Haos1111', NULL, '1111', 'milos.avram11@hotmail.com', '', 0, 0),
(6, 'Jaala', '$2y$08$H463UT1Uy7gAxm1vbGEPYOUzPfvUZL99w1pmBD/b4eWi2zc3Llgoi', 3, 'dasda 1', 'Jaala12345.jpg', 'Jaala12345', NULL, '12345', 'necaaaaa82bg@hotmail.com', '::1', 1552343594, 1),
(7, 'Jovon', '$2y$08$nWHWJzytv9oyH/9y6oljeO9J4etf6X4/Y4PF4pjmpx7I3Q8S4atrG', 3, 'dasda 1', 'Jovon381604944911.jpg', 'Jovon381604944911', NULL, '381604944911', 'shkene.aabre11@gmail.com', '::1', 1552344565, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `username_id` int(11) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `counter_working_hours`
--

DROP TABLE IF EXISTS `counter_working_hours`;
CREATE TABLE IF NOT EXISTS `counter_working_hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hours` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `counter_working_hours`
--

INSERT INTO `counter_working_hours` (`id`, `hours`) VALUES
(2, '00'),
(3, '01'),
(4, '02'),
(5, '03'),
(6, '04'),
(7, '05'),
(8, '06'),
(9, '07'),
(10, '08'),
(11, '09'),
(12, '10'),
(13, '11'),
(14, '12'),
(15, '13'),
(16, '14'),
(17, '15'),
(18, '16'),
(19, '17'),
(20, '18'),
(21, '19'),
(22, '20'),
(23, '21'),
(24, '22'),
(25, '23'),
(26, '24');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE IF NOT EXISTS `currencies` (
  `iso` char(3) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `visible` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`iso`, `visible`, `sort`, `name`, `id`) VALUES
('KRW', 0, 0, '(South) Korean Won', 1),
('AFA', 0, 0, 'Afghanistan Afghani', 2),
('ALL', 0, 0, 'Albanian Lek', 3),
('DZD', 0, 0, 'Algerian Dinar', 4),
('ADP', 0, 0, 'Andorran Peseta', 5),
('AOK', 0, 0, 'Angolan Kwanza', 6),
('ARS', 0, 0, 'Argentine Peso', 7),
('AMD', 0, 0, 'Armenian Dram', 8),
('AWG', 0, 0, 'Aruban Florin', 9),
('AUD', 0, 0, 'Australian Dollar', 10),
('BSD', 0, 0, 'Bahamian Dollar', 11),
('BHD', 0, 0, 'Bahraini Dinar', 12),
('BDT', 0, 0, 'Bangladeshi Taka', 13),
('BBD', 0, 0, 'Barbados Dollar', 14),
('BZD', 0, 0, 'Belize Dollar', 15),
('BMD', 0, 0, 'Bermudian Dollar', 16),
('BTN', 0, 0, 'Bhutan Ngultrum', 17),
('BOB', 0, 0, 'Bolivian Boliviano', 18),
('BWP', 0, 0, 'Botswanian Pula', 19),
('BRL', 0, 0, 'Brazilian Real', 20),
('GBP', 1, 4, 'British Pound', 21),
('BND', 0, 0, 'Brunei Dollar', 22),
('BGN', 0, 0, 'Bulgarian Lev', 23),
('BUK', 0, 0, 'Burma Kyat', 24),
('BIF', 0, 0, 'Burundi Franc', 25),
('CAD', 0, 0, 'Canadian Dollar', 26),
('CVE', 0, 0, 'Cape Verde Escudo', 27),
('KYD', 0, 0, 'Cayman Islands Dollar', 28),
('CLP', 0, 0, 'Chilean Peso', 29),
('CLF', 0, 0, 'Chilean Unidades de Fomento', 30),
('COP', 0, 0, 'Colombian Peso', 31),
('XOF', 0, 0, 'Communauté Financière Africaine BCEAO - Francs', 32),
('XAF', 0, 0, 'Communauté Financière Africaine BEAC, Francs', 33),
('KMF', 0, 0, 'Comoros Franc', 34),
('XPF', 0, 0, 'Comptoirs Français du Pacifique Francs', 35),
('CRC', 0, 0, 'Costa Rican Colon', 36),
('CUP', 0, 0, 'Cuban Peso', 37),
('CYP', 0, 0, 'Cyprus Pound', 38),
('CZK', 0, 0, 'Czech Republic Koruna', 39),
('DKK', 0, 0, 'Danish Krone', 40),
('YDD', 0, 0, 'Democratic Yemeni Dinar', 41),
('DOP', 0, 0, 'Dominican Peso', 42),
('XCD', 0, 0, 'East Caribbean Dollar', 43),
('TPE', 0, 0, 'East Timor Escudo', 44),
('ECS', 0, 0, 'Ecuador Sucre', 45),
('EGP', 0, 0, 'Egyptian Pound', 46),
('SVC', 0, 0, 'El Salvador Colon', 47),
('EEK', 0, 0, 'Estonian Kroon (EEK)', 48),
('ETB', 0, 0, 'Ethiopian Birr', 49),
('EUR', 1, 2, 'Euro', 50),
('FKP', 0, 0, 'Falkland Islands Pound', 51),
('FJD', 0, 0, 'Fiji Dollar', 52),
('GMD', 0, 0, 'Gambian Dalasi', 53),
('GHC', 0, 0, 'Ghanaian Cedi', 54),
('GIP', 0, 0, 'Gibraltar Pound', 55),
('XAU', 0, 0, 'Gold, Ounces', 56),
('GTQ', 0, 0, 'Guatemalan Quetzal', 57),
('GNF', 0, 0, 'Guinea Franc', 58),
('GWP', 0, 0, 'Guinea-Bissau Peso', 59),
('GYD', 0, 0, 'Guyanan Dollar', 60),
('HTG', 0, 0, 'Haitian Gourde', 61),
('HNL', 0, 0, 'Honduran Lempira', 62),
('HKD', 0, 0, 'Hong Kong Dollar', 63),
('HUF', 0, 0, 'Hungarian Forint', 64),
('INR', 0, 0, 'Indian Rupee', 65),
('IDR', 0, 0, 'Indonesian Rupiah', 66),
('XDR', 0, 0, 'International Monetary Fund (IMF) Special Drawing Rights', 67),
('IRR', 0, 0, 'Iranian Rial', 68),
('IQD', 0, 0, 'Iraqi Dinar', 69),
('IEP', 0, 0, 'Irish Punt', 70),
('ILS', 0, 0, 'Israeli Shekel', 71),
('JMD', 0, 0, 'Jamaican Dollar', 72),
('JPY', 0, 0, 'Japanese Yen', 73),
('JOD', 0, 0, 'Jordanian Dinar', 74),
('KHR', 0, 0, 'Kampuchean (Cambodian) Riel', 75),
('KES', 0, 0, 'Kenyan Schilling', 76),
('KWD', 0, 0, 'Kuwaiti Dinar', 77),
('LAK', 0, 0, 'Lao Kip', 78),
('LBP', 0, 0, 'Lebanese Pound', 79),
('LSL', 0, 0, 'Lesotho Loti', 80),
('LRD', 0, 0, 'Liberian Dollar', 81),
('LYD', 0, 0, 'Libyan Dinar', 82),
('MOP', 0, 0, 'Macau Pataca', 83),
('MGF', 0, 0, 'Malagasy Franc', 84),
('MWK', 0, 0, 'Malawi Kwacha', 85),
('MYR', 0, 0, 'Malaysian Ringgit', 86),
('MVR', 0, 0, 'Maldive Rufiyaa', 87),
('MTL', 0, 0, 'Maltese Lira', 88),
('MRO', 0, 0, 'Mauritanian Ouguiya', 89),
('MUR', 0, 0, 'Mauritius Rupee', 90),
('MXP', 0, 0, 'Mexican Peso', 91),
('MNT', 0, 0, 'Mongolian Tugrik', 92),
('MAD', 0, 0, 'Moroccan Dirham', 93),
('MZM', 0, 0, 'Mozambique Metical', 94),
('NAD', 0, 0, 'Namibian Dollar', 95),
('NPR', 0, 0, 'Nepalese Rupee', 96),
('ANG', 0, 0, 'Netherlands Antillian Guilder', 97),
('YUD', 0, 0, 'New Yugoslavia Dinar', 98),
('NZD', 0, 0, 'New Zealand Dollar', 99),
('NIO', 0, 0, 'Nicaraguan Cordoba', 100),
('NGN', 0, 0, 'Nigerian Naira', 101),
('KPW', 0, 0, 'North Korean Won', 102),
('NOK', 0, 0, 'Norwegian Kroner', 103),
('OMR', 0, 0, 'Omani Rial', 104),
('PKR', 0, 0, 'Pakistan Rupee', 105),
('XPD', 0, 0, 'Palladium Ounces', 106),
('PAB', 0, 0, 'Panamanian Balboa', 107),
('PGK', 0, 0, 'Papua New Guinea Kina', 108),
('PYG', 0, 0, 'Paraguay Guarani', 109),
('PEN', 0, 0, 'Peruvian Nuevo Sol', 110),
('PHP', 0, 0, 'Philippine Peso', 111),
('XPT', 0, 0, 'Platinum, Ounces', 112),
('PLN', 0, 0, 'Polish Zloty', 113),
('QAR', 0, 0, 'Qatari Rial', 114),
('RON', 0, 0, 'Romanian Leu', 115),
('RUB', 0, 0, 'Russian Ruble', 116),
('RWF', 0, 0, 'Rwanda Franc', 117),
('WST', 0, 0, 'Samoan Tala', 118),
('STD', 0, 0, 'Sao Tome and Principe Dobra', 119),
('SAR', 0, 0, 'Saudi Arabian Riyal', 120),
('SCR', 0, 0, 'Seychelles Rupee', 121),
('SLL', 0, 0, 'Sierra Leone Leone', 122),
('XAG', 0, 0, 'Silver, Ounces', 123),
('SGD', 0, 0, 'Singapore Dollar', 124),
('SKK', 0, 0, 'Slovak Koruna', 125),
('SBD', 0, 0, 'Solomon Islands Dollar', 126),
('SOS', 0, 0, 'Somali Schilling', 127),
('ZAR', 0, 0, 'South African Rand', 128),
('LKR', 0, 0, 'Sri Lanka Rupee', 129),
('SHP', 0, 0, 'St. Helena Pound', 130),
('SDP', 0, 0, 'Sudanese Pound', 131),
('SRG', 0, 0, 'Suriname Guilder', 132),
('SZL', 0, 0, 'Swaziland Lilangeni', 133),
('SEK', 0, 0, 'Swedish Krona', 134),
('CHF', 1, 1, 'Swiss Franc', 135),
('SYP', 0, 0, 'Syrian Potmd', 136),
('TWD', 0, 0, 'Taiwan Dollar', 137),
('TZS', 0, 0, 'Tanzanian Schilling', 138),
('THB', 0, 0, 'Thai Baht', 139),
('TOP', 0, 0, 'Tongan Paanga', 140),
('TTD', 0, 0, 'Trinidad and Tobago Dollar', 141),
('TND', 0, 0, 'Tunisian Dinar', 142),
('TRY', 0, 0, 'Turkish Lira', 143),
('UGX', 0, 0, 'Uganda Shilling', 144),
('AED', 0, 0, 'United Arab Emirates Dirham', 145),
('UYU', 0, 0, 'Uruguayan Peso', 146),
('USD', 1, 3, 'US Dollar', 147),
('VUV', 0, 0, 'Vanuatu Vatu', 148),
('VEF', 0, 0, 'Venezualan Bolivar', 149),
('VND', 0, 0, 'Vietnamese Dong', 150),
('YER', 0, 0, 'Yemeni Rial', 151),
('CNY', 0, 0, 'Yuan (Chinese) Renminbi', 152),
('ZRZ', 0, 0, 'Zaire Zaire', 153),
('ZMK', 0, 0, 'Zambian Kwacha', 154),
('ZWD', 0, 0, 'Zimbabwe Dollar', 155);

-- --------------------------------------------------------

--
-- Table structure for table `dropdown_options`
--

DROP TABLE IF EXISTS `dropdown_options`;
CREATE TABLE IF NOT EXISTS `dropdown_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `hairs`
--

DROP TABLE IF EXISTS `hairs`;
CREATE TABLE IF NOT EXISTS `hairs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hairs`
--

INSERT INTO `hairs` (`id`, `name`) VALUES
(1, 'Blonde'),
(2, 'Brunete'),
(3, 'Red'),
(4, 'Short'),
(5, 'Dark');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(3, '::1', '2121312312111', 1552337093),
(4, '::1', '2121312312111', 1552341220),
(5, '::1', '2121312312111', 1552341631),
(6, '::1', '2121312312111', 1552341660),
(7, '::1', '1111', 1552341824),
(8, '::1', '1111', 1552341960),
(9, '::1', '1111', 1552341969),
(10, '::1', '11111', 1552342017),
(11, '::1', '11111', 1552342101),
(12, '::1', '11111', 1552342117),
(13, '::1', '51', 1552342443),
(14, '::1', '51', 1552342483),
(15, '::1', '12345', 1552343704),
(16, '::1', '12345', 1552343832),
(17, '::1', '12345', 1552344208),
(18, '::1', '381604944911', 1552344647),
(19, '::1', '381604944911', 1552344750);

-- --------------------------------------------------------

--
-- Table structure for table `madames`
--

DROP TABLE IF EXISTS `madames`;
CREATE TABLE IF NOT EXISTS `madames` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `age` int(11) NOT NULL,
  `height` int(6) NOT NULL,
  `weight` int(6) NOT NULL,
  `hair_id` int(2) DEFAULT NULL,
  `type_id` int(3) DEFAULT NULL,
  `TS` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_id` int(4) NOT NULL,
  `contact` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1028) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `profile_photo` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(64) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `club` varchar(64) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `chat_status` int(11) NOT NULL,
  `weekend_id` int(11) NOT NULL,
  `monfri_id` int(11) NOT NULL,
  `orientation_id` int(11) NOT NULL,
  `servicefor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orientation`
--

DROP TABLE IF EXISTS `orientation`;
CREATE TABLE IF NOT EXISTS `orientation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orientation`
--

INSERT INTO `orientation` (`id`, `name`) VALUES
(1, 'Straight'),
(2, 'Gay'),
(3, 'Bisexual');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`) VALUES
(1, 'Zurich - City'),
(2, 'Zurich - Umgebung'),
(3, 'Basel und Umgebung'),
(4, 'Mittelland - AARGAU'),
(5, 'Mittelland - SOLOTHURN'),
(6, 'W\'thur -Thurgau-St.Gallen'),
(7, 'Bern - City'),
(8, 'Bern - Umgeb - Oberland'),
(9, 'Luzern - Innerschweiz'),
(10, 'Graubunden - Glarus'),
(11, 'Wallis'),
(12, 'Biel / Bienne - Grenchen');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`) VALUES
(1, '69 position'),
(2, 'Anal'),
(3, 'Blowjob'),
(4, 'Threesome'),
(5, 'Toys'),
(6, 'Cum in mouth'),
(7, 'Cum on face'),
(8, 'French Kissing'),
(9, 'Gf Experience (GFE)'),
(10, 'Kissing'),
(11, 'Blowjob w/out Condom'),
(12, 'Cumshot on body'),
(13, 'Erotic massage'),
(14, 'Intimate massage'),
(15, 'Handjob'),
(16, 'Fisting'),
(17, 'Deepthroat'),
(18, 'Double penetration'),
(19, 'Facesitting'),
(20, 'Gangbang'),
(21, 'Masturbate'),
(22, 'Titjob'),
(23, 'Rimming (give)'),
(24, 'Rimming (receive)');

-- --------------------------------------------------------

--
-- Table structure for table `servicesfor`
--

DROP TABLE IF EXISTS `servicesfor`;
CREATE TABLE IF NOT EXISTS `servicesfor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `servicesfor`
--

INSERT INTO `servicesfor` (`id`, `name`) VALUES
(1, 'Man'),
(2, 'Couple'),
(3, 'Girl'),
(4, 'Orgy');

-- --------------------------------------------------------

--
-- Table structure for table `times`
--

DROP TABLE IF EXISTS `times`;
CREATE TABLE IF NOT EXISTS `times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `abbr` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `times`
--

INSERT INTO `times` (`id`, `name`, `abbr`) VALUES
(1, 'Minutes', 'min'),
(2, 'Hours', 'h'),
(9, 'Days', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`) VALUES
(1, 'Latina'),
(2, 'Milf'),
(3, 'Asian'),
(4, 'European'),
(5, 'Ebony'),
(6, 'Teen');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `age` int(11) NOT NULL,
  `height` int(6) NOT NULL,
  `weight` int(6) NOT NULL,
  `hair_id` int(2) DEFAULT NULL,
  `type_id` int(3) DEFAULT NULL,
  `TS` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_id` int(4) NOT NULL,
  `contact` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1028) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `profile_photo` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(64) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `club` varchar(64) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `chat_status` int(11) NOT NULL,
  `weekend_id` int(11) NOT NULL,
  `monfri_id` int(11) NOT NULL,
  `orientation_id` int(11) NOT NULL,
  `servicefor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `age`, `height`, `weight`, `hair_id`, `type_id`, `TS`, `region_id`, `contact`, `description`, `profile_photo`, `status`, `slug`, `web`, `address`, `club`, `updated_at`, `chat_status`, `weekend_id`, `monfri_id`, `orientation_id`, `servicefor_id`) VALUES
(26, '::1', 'Milica', '$2y$08$4F/Y5Dtzo/D7hyFVmpQKpO3KjkLKaAlkOGYW1t3/vN5yYwAn3Llm2', NULL, 'boza.jankovic@hotmail.com', NULL, NULL, NULL, NULL, 1549221756, 1551999663, 1, NULL, NULL, NULL, '381604944977', 19, 0, 0, NULL, NULL, '1', 3, '', '', 'Milica381604944977.jpg', 0, 'Milica381604944977', '', 'addres', '', '2019-03-07 23:02:54', 1, 0, 0, 0, 0),
(27, '::1', 'Nemanja', '$2y$08$qJEsng8EkG/uV8adWbWeqeOXpgBZR.YxDDmuPbi5Xyzhoq4c3DDru', NULL, 'neca82bg@hotmail.com', NULL, NULL, NULL, NULL, 1551306722, 1552211630, 1, NULL, NULL, NULL, '3816049449', 18, 0, 0, NULL, NULL, '0', 3, '', 'adsadsa\n\ndasdasdsadasdasdas das da dsad aaaa                                                      asdad\n\n\n\n\n1111\n\n\n\nccc\n\n222\n\n\nasdadas\n45465456465\n\n\n77', 'Nemanja3816049449.jpg', 1, 'Nemanja3816049449', NULL, NULL, '', '2019-03-10 00:55:36', 1, 0, 0, 0, 0),
(28, '::1', 'shkene09', '$2y$08$yePKBu.rkq40tbw.aSmDpOInVha6WX4kKihcL9UE.pq3GbtNI2P/2', NULL, 'zoran.djuro@hotmail.com', NULL, NULL, NULL, NULL, 1551729817, 1552337075, 1, NULL, NULL, NULL, '21213123121231', 24, 0, 0, NULL, NULL, '1', 7, '', '', 'shkene0921213123121231.jpg', 0, 'shkene0921213123121231', NULL, NULL, '', '2019-03-11 20:44:39', 0, 0, 0, 0, 0),
(29, '::1', 'AAAAA', '$2y$08$gYAM3W2vsh/uwpgAJr8Y4uP8vyzotj59HfyD/W8XRpMmaFH8eFKoi', NULL, 'shkene.bre11@gmail.com', NULL, NULL, NULL, NULL, 1551730063, NULL, 1, NULL, NULL, NULL, '121314214121213', 21, 0, 0, NULL, NULL, '0', 3, '', '', 'AAAAA121314214121213.jpg', 0, 'AAAAA121314214121213', NULL, NULL, '', '2019-03-04 21:07:43', 0, 0, 0, 0, 0),
(30, '::1', 'BBBB', '$2y$08$NfaUcHZJd39QzM7H7A9VXOpnhriiIIl.uRwf4izLWXwxDYqEcR2WG', NULL, 'milos.avram11@hotmail.com', NULL, NULL, NULL, NULL, 1551730103, NULL, 1, NULL, NULL, NULL, '12131', 22, 0, 0, NULL, NULL, '1', 3, '', '', 'BBBB12131.jpg', 0, 'BBBB12131', NULL, NULL, '', '2019-03-04 21:08:23', 0, 0, 0, 0, 0),
(31, '::1', 'dasdadsadsa', '$2y$08$RLRr5bKox9UMVvVm.g0JguKYUdzvfBoV5HYM0pqYY2b148RKVkIVq', NULL, 'dasdad@gmail.com', NULL, NULL, NULL, NULL, 1551730202, NULL, 1, NULL, NULL, NULL, '12131421412111', 23, 0, 0, NULL, NULL, '0', 4, '', '', 'dasdadsadsa12131421412111.jpg', 0, 'dasdadsadsa12131421412111', NULL, NULL, '', '2019-03-04 21:10:02', 0, 0, 0, 0, 0),
(33, '::1', 'JAAAALA', '$2y$08$5zUiKqyrNBjNb44YY7i1TOWNAEkj0pYcGXXSrY1ABEas23ww8Bko2', NULL, 'shkene_bag_borcha@hotmail.com', NULL, NULL, NULL, NULL, 1552344353, NULL, 1, NULL, NULL, NULL, '123456', 0, 0, 0, NULL, NULL, NULL, 3, '', '', '', 0, 'JAAAALA123456', NULL, 'dasda 1', '', '2019-03-11 23:45:53', 0, 0, 0, 0, 0),
(34, '::1', 'Jovan', '$2y$08$h1QZRSt2A1Aev/JvaEhD8.awNSIMpVgoGcNJLOmPzLrPDr1mbiL1W', NULL, 'milos.avramaaa11@hotmail.com', NULL, NULL, NULL, NULL, 1552344408, NULL, 1, NULL, NULL, NULL, '381613124141', 0, 0, 0, NULL, NULL, NULL, 3, '', '', '', 0, 'Jovan381613124141', NULL, 'dasda 1', '', '2019-03-11 23:46:48', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `usersb`
--

DROP TABLE IF EXISTS `usersb`;
CREATE TABLE IF NOT EXISTS `usersb` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `hair` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `height` int(6) NOT NULL,
  `weight` int(6) NOT NULL,
  `region_id` int(4) NOT NULL,
  `contact` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `profile_photo` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usersb`
--

INSERT INTO `usersb` (`user_id`, `email`, `password`, `username`, `age`, `hair`, `type`, `height`, `weight`, `region_id`, `contact`, `description`, `profile_photo`, `status`, `register_date`, `slug`) VALUES
(1, '', '', 'Nicky', 1, NULL, NULL, 169, 58, 3, '013645123', 'Nasty Girl', 'user2.jpg', 1, '2018-06-09 12:55:59', 'Nicky'),
(2, '', '', 'Maria', 2, NULL, NULL, 154, 54, 5, '013465897', 'Blonde girl', 'user1.jpg', 1, '2018-06-09 12:55:59', 'Maria'),
(4, 'dasda@gmail.com', '1122', 'Violeta', 4, NULL, NULL, 167, 0, 3, '011789131', '', '', 1, '2018-06-11 14:44:37', 'Violeta'),
(8, 'dasdcxa@gmail.com', '123456', 'Sara', 24, NULL, NULL, 154, 55, 3, '014123456', '', '', 0, '2018-07-17 15:10:46', 'Sara'),
(10, 'dasdsadasd@gmail.com', '1234', 'Joy', 21, NULL, NULL, 157, 156, 3, '', '', '', 0, '2018-08-04 10:38:00', 'Joy'),
(34, 'jiio', '1234', 'Jojo', 0, NULL, NULL, 0, 0, 3, '', '', '15079062_1813759288897975_2745948972425941442_n.jpg', 0, '2018-08-10 15:03:40', 'Jojo'),
(37, 'dasacx@gmail.com', '1234', 'Kya', 37, NULL, NULL, 167, 47, 3, '', '', 'girl.jpg', 0, '2018-08-12 03:10:34', 'Kya'),
(38, 'daca@gmail.com', '1234', 'Baty', 0, NULL, NULL, 0, 0, 9, '', '', 'noimage.jpg', 0, '2018-09-06 06:43:02', 'Baty'),
(39, 'xassxa@dsasa.com', '1234', 'Gray', 39, NULL, NULL, 0, 0, 6, '', '', 'girl.jpg', 0, '2018-09-06 06:46:34', 'Gray'),
(40, 'rtstws', '1234', 'admin user', 40, NULL, NULL, 0, 0, 3, '', '', 'url.jpg', 0, '2018-09-17 10:01:12', 'admin user'),
(41, 'kg', '1234', 'admin usern', 0, NULL, NULL, 0, 0, 3, '', '', 'girl.jpg', 0, '2018-09-17 10:02:28', 'admin-usern'),
(42, 'dasd@dsadsa.dsa', '112233', 'dasd sd', 0, NULL, NULL, 0, 0, 3, '', '', 'girl.jpg', 0, '2018-09-17 10:03:54', 'dasd-sd'),
(43, 'hhgui', '1234', 'aaa', 0, NULL, NULL, 0, 0, 3, '', '', 'girl.jpg', 0, '2018-09-17 10:05:07', 'aaa'),
(44, 'ddas@das.cd', '1122', 'devojka d', 44, NULL, NULL, 0, 0, 3, '', '', '5a3af8b4a3b5f_1.jpg', 0, '2018-09-17 10:13:53', 'devojka d');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(51, 6, 2),
(54, 7, 2),
(29, 11, 2),
(30, 12, 2),
(31, 13, 2),
(32, 14, 2),
(33, 15, 2),
(34, 16, 2),
(35, 17, 2),
(36, 18, 2),
(37, 19, 2),
(38, 20, 2),
(39, 21, 2),
(40, 22, 2),
(41, 23, 2),
(42, 24, 2),
(43, 25, 2),
(44, 26, 2),
(45, 27, 2),
(46, 28, 2),
(47, 29, 2),
(48, 30, 2),
(49, 31, 2),
(50, 32, 2),
(52, 33, 2),
(53, 34, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_to_prices`
--

DROP TABLE IF EXISTS `users_to_prices`;
CREATE TABLE IF NOT EXISTS `users_to_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time_type_id` int(11) NOT NULL,
  `user_slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `currency_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_to_prices`
--

INSERT INTO `users_to_prices` (`id`, `time`, `price`, `user_id`, `time_type_id`, `user_slug`, `currency_id`) VALUES
(1, 1, 100, 12, 2, 'Madam3', 135),
(3, 30, 150, 113, 1, 'Acai', 135),
(12, 1, 100, 10, 2, 'Madam1', 135),
(13, 30, 50, 10, 1, 'Madam1', 135),
(15, 2, 200, 12, 2, 'Madam3', 135),
(16, 3, 300, 12, 2, 'Madam3', 135),
(17, 15, 150, 27, 1, 'Nemanja3816049449', 135),
(18, 15, 44, 27, 1, 'Nemanja3816049449', 135),
(19, 25, 44, 27, 1, 'Nemanja3816049449', 135),
(20, 11, 11, 27, 1, 'Nemanja3816049449', 135),
(22, 12, 111, 27, 1, 'Nemanja3816049449', 135),
(23, 1, 1, 27, 1, 'Nemanja3816049449', 135);

-- --------------------------------------------------------

--
-- Table structure for table `users_to_services`
--

DROP TABLE IF EXISTS `users_to_services`;
CREATE TABLE IF NOT EXISTS `users_to_services` (
  `user_to_services_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `user_slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`user_to_services_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_to_services`
--

INSERT INTO `users_to_services` (`user_to_services_id`, `user_id`, `service_id`, `user_slug`, `status`) VALUES
(7, 13, 1, 'nnnnnnnnn2121312312222', 0),
(8, 13, 2, 'nnnnnnnnn2121312312222', 0),
(9, 26, 1, 'Milica381604944977', 0),
(10, 27, 1, 'Nemanja3816049449', 0),
(11, 27, 2, 'Nemanja3816049449', 0),
(12, 27, 3, 'Nemanja3816049449', 0),
(13, 27, 4, 'Nemanja3816049449', 0),
(14, 27, 5, 'Nemanja3816049449', 0),
(15, 27, 6, 'Nemanja3816049449', 0),
(16, 27, 7, 'Nemanja3816049449', 0),
(17, 27, 8, 'Nemanja3816049449', 0),
(18, 27, 9, 'Nemanja3816049449', 0),
(19, 27, 10, 'Nemanja3816049449', 0),
(20, 27, 11, 'Nemanja3816049449', 0),
(21, 27, 12, 'Nemanja3816049449', 0),
(22, 27, 13, 'Nemanja3816049449', 0),
(23, 27, 14, 'Nemanja3816049449', 0),
(24, 27, 15, 'Nemanja3816049449', 0),
(25, 27, 16, 'Nemanja3816049449', 0),
(26, 27, 17, 'Nemanja3816049449', 0),
(27, 27, 18, 'Nemanja3816049449', 0),
(28, 27, 19, 'Nemanja3816049449', 0),
(29, 27, 20, 'Nemanja3816049449', 0),
(30, 27, 21, 'Nemanja3816049449', 0),
(31, 27, 22, 'Nemanja3816049449', 0),
(32, 27, 23, 'Nemanja3816049449', 0),
(33, 27, 24, 'Nemanja3816049449', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_settings`
--

DROP TABLE IF EXISTS `user_settings`;
CREATE TABLE IF NOT EXISTS `user_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `account_paused_at` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_settings`
--

INSERT INTO `user_settings` (`id`, `user_id`, `account_paused_at`, `updated_at`) VALUES
(2, 115, 1545577884, '2018-12-23 16:11:24');

-- --------------------------------------------------------

--
-- Table structure for table `user_to_services`
--

DROP TABLE IF EXISTS `user_to_services`;
CREATE TABLE IF NOT EXISTS `user_to_services` (
  `user_to_services_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `work_time_mon_fri`
--

DROP TABLE IF EXISTS `work_time_mon_fri`;
CREATE TABLE IF NOT EXISTS `work_time_mon_fri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `start` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `end` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `work_time_mon_fri`
--

INSERT INTO `work_time_mon_fri` (`id`, `user_id`, `start`, `end`) VALUES
(8, 5, '18', '18'),
(9, 13, '00', '00'),
(11, 22, '00', '00');

-- --------------------------------------------------------

--
-- Table structure for table `work_time_weekend`
--

DROP TABLE IF EXISTS `work_time_weekend`;
CREATE TABLE IF NOT EXISTS `work_time_weekend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `start` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `end` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `work_time_weekend`
--

INSERT INTO `work_time_weekend` (`id`, `user_id`, `start`, `end`) VALUES
(13, 5, '16', '14'),
(14, 13, '00', '00'),
(16, 22, '00', '00');
COMMIT;


