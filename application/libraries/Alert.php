<?php
namespace Lib;
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Get and set session flash messages
*/
class Alert
{
	protected static $CI = null;
	protected static $data = null;
	protected static $text = null;
	protected static $error = null;
	protected static $success = null;
	protected static $message = null;
	protected static $errorMsg = null;
	protected static $local_error = null;
	protected static $local_success = null;
	protected static $local_warning = null;

	private function __construct(){}

	protected function __clone(){}

	protected function __wakeup(){}

	function __invoke(){
		return false;
	}

	/**
	 * When invoking inaccessible methods (private, protected, not exiting...)
	 * @param  [string] $method
	 * @param  [array] $arg
	 * @return [Alert::method]       Returns the return value of the callback, or FALSE on error.
	 */
	public static function __callStatic($method, $arg){
		self::getInstance();
		self::init();
		return call_user_func_array([__CLASS__,$method], $arg);
	}

	public static function getInstance(){
		if (self::$CI == null) {
			self::$CI = &get_instance();
			self::$CI->lang->load('alert', 'en');
		}
	}

	protected static function init($value='')
	{
		if (!empty(self::$CI->session->error_flash)) {
			self::$error = '<div class="row madames-msg">
								<div class="alert alert-danger col-md-12 col-xs-12 text-center">
									<button class="close" data-dismiss="alert">&times;</button>
									<p class=" text-center">'.self::$CI->session->error_flash.'</p>
							  </div>
						  </div>';
		  #unset($_SESSION['success_flash']);
		}
		if (!empty(self::$CI->session->success_flash)) {
			self::$success = '<div class="row madames-msg">
								<div class="alert alert-success col-md-12 col-xs-12 text-center" >
									<button class="close" data-dismiss="alert">&times;</button>
									<p class=" text-center">'.self::$CI->session->success_flash.'</p>
							  </div>
				  		</div>';
		  #unset($_SESSION['error_flash']);
		}
		if (!empty(self::$CI->session->local_success_flash)) {
			self::$local_success = '<div class="row madames-msg">
								<div class="alert alert-success col-md-12 col-xs-12 text-center" >
									<button class="close" data-dismiss="alert">&times;</button>
									<p class=" text-center">'.self::$CI->session->local_success_flash.'</p>
							  </div>
				  		</div>';
		}
		if (!empty(self::$CI->session->local_error_flash)) {
			self::$local_error = '<div class="row madames-msg">
								<div class="alert alert-danger col-md-12 col-xs-12 text-center">
									<button class="close" data-dismiss="alert">&times;</button>
									<p class=" text-center">'.self::$CI->session->local_error_flash.'</p>
							  </div>
						  </div>';
			self::$CI->session->unset_userdata('local_error_flash');
		}
		if (!empty(self::$CI->session->local_warning_flash)) {
			self::$local_warning = '
						<div class="alert alert-warning alert-dismissible madames-msg">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			        <h4><i class="icon fa fa-warning"></i> '.self::$CI->lang->line('warning').'!</h4>
			        <p class=" text-center">'.self::$CI->session->local_warning_flash.'</p>
			      </div>';
		}
		if (!empty(self::$CI->session->data_flash)) {
			self::$data = unserialize(self::$CI->session->data_flash);
		  #unset($_SESSION['error_flash']);
		}
		if (!empty(self::$CI->session->text)) {
			self::$text = self::$CI->session->text;
		  #unset($_SESSION['error_flash']);
		}
		return null;
	}

	protected static function hasError($value='')
	{
		if (self::$error != null) {
			return true;
		}
	}
	# Execute callback function if has error
	protected static function ifError($callback)
	{
		if (self::$error != null) {
			$callback(self::$error);
		}
	}

	protected static function ifMessage($callback)
	{
		if (self::$message != null) {
			$callback(self::$message);
		}
	}

	protected static function getError($value='')
	{
		if (self::$error != null) {
			return self::$error;
		}
		return;
	}

	protected static function getErrorMsg($value='')
	{
		if (self::$errorMsg != null) {
			return self::$errorMsg;
		}
		return;
	}

	protected static function getSuccess($value='')
	{
		if (self::$success != null) {
			return self::$success;
		}
		return;
	}

	protected static function getMessage($value='')
	{
		#if(self::getText()) return self::getText();
		if(self::getError()) return self::getError();
		if(self::getSuccess()) return self::getSuccess();
		if(self::getErrorMsg()) return self::getErrorMsg();
		return self::getLocalMsg();
		
	}

	protected static function getLocalMsg($value='')
	{
		if (self::$local_error !== null) return self::$local_error;
		if (self::$local_success !== null) return self::$local_success;
		if (self::$local_warning !== null) return self::$local_warning;
		return null;
	}

	protected static function getData($value='')
	{
		if (self::$data != null) {
			return self::$data;
		}
		return;
	}

	protected static function getText($value='')
	{
		if (self::$text != null) {
			return self::$text;
		}
		return;
	}

	public static function getBootstrap()
	{
		echo '<link href="'.base_url().'web/admin/css/bootstrap.css" rel="stylesheet" />';
	}

	public static function setError($value='No errors')
	{
		self::getInstance();
		self::$CI->session->set_flashdata('error_flash', $value);
	}

	protected static function setText($text='')
	{
		#self::getInstance();
		self::$CI->session->set_flashdata('text', $text);
	}

	public static function setSuccess($value='No message')
	{
		self::getInstance();
		self::$CI->session->set_flashdata('success_flash', $value);
	}

	protected static function setLocalSuccess($value='', $not_redirect = FALSE)
	{
		if ($not_redirect === true) {
			self::$local_warning = self::_createAlert('warning', $value);
		}
		else
		{
			self::$CI->session->set_flashdata('local_success_flash', $value);
		}
	}

	protected static function setLocalWarning($value='')
	{
		self::$CI->session->set_flashdata('local_warning_flash', $value);
	}

	protected static function setLocalError($value='')
	{
		self::$CI->session->set_flashdata('local_error_flash', $value);
	}

	public static function setData($data=null)
	{
		self::getInstance();
		if (is_array($data)) {
			self::$CI->session->set_flashdata('data_flash', serialize($data));
		} else
		die("Alert::setData() error! Expexting array! Given: ". gettype($data));
	}

	public static function Clog($value='')
	{
		echo "<script>console.log('$value');</script>";
	}

	public static function TestMsg($value='')
	{
		echo "<div style='display:none'>$value</div>";
	}

	private static function _createAlert($type, $value)
	{
		$additional = '';
		if ($type === 'warning')
		{
			$additional = '<h4><i class="icon fa fa-warning"></i> '.self::$CI->lang->line('warning').'!</h4>';
		}
		$alert = '
						<div class="alert alert-'.$type.' alert-dismissible">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			        '.$additional.'
			        <p class=" text-center">'.$value.'</p>
			      </div>';
		return $alert;
	}

}

 ?>
