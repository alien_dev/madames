<?php
use Lib\Validate;
use Lib\Form;

class MY_Model extends CI_Model {
    public $table_name = "DontForgetTableName";
    protected $id = 'id';
    public $sort = 'id';
    public $table_data_all = null; # Cuva podatke iz $model->all() kako ne bi svaki put slao upit u bazu
    public $field_names = '';
    function __construct()
    {
        parent::__construct();
        #$this->lang->load('common', 'rs');
        /*if (isset($this->relations)) {
            foreach ($this->relations as $relation_name => $relation) {
                $this->load->model($relation[2]); 
                $model = $this->modelName($relation[2]);
                $this->db->join($this->{$model}->table, $this->table.'.'.$relation[1].' = '.$this->{$model}->table.'.id', 'inner');
            }
        }
        $this->db->from($this->table);*/
        
    }

    public function displayName($row)
    {
        if(isset($row['name'])) return 'name';
        if(isset($row['name_rs'])) return 'name_rs';
        return null;
    }

    public function tableName()
    {
        #return lang(get_class($this));
        return $this->title;
    }

    protected function rules()
    {
        return null;
    }

    private function validate($table_data=null, $multi_forms=null)
    {
        if($this->rules()){
            if($multi_forms)
            {
                foreach($table_data as $form)
                {
                    if(!Validate::form($this->rules(), $form))
                    {
                        /* printr(validation_errors());
                        printr(Validate::$errors);die(); */
                        if($this->input->is_ajax_request())
                        {
                            $error_msg = validation_errors();
                            $response = [
                                'status' => 'error',
                                'error_msg' => $error_msg
                            ];
                            echo json_encode($response, false);
                            return false;
                        }
                        else return false;
                    }
                }
                if($this->input->is_ajax_request())
                {
                    echo json_encode(['status' => 'ok'], false);   
                }
                return true;
            }
            else return Validate::form($this->rules(), $table_data);
        }
        return true;
    }

    protected function table($fields='*')
    {
        $this->db->select($fields);
        $this->db->from($this->table_name);
    }

    public function all($take=null, $start=null, $array = false)
    {   
        $data = null;
        $this->table();
        $this->scope();
        if ($take !== null && $start !== null) 
        {
            $this->db->limit((int)$take, (int)$start);
        } 
        $this->db->order_by($this->table_name.'.'.$this->sort, 'ASC');
        if ($array) 
        {     
            /*if (isset($this->table_data_all[$this->table])) 
            {
                $data = $this->table_data_all[$this->table];
            }  
            else 
            {
                $this->table_data_all[$this->table] = $this->db->get()->result_array();
                $data = $this->table_data_all[$this->table];
                $data = $this->renderColumn($data);
            }*/
            $data = $this->db->get()->result_array();
            #return $this->renderColumn($data);
            return $data;
        } 
        else 
        {
            /*if (isset($this->table_data_all[$this->table])) 
            {
                $data = $this->table_data_all[$this->table];
            }  
            else 
            {
                #echo $this->db->last_query();
                $this->table_data_all[$this->table] = $this->db->get()->result();
                $data = $this->table_data_all[$this->table];
            }*/
            return $this->db->get()->result();
        }
         
        /*return $data;*/
        # Problem kod forme za kreiranje vozila, nepoznat uzork greske
    }

    public function renderColumn($data)
    {
        /*foreach ($data as &$row) {
            $row['visible'] = 'Vidljivo';
        }
        return $data;*/
        if (method_exists($this, 'columnDisplay')) {
            array_walk($data, function(&$value, $index){
                #$this->columnDisplay($value);
                #$value['visible'] = 'Vidljivo';
                $columns_settings = $this->columnDisplay();
                foreach ($columns_settings as $key => $column) {
                    if (isset($value[$key])) {
                        $new_vlaue['type'] = $column['type'];
                        if (isset($column['value'])) {
                            $text_or_func = $column['value'];
                            if(gettype($text_or_func) == 'object'){
                                $new_vlaue['value'] = $text_or_func($value[$key]);
                            }
                            else if(gettype($text_or_func == 'string'))
                            {
                                $new_vlaue['value'] = $column['value']; //$text_or_func
                            }
                        } else $new_vlaue['value'] = $value[$key];
                        $value[$key] = $new_vlaue;
                    }
                }
            });
        }
        
        return $data;
    }

    public function get($fields, $take=null, $start=null, $array = false)
    {   
            
        $this->table($fields);
        $this->scope();
        if ($take !== null && $start !== null) 
        {
            $this->db->limit((int)$take, (int)$start);
        } 
        $this->db->order_by($this->table_name.'.'.$this->sort, 'ASC');
        if ($array) 
        {       
            return $this->db->get($this->table_name)->result_array();
        } 
        else 
        {
            return $this->db->get($this->table_name)->result();
        }
    }

    public function find($id)
    {    
        $this->table();
        if(is_array($id))
        {
            $this->db->where($id);
        }
        else
        {
            $this->db->where($this->table_name.".id", $id);
        }
        $result = $this->db->get()->result();
        if(empty($result))
        {
            return null;
        }
        else return $result[0];
    }

    public function search($search_fields, $search_param)
    {   
        /*if (isset($search_fields) && !empty($search_fields)) 
        {
            $search = [];
            foreach ($search_fields as $field) {
                $search[($this->table.".".$field)] = $search_param;
            };
            $this->db->or_like($search);        
        }*/
        if (isset($this->search_fields)) 
        {
            $search = [];
            foreach ($this->fields as $field_name => $field_array) {
                if ($this->hasRelation($field_name)) {
                    $model = $this->model($this->relations[$field_name][2]);
                    $search[$model->table_name.'.'.$model->searchField] = $search_param;                
                } else $search[$this->table_name.'.'.$field_name] = $search_param;
            };
            $this->db->or_like($search);        
        }
    }

    public function searchModels($search_fields, $search_param)
    {
        $this->table();  
        if (!empty($search_fields)) 
        {
            $search = [];
            foreach ($search_fields as $field) {
                $search[($this->table_name.".".$field)] = $search_param;
            };
            $this->db->where($search);   
            return $this->db->get()->result();    
        }    
    }

    public function findByAttributes($search)
    {
        $this->table();  
        if (!empty($search)) 
        {
            $this->db->where($search);   
            return $this->db->get()->result();    
        }    
    }

    public function total($value='')
    {
    	return $this->db->get($this->table_name)->num_rows();
    }

    public function delete($id)
	{
        $this->beforeDelete($id);
        if(is_array($id))
        {
            $this->db->where($id);
        }
        else
        {
            $this->db->where($this->id, $id);
        }
		
		$this->db->delete($this->table_name);

        #$this->afterDelete();
	}

    public function filter($filter)
    {
        if ($filter) 
        {
            $model_filters = $this->modelFilter();
            $search_field = key($model_filters[$filter]);
            if (is_array($model_filters[$filter][$search_field])) 
            {
                foreach ($model_filters[$filter][$search_field] as $search_value) 
                { 
                   $this->db->or_where($search_field, $search_value);
                }
            }
            else 
            {
                $this->db->where($model_filters[$filter]);
            }
        }
        
    }

    public function beforeSave(){return [];}
    public function afterSave($data){}
    public function afterDelete($table_data){}
    public function beforeDelete($id){}
    public function scope(){}

    public function save($table_data)
    {
        $custom_data = $this->beforeSave();
        $table_data = array_merge($table_data,$custom_data);
        if(isset($table_data['multi_forms']))
        {
            if($this->validate($table_data['multi_forms'],true))
            {
                $this->db->insert_batch($this->table_name, $table_data['multi_forms']);
            }        
        }
        else
        {
            if($this->validate($table_data))
            {
                $this->db->insert($this->table_name, $table_data);
            }
        }       
        $this->afterSave($table_data);  
        return $this->db->insert_id();     
    }

    public function update($id, $table_data)
    {
        $custom_data = $this->beforeSave();
        $table_data = array_merge($table_data,$custom_data);
        
        if (is_array($table_data[key($table_data)])) 
        {
            # code...
        }
        else 
        {
            #$table_data = $this->relations($table_data); // Gazi ceo niz
            if(is_array($id)) //['user_id' => 115]
            {
                $this->db->where($id);
            }
            else
            {
                $this->db->where($this->id, $id);
            }           
            $table_data['updated_at'] = date("Y-m-d H:i:s");
            return $this->db->update($this->table_name, $table_data); 
        }
    }

    public function updateManyMany($table_data)
    {
        # id_field => ['make_id'=>1, 'category_id'=>1]
        # where model_id = 1 AND category_id = 1
        foreach ($table_data['id_fields'] as $field_name => $id_field) {
           $this->db->where($this->id, $table_data['id']);
        }
        unset($table_data['id_fields']);
        $this->db->update($this->table_name, $table_data);
    }

    /**
     * Prvi key u nizu je naziv relacije koja se upisuje u :relation kako bi posle 
     * relaciona kolona znala koji naziv da nose inputi kako bi u nastavku model 
     * znao da prepozna relaciju i kako da nastavi dalje
     * Proverava se da li je prosledjena neka relacija, u trazenom relacionom modelu 
     * se vrsi cuvanje, zatim se unsetuje ta relacija i cuvaju se ostali inputu u ovom 
     * modelu (name, name_rs...)
     * @param  [array] &$item [prosledjeni podaci za cuvanje u bazu, ceo red iz crud liste]
     * @return [type]        [description]
     */
    protected function relations(&$table_data)
    {   
        if (isset($this->relations)) 
        {
            foreach ($this->relations as $key => $value) { printr($table_data);
                # ako je input name isti kao 3 element u relations podnizu
                if (isset($table_data[$key])) 
                {
                    switch ($value[0]) {
                        case 'MANY_MANY':
                            return $this->manyMany($table_data, $key, $value);
                            break;

                        case 'HAS_ONE':
                            $data[$this->relations[$key][1]] = $table_data[$key];
                            printr($data);
                            $this->db->where($this->id, $table_data['id']);
                            $this->db->set($data);
                            break;
                        
                        default:
                            # code...
                            break;
                        
                    }
                    unset($item[$key]);
                }
            }
        }
        
    }

    public function isRelation($name)
    {
        if(strpos($name, "id") !== false)
        {
            #return explode("_", $name)[0];
            return rtrim($name, "_id");
        }
        return $name;
    }

    protected function hasRelation($key)
    {
        if (isset($this->relations[$key])) {
           return true;
        }
        return false;
    }

    protected function manyMany(&$table_data, $relation_name, $relation)
    {

        if (isset($table_data['id'])) {
            $field = strtolower(get_class($this))."_id";
            $make = $this->find($table_data['id']);
            $id_fields[$relation[1]] = $table_data[$relation_name]; //category_id
            $id_fields[$field] = $table_data['id']; //make_id
            $data['id_fields'] = $id_fields;
            unset($table_data['id']);
            unset($table_data[$relation[1]]);
            $table_update_data = array_merge($table_data, $id_fields);
            $this->load->model($relation[2]);
            // Dodati WHERE pa onda save
            $this->{explode('/', $relation[2])[1]}->updateManyMany($table_update_data);

        } else {

        }
        
    }

    public function getRelations()
    {
        if (isset($this->relations)) {
            return $this->relations;
        }
        else return '';
    }

    public function getFieldNames()
    {
        if (isset($this->field_names)) {
            return $this->field_names;
        }
        return '';
    }

    public function getModel($full_name)
    {
        if(strpos($full_name,'/') !== false)
		{
			$model_name = explode("/", $full_name)[1];
		}
		else $model_name = $full_name;
        $this->load->model($full_name);
        return $this->{$model_name};
    }

    public function generateFormFields()
    {
        $form_fields = null;
        Form::registerModel($this);
        foreach ($this->fields as $field_key => $field) {
            $form_fields[$field_key] = Form::field($field_key, $field);
        }
        return $form_fields;
    }

}