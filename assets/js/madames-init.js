window._m = {
    base_url: document.querySelector("meta[name='base_url']").content,
    screens: {
        desktop_min_size: 975
    },
    account_statuses: {
        ENABLED: 0,
        DISABLED: 8
    },
    account_periods: {
        a: 30,
        b: 15,
        c: 7,
        d: 2,
        e: 1
    }
}
var base_url = window._m.base_url;