<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb348fe104221bc1ff34979e093ba37d4
{
    public static $files = array (
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
    );

    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Contracts\\' => 18,
            'Symfony\\Component\\Translation\\' => 30,
        ),
        'M' => 
        array (
            'Model\\' => 6,
            'Madames\\' => 8,
        ),
        'L' => 
        array (
            'Lib\\' => 4,
        ),
        'F' => 
        array (
            'Faker\\' => 6,
        ),
        'E' => 
        array (
            'Extensions\\' => 11,
        ),
        'C' => 
        array (
            'Carbon\\' => 7,
        ),
        'B' => 
        array (
            'Base\\Model\\' => 11,
        ),
        'A' => 
        array (
            'Admin\\Model\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Contracts\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/contracts',
        ),
        'Symfony\\Component\\Translation\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/translation',
        ),
        'Model\\' => 
        array (
            0 => __DIR__ . '/../..' . '/models',
        ),
        'Madames\\' => 
        array (
            0 => __DIR__ . '/../..' . '/components',
        ),
        'Lib\\' => 
        array (
            0 => __DIR__ . '/../..' . '/libraries',
        ),
        'Faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/fzaninotto/faker/src/Faker',
        ),
        'Extensions\\' => 
        array (
            0 => __DIR__ . '/../..' . '/extensions',
        ),
        'Carbon\\' => 
        array (
            0 => __DIR__ . '/..' . '/nesbot/carbon/src/Carbon',
        ),
        'Base\\Model\\' => 
        array (
            0 => __DIR__ . '/../..' . '/modules/base/models',
        ),
        'Admin\\Model\\' => 
        array (
            0 => __DIR__ . '/../..' . '/modules/admincontrol/models',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb348fe104221bc1ff34979e093ba37d4::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb348fe104221bc1ff34979e093ba37d4::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
