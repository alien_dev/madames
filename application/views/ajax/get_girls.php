<div class="row girls_row" id="girls">
    <?php foreach ($girls as $girl): ?>
        <div class="col-lg-3 col-md-6 mb-3 girl_card_phone">
            <div class="card home_card">
                <div class="img-holder">
                    <a href="<?php echo site_url('/girls/profile/' . $girl['slug']); ?>">
                        <span><?php echo $girl['city']; ?></span>
                        <!-- <img class="card-img-top" src="<?=(!empty($girl['profile_photo'])) ? $girl['profile_photo'] : site_url('/assets/images/girls/' . $girl['slug']);?>.jpg" alt=""> -->
                        <img class="card-img-top" src="<?=$this->girl_model->get_user_profile_image($girl['slug'])?>" alt="">
                    </a>
                </div>
                <div class="card-body">
                    <h4 class="card-title h4_home_card">
                        <a href="<?php echo site_url('/girls/profile/' . $girl['slug']); ?>"><?php echo $girl['username']; ?></a>
                    </h4>
                     <div class="status_home_girl">
                       <?php if ($girl['status'] == 1): ?>
                        <img style="float:left;"class="dot_girl" src="assets/images/green.png">
                          <?php else: ?>
                        <img style="float:left;"class="dot_girl" src="assets/images/yellow.png">
                        <?php endif ?>
                        <!-- <button type="button" class="btn btn-primary">
                          Launch
                        </button> -->

                        <!-- <?php if (!$USER): ?> -->
                          <!-- <?php if ($girl['status']==1&&$girl['chat_status']==1): ?> -->
                            <img id="btn-getInTouch" onclick="chat('<?php echo $girl['id']; ?>','<?php echo $girl['username']; ?>',event);" src="<?=base_url();?>assets/images/live.png" style="width:28px;height:28px;float:right;">
                          <!-- <?php endif ?> -->
                        <!-- <?php endif ?> -->
                        <div style="clear:both;"></div>
                      </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>
<?=$pagination_links?>
<!-- /.row -->
