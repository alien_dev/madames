
    <!-- Page Content -->
     <div class="row custom_profile_row">
       <div class="col-lg-4 order-lg-1 text-center img_div_pro">
           <img src="<?php echo base_url(); ?>assets/images/girls/<?=$slug;?>.jpg" class="mx-auto img-fluid img-circle d-block img_profile" alt="avatar">
            <?php if (isset($_SESSION['slug']) && $_SESSION['slug']==$girl->slug): ?>

            <?php endif ?>
            <div class="row">
              <div class="profile_description">
                  <p class="user-description"><?=$girl->description?></p>
              </div>
            </div>
       </div>
         <div class="col-lg-8 order-lg-2 table_profile_tab">
             <ul class="nav nav-tabs">
                 <li class="nav-item nav_item_profile" id="scroll_to">
                     <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Contact</a>
                 </li>
             </ul>
             <div class="tab-content py-4">
               <h3 class="mb-3 profile_h3"><?php echo $girl->username;?></h5>
                <?php echo form_open("girls/update_contact/".$girl->slug, ['id'=>'update_contact']); ?>
                  <input type="hidden" name="id"  value="<?php echo $girl->id; ?>">
                  <input type="hidden" name="username"  value="<?php echo $girl->username; ?>">
                  <table class="table profile_table">
                    <tbody class="profile_table">
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Phone:</span></th>
                        <td><input class="pull-left profile_input" type="text" class="form-control" name="phone" id="exampleInputEmail1" aria-describedby="emailHelp"  disabled value="<?php echo $girl->phone; ?>"><i class="fas fa-question-circle" style="font-size:18px;color:#1a8cff;margin-left:8px;"></i></td>
                      </tr>

                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Address:</span></th>
                        <td><input class="pull-left profile_input" type="text" class="form-control" name="address" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->address; ?>"></td>
                      </tr>

                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Web:</span></th>
                        <td><input class="pull-left profile_input" type="text" class="form-control" name="web" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->web; ?>"></td>
                      </tr>

                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Club:</span></th>
                        <td><input class="pull-left profile_input" type="text" class="form-control" name="club" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->club; ?>"></td>
                      </tr>

                    </tbody>
                    </table>
                    <div class="div_btn_sbm">
                    <button type="submit" class="btn btn-primary aa">Submit</button>
                      <a href="<?php echo site_url('/girls/profile/'.$girl->slug); ?>" class="btn btn-cancel" style="border-radius:3px;">Cancel</a>
                    </div>
                    </form>


             </div>
         </div>
     </div>
     <script src="<?php echo base_url(); ?>assets/js/validate.js"></script>
    <!-- /.container -->
    <script>
    if ($(window).width() < 992) {
      $(function() {
          let elmnt = document.getElementById('scroll_to');
          elmnt.scrollIntoView(true);
      });
      }
      let V = new Validate("update_contact");
      V.setRules({
          address:{
              name: "address",
              rule: "chars|max:25",
          },
          web:{
              name: "web",
              rule: "chars|max:25",
          },
          club:{
              name: "club",
              rule: "chars|max:25",
          },
      });
      V.run();
    </script>
