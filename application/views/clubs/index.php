      <div class="row" style="padding-left:10px;padding-right:10px;">

        <div class="col-lg-3 aside-search">


          <div class="list-group" id="regions">
              <h1 class="my-4 slide-down">
                City / Regions
                <div class="angle-down-div">
                  <i class="fas fa-angle-down"></i>
                </div>
              </h1>
              <div class="slide-down-list">
              <?php foreach($regions as $region) : ?>
                <a class="list-group-item" href="<?php echo site_url('/clubs/regions/'.$region->id); ?>">
                  <span class="icon-m-location"></span>
                  <div style="display:inline-block;" class="side_p_reg">
                  <?php echo $region->name; ?>
                  </div>
                </a>
              <?php endforeach; ?>
              </div>
          </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9 clubs_row">

          <div class="row">
            <?php foreach($clubs as $club) : ?>
            <div class="col-lg-3 col-md-6 mb-3 girl_card_phone">
              <div class="card home_card">
                <div class="card-body">
                  <h4 class="card-title club_h4"><?php echo $club['username']; ?>
                  </h4>
                </div>
                <div class="img-holder">
                    <span><?php echo $club['name']; ?></span>
                      <a href="<?php echo site_url('/clubs/profile/' . $club['slug']); ?>"><img class="card-img-top club_img" src="<?php echo base_url(); ?><?=$this->club_model->get_club_profile_image($club['slug'])?>" alt=""></a>
                  </a>
                </div>
                <div class="card-body club_card">
                  <h6><b>Address</b></h6>
                  <h6 class="club_line"><?php echo $club['address']; ?></h6>
                </div>
             </div>
            </div>
          <?php endforeach; ?>
          </div>
          <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <?=$pagination_links?>
      <!-- /.row -->
      <script>
        window.addEventListener('load', function() {
          slideDownList( "regions", customListCSS );
        })

        function customListCSS(list_status, container){
            var list = $(container).find('.slide-down');
            if(list_status == 'open'){
                list.css('border-bottom-left-radius', '0');
                list.css('border-bottom-right-radius', '0');
            } else {
                list.css('border-bottom-left-radius', '4px');
                list.css('border-bottom-right-radius', '4px');
            }
        }

        var window_loc = window.location.href;


        var a = $('.list-group-item');

        $.each( a, function( key, value ) {
            if(value.getAttribute("href") == window_loc){
              value.classList.add('active-region');
            };
          });

      </script>
