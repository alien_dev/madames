var EventBus = new Vue();

//Component
var filterTags = {
    template:"#filter-tags",
    props: {
        tags:{type:Array, default:[]},
    },
    data () {
        return {
            message: "Filter tags component",
            data: "",
            filters: []
        }
    },
    methods: {
        close: function () {
            this.show = false;
        },
        removeFilter(filter){
            this.$emit('remove-filter-tag', filter);
        },
    }
};

var Listing = new Vue({
    el: '#listing',
    components: {
        'filterTags': filterTags
    },
    data() {
        return {
            _loader: null,
            base_url: window._m.base_url,
            response: "<strong>No results</strong>",
            search_input: null,
            responseStatus: false,
            pagination_start: 0,
            girls: null,
            online_girls: [],
            filters: {
                regions: [],
                ts: {
                    name: "TS"
                },
                age: {
                    name: "Age",
                    value: ["18-25", "25-35", "35-45", "45-55", "55+"]
                },
                services: {
                    name: "Services",
                    value: [] //Popunjava se u getData()
                },
                hairs: {
                    name: "Hair color",
                    value: []
                },
                types: {
                    name: "Type",
                    value: []
                },
                username:{
                    name: 'Username',
                    value: ''
                }
            },
            tags: [],
            selected_filters: {
                age: "",
                services: [],
                hairs: [],
                types: [],
                ts: "",
                order: [],
                order_age: "",
                order_username: "",
                region: "",
                username: ""
            },
            search_timer: null
        };
    },
    methods: {
        close: function() {
            console.log("test");
        },
        search(e){
            var _this = this;
            if(this.search_timer){
                clearTimeout(this.search_timer);
                this.search_timer = null;
            }
            this.search_timer =  setTimeout( function(){
                _this.selected_filters.username = _this.search_input;
                _this.renderView();
            },800);
        },
        remove() {},
        filterRegion(value, event) {
            this._loader.style.display = 'block';
            let el = event.target;
            this.pagination_start=0;
            if (el.classList.contains("active-region"))
            {
                el.classList.remove("active-region");
                this.selected_filters['region'] = "";
            }
            else
            {
                let parent = el.parentElement;
                let active_elements = parent.querySelectorAll(".active-region");
                if (active_elements.length > 0)
                {
                    active_elements[0].classList.remove("active-region");
                }
                el.classList.add("active-region");
                this.selected_filters['region'] = value;
            }
            this.renderView();
            if ($(window).width() < 992) {
              let elmnt = document.getElementById('online-girls');
              elmnt.scrollIntoView(true);
              }

        },
        filterSingleSelection(filter_name, value, name, event) {           
            this._loader.style.display = 'block';
            let el = event.target;
            let check = el.querySelector('.fa-check-circle');
            this.pagination_start=0;
            let filter = {
                    name: filter_name,
                    display_value: name,
                    value: value,
                    el: el
                };
            if (el.classList.contains("active-filter"))
            {
                el.classList.remove("active-filter");
                this.selected_filters[filter_name] = "";
                //EventBus.$emit("reset_filter", filter);
                //Ovde vracamo samo onaj filter u niz koji ima razlicit name od prosledjenog
                //(ukoliko je activ-filter nece vratiti izabrani filter tj. resetujemo tags)
                // zato sto je singleSelection
                this.tags = this.tags.filter(item => item.name != filter_name);
                check.classList.add('hidden');
            }
            else
            {
                let ul = el.parentElement;
                let active_elements = ul.querySelectorAll(".active-filter");
                if (active_elements.length > 0)
                {
                    let active_elements_check = active_elements[0].querySelector('.fa-check-circle');
                    active_elements[0].classList.remove("active-filter");
                    active_elements_check.classList.add('hidden');
                    this.tags = this.tags.filter(item => item.name != filter_name);
                }
                el.classList.add("active-filter");
                if (name != "") this.tags.push(filter);
                this.selected_filters[filter_name] = value;
                check.classList.remove('hidden');
            }
            this.renderView();
        },
        filterMultipleSelection(filter_name, value, name, event) {
            this._loader.style.display = 'block';
            let el = event.target;
            let check = el.querySelector('.fa-check-circle');
            this.pagination_start=0;
            let filter = {
                name: filter_name,
                display_value: name,
                value: value,
                el: el
            };

            if (el.classList.contains("active-filter")) {
                el.classList.remove("active-filter");
                this.selected_filters[filter_name] = this.selected_filters[filter_name].filter(item => item !== value);
                this.tags = this.tags.filter(item => item.value !== value);
                check.classList.add('hidden');
                this.renderView();
            } else {
                el.classList.add("active-filter");
                check.classList.remove('hidden');
                //if(typeof value == 'string') value = value.toLowerCase();
                this.selected_filters[filter_name].push(value);
                this.tags.push(filter);
                this.renderView();
            }
        },
        renderView: function() {
            var self = this;
            if (self.selected_filters.services.length == 0) self.services = "";
            if (self.selected_filters.hairs.length == 0) self.hairs = "";

            AJAX.post({
                url: "ajax/getgirls/"+self.pagination_start,
                responseType: "text",
                data: {
                    age: self.selected_filters.age,
                    services: self.selected_filters.services,
                    hairs: self.selected_filters.hairs,
                    type: self.selected_filters.types,
                    ts: self.selected_filters.ts,
                    order: self.selected_filters.order,
                    region: self.selected_filters.region,
                    username: self.selected_filters.username
                }
            }).then(function(obj) {
                //console.log(obj);
                self.responseStatus = true;
                self.response = obj;
                self._loader.style.display = 'none';
            });
        },
        removeSelectedFilter(filter) {
            this._loader.style.display = 'block';
            if(typeof this.selected_filters[filter.name] == "string" || typeof this.selected_filters[filter.name] == "number")
            {
                this.selected_filters[filter.name] = '';
                this.tags = this.tags.filter(item => item.name != filter.name);
            }
            else
            {
                this.selected_filters[filter.name] = this.selected_filters[filter.name].filter(item => item !== filter.value);
                // Zbog mogucnosti podudaranja id-jeva morama proveru value-a da vezemo za njemu pripadajuci filter name
                this.tags = this.tags.filter(item => {
                    if (item.name == filter.name && item.value == filter.value)
                    {
                        return false;
                    }
                    else return true
                });
            }
            filter.el.classList.remove("active-filter");
            filter.el.querySelector('.fa-check-circle').classList.add('hidden');
            this.renderView();
        },
        chatAdapter(id, name, event){
            event.preventDefault();
            chat(id,name, event);
        },
        getData(){
            var self = this;
            AJAX.post({
                url: "ajax/getModels",
                data: {
                    model_names: ["girl_model", "region", "service", "type", "hair"],
                    model_pagination: {girl_model: {take:12,start:1}, model_b:{take:12,start:0}}
                }
            }).then(function(response) {
                //console.log(response);
                self.girls = response.girl_modell;
                self.online_girls = response.online_girls;
                self.filters.regions = response.region;
                self.filters.services.value = response.service;
                self.filters.hairs.value = response.hair;
                self.filters.types.value = response.type;
                //console.log(self.online_girls);
            })
        },
        pag(e){
            e.preventDefault();
            this.pagination_start = (e.target.dataset.ciPaginationPage-1)*12; //Mora -1 zato sto CI tako funkcionise
            this.renderView();
            window.scrollTo(0,0); //$("html, body").animate({ scrollTop: 0 }, "slow");
        },

        init(){
            function customListCSS(list_status, container){
                var list = $(container).find('.slide-down');
                if(list_status == 'open'){
                    list.css('border-bottom-left-radius', '0');
                    list.css('border-bottom-right-radius', '0');
                } else {
                    list.css('border-bottom-left-radius', '4px');
                    list.css('border-bottom-right-radius', '4px');
                }
            }
            slideDownList( "regions", customListCSS); //script.js

            slideDownList( "online-girls", customListCSS);

            slideDownList( "filters", customListCSS);
        }
    },
    computed: {},
    watch: {},
    mounted() {
        var self = this;
        this.renderView();
        EventBus.$on('pag', page => this.pag(page)); //Event za paginaciju
        this.init();
    },
    created() {
        this.getData();
        this._loader = document.getElementById("m-loader");
    }
})
