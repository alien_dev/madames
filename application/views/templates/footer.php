<footer id="footer" class="py-5 footer">
    <div class="footer1">
        <div class="container-fluid">

            <div class="container ">
                <div class="row">
                    <div class="col-sm-4 foot">
                        <h2>Where we are<i style="float:right;font-size: 20px;padding-top:3px;color:white;margin-right:1px;" class="icon-m-location"></i></h2><p>
                        <div class="myframegmap">
                            <div style="width: 720px">
                                <iframe width="720" height="250"
                                    src="https://maps.google.com/maps?width=720&amp;height=250&amp;hl=en&amp;q=Untervaz,Hintergasse,3+(madames)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"
                                    frameborder="0"
                                    scrolling="no"
                                    marginheight="0"
                                    marginwidth="0">
                                    <a href="https://www.maps.ie/create-google-map/">
                                        Google map generator
                                    </a>
                                </iframe>
                            </div>
                            <br />
                        </div>
                        <br>
                    </div>
                    <div class="col-sm-4 foot">
                        <h2 class="hhh">Home<i style="float:right;font-size: 20px;padding-top:3px;" class="fa fa-home"></i></h2>
                        <a href="<?php echo base_url(); ?>" class="p_footer">Girls</a><br/>
                        <a href="<?php echo base_url(); ?>Clubs" class="p_footer">Clubs</a><br/>
                        <a href="<?php echo base_url(); ?>Contact" class="p_footer">Contact</a><br/>
                        <a href="<?php echo base_url(); ?>Help" class="p_footer">Help</a><br/>
                        <a href="<?php echo base_url(); ?>About" class="p_footer">About us</a><br/>
                        <?php if (!$this->ion_auth->logged_in()) : ?>
                        <a href="<?php echo base_url(); ?>account/login" class="p_footer">Members</a><br/>
                        <?php endif; ?>
                        <a href="<?php echo base_url(); ?>price" class="p_footer">Price</a><br/>
                    </div>
                    <div class="col-sm-4 foot">
                        <h2 style="margin-bottom: 15px;">Contact Us<i style="float:right;font-size: 20px;padding-top:3px;" class="fa fa-phone"></i></h2>
                        <i style="margin-left:12px;height:14px;width:14px;" class="fas fa-mobile-alt"></i>    <a href="#" class="p_footer" style="margin-left:7px;letter-spacing: .02rem;">+ 41 (0)79 289 45 32</a><br/>
                        <i style="margin-left:8px;height:14px;width:14px;" class="fas fa-home"></i>    <a href="#" class="p_footer" style="margin-left:11px;letter-spacing: .035rem;">+ 41 (0)81 322 74 36</a><br/>
                        <i style="margin-left:9px;height:14px;width:14px;" class="fa fa-envelope"></i>    <a href="mailto:abc@abc.com" class="p_footer">info@madames.ch</a><br>
                        <i style="margin-left:12px;color:white;font-size:16px;margin-right:0;" class="icon-m-location"></i>    <a class="p_footer" style="margin-left:.39rem;"> Hintergasse 3, 7204 <span class="abcd">Untervaz, CH</span></a>
                        <br>
                    </div>
                </div>
                <div class="clear30"></div>
                <p class="copy_footer"> Copyright &copy VG Tech</p>
            </div>
        </div>
    </div>

    <div class="footer2">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="clear30"></div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url(); ?>assets/js/confirmation.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/dist/js/manifest.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/vendor.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/main.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>

</body>
</html>
