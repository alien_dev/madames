const APP = {
	//base_url : 'http://www.madames.ch/',
	base_url : 'http://localhost/ongit/project/madames/',
	lng : 'EN'
};
Object.defineProperty(APP, 'ajax_url', {
	get: function () {
		return this.base_url+"ajax/";
	}
});
export {APP};

//export const BASE_URL = 'http://www.madames.ch/';
export const BASE_URL = 'http://localhost/ongit/project/madames/';