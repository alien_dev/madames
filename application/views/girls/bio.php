
    <!-- Page Content -->
     <div class="row custom_profile_row">
       <div class="col-lg-4 order-lg-1 text-center img_div_pro">
           <img src="<?php echo base_url(); ?>assets/images/girls/<?=$slug;?>.jpg" class="mx-auto img-fluid img-circle d-block img_profile" alt="avatar">
            <div class="row">
              <div class="profile_description">
                  <p class="user-description"><?=$girl->description?></p>
              </div>
            </div>
       </div>
         <div class="col-lg-8 order-lg-2 table_profile_tab">
             <ul class="nav nav-tabs">
                 <li class="nav-item nav_item_profile" id="scroll_to">
                     <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Bio</a>
                 </li>
             </ul>
             <div class="tab-content py-4">
               <h3 class="mb-3 profile_h3"><?php echo $girl->username;?></h5>
                <?php echo form_open("girls/update_bio/".$girl->slug, ['id'=>'update_bio']); ?>
                  <input type="hidden" name="id"  value="<?php echo $girl->id; ?>">
                  <table class="table profile_table">
                    <tbody class="profile_table">
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Username:</span></th>
                        <td><input class="pull-left profile_input" type="text" class="form-control" name="username" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->username; ?>"></td>
                      </tr>
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Regions:</th>
                        <td><select class="form-control bio_form_profile pull-left" id="sel1"  name="region_id">
                          <?php foreach($regions as $region) : ?>
                          <option class="pull-left" value="<?php echo $region->id; ?>"><?php echo $region->name; ?></option>
                          <?php endforeach; ?>
                        </select></td>
                      </tr>
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Age:</span></th>
                        <td><input class="pull-left profile_input" type="text" class="form-control" name="age" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->age; ?>"></td>
                      </tr>
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Height:</span></th>
                        <td><input class="pull-left profile_input" type="text" class="form-control" name="height" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->height; ?>"></td>
                      </tr>
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Weight:</span></th>
                        <td><input class="pull-left profile_input" type="text" class="form-control" name="weight" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->weight; ?>"></td>
                      </tr>
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Orientation:</span></th>
                        <td><select class="form-control bio_form_profile pull-left" id="sel1"  name="orientation">
                          <?php foreach($orientations as $orientation) : ?>
                        <option class="pull-left" value="<?php echo $orientation['id']; ?>"><?php echo $orientation['name']; ?></option>
                      <?php endforeach; ?>
                    </select></td>
                      </tr>
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Service for:</span></th>
                        <td><select class="form-control bio_form_profile pull-left" id="sel1"  name="servicefor">
                          <?php foreach($servicefor as $service) : ?>
                          <option class="pull-left" value="<?php echo $service['id']; ?>"><?php echo $service['name']; ?></option>
                          <?php endforeach; ?>
                        </select></td>
                      </tr>
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Hair color:</span></th>
                        <td><select class="form-control bio_form_profile pull-left" id="sel1"  name="hair">
                          <?php foreach($hairs as $hair) : ?>
                          <option class="pull-left" value="<?php echo $hair->id; ?>"><?php echo $hair->name; ?></option>
                          <?php endforeach; ?>
                        </select></td>
                      </tr>
                      <tr class="tr_profile">
                        <th scope="row" class="th_profile"><span>Type:</span></th>
                        <td><select class="form-control bio_form_profile pull-left" id="sel1"  name="type">
                          <?php foreach($types as $type) : ?>
                          <option class="pull-left" value="<?php echo $type->id; ?>"><?php echo $type->name; ?></option>
                          <?php endforeach; ?>
                        </select></td>
                      </tr>
                      <tr class="tr_profile">
                          <th scope="row" class="th_profile"><span>Mon - Fri</span></th>
                          <td><select class="form-control bio_form_profile sel_mon" id="monfri_start" name="monfri_start">
                            <?php foreach($working_hours as $hour) : ?>
                            <option value="<?php echo $hour['hours']; ?>"><?php echo $hour['hours']; ?></option>
                          <?php endforeach; ?>
                        </select> <i class="fas fa-arrows-alt-h" style="font-size:9px;padding-top:13px;"></i> <select class="form-control bio_form_profile sel_mon" style="float:right;" id="monfri_end"  name="monfri_end">
                              <?php foreach($working_hours as $hour) : ?>
                            <option value="<?php echo $hour['hours']; ?>"><?php echo $hour['hours']; ?></option>
                          <?php endforeach; ?>
                          </select></td>
                      </tr>
                      <tr class="tr_profile">
                          <th scope="row" class="th_profile"><span>Weekend</span></th>
                          <td><select class="form-control bio_form_profile sel_mon" id="weekend_start" style="" name="weekend_start">
                            <?php foreach($working_hours as $hour) : ?>
                            <option value="<?php echo $hour['hours']; ?>"><?php echo $hour['hours']; ?></option>
                          <?php endforeach; ?>
                        </select> <i class="fas fa-arrows-alt-h" style="font-size:9px;padding-top:13px;"></i> <select class="form-control bio_form_profile sel_mon" style="float:right;" id="weekend_end" name="weekend_end">
                              <?php foreach($working_hours as $hour) : ?>
                            <option value="<?php echo $hour['hours']; ?>"><?php echo $hour['hours']; ?></option>
                          <?php endforeach; ?>
                          </select></td>
                      </tr>
                    </tbody>
                    </table>
                    <div class="div_btn_sbm">
                    <button type="submit" class="btn btn-primary aa">Submit</button>
                      <a href="<?php echo site_url('/girls/profile/'.$girl->slug); ?>" class="btn btn-cancel" style="border-radius:3px;">Cancel</a>
                    </div>
                    </form>


             </div>
         </div>
     </div>
     <script src="<?php echo base_url(); ?>assets/js/validate.js"></script>
    <!-- /.container -->
    <script>
    if ($(window).width() < 992) {
$(function() {
    let elmnt = document.getElementById('scroll_to');
    elmnt.scrollIntoView(true);
});
}
let V = new Validate("update_bio");
V.setRules({
    username:{
        name: "username",
        rule: "chars|max:16",
    },
    weight:{
        name: "weight",
        rule: "numbers|max:3",
    },
    height:{
        name: "height",
        rule: "numbers|max:3",
    },
});
V.run();
    </script>
