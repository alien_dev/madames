<?php $this->load->view('templates/header.php', ['USER' => $USER ]); ?>
<?php $this->load->view('templates/confirmation.php', []); ?>

<!-- Page Content -->
<div class="container container-holder">
  <?php
    echo $msg;
    $this->load->view($view, $data);
  ?>
</div>

<?php $this->load->view('templates/footer.php'); ?>
