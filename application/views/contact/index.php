<div class="container container-holder">
<section class="section">

    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-5 abc">Contact us</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
        matter of hours to help you.</p>

    <div class="row row_contact">

        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5 contact_row_left">
            <form method="post" action="<?=base_url('contact/send')?>" enctype="multipart/form-data" id="contact_form">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class="">Your name</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="">Your email</label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <label for="subject" class="">Subject</label>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="message" rows="5" class="form-control md-textarea"></textarea>
                            <label for="message">Your message</label>
                        </div>

                    </div>
                </div>
                <!--Grid row-->



            <div class="text-center text-md-left">
                <input class="btn btn-primary aa" type="submit" value="Send" />
            </div>
            </form>
            <div class="status"></div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center contact_row_right">
            <ul class="list-unstyled mb-0">
                <li><i style="font-size:22px;margin-left:7px;" class="icon-m-location"></i>
                    <p style="margin-top:4px;margin-bottom:0;">Hintergasse 3</p>
                    <p> CH - 7204 Untervaz</p>
                </li>

                <li><i style="color:#990000;font-size:22px;margin-bottom:4px;" class="fas fa-phone-square"></i>
                    <p style="margin-bottom:15px;"> + 41 (0)79 289 45 32</p>

                </li>
                <li><i style="color:#990000;font-size:22px;margin-bottom:4px" class="fas fa-home"></i>
                    <p style="margin-bottom:15px;">+ 41 (0)81 322 74 36</p>

                </li>

                <li><i style="color:#990000;font-size:22px;" class="fa fa-envelope"></i>
                    <p>info@madames.ch</p>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>

</section>
<!--Section: Contact v.2-->
</div>
<!-- /.container -->
<script src="<?php echo base_url(); ?>assets/js/validate.js"></script>
<script >
    let V = new Validate("contact_form");
    V.setRules({
        email:{
            name: "E-mail",
            rule: "required|email|max:64",
        },
        name:{
            name: "Name",
            rule: "required|chars|max:64"
        },
        message:{
            name: "Message",
            rule: "required|chars|max:64"
        },
    });
    V.run();

</script>
