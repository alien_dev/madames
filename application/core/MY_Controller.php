<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Lib\Security;
use Lib\Alert;
#require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends CI_Controller{
	public $layout = "layouts/madames_layout";
	public $tab_title = "Madames.ch";
	public $page_title = "Madames";
	public $page_description = "Madames";
	protected $response_data = [];

	public $scripts = [];
	protected $USER = null;
	protected $madames = [
		'loader' => true
	];

	public function __construct()
	{
		parent::__construct();
		if($this->config->item('url_activate'))
		{
			if (!isset($_SESSION['madames'])) {
				die('ERROR');
			}
		}

		$this->USER = $this->ion_auth->user()->row();
		if (!empty($this->USER)) {
			$this->USER->group_name = $this->ion_auth->get_users_groups()->result();
		}

		if(!isset($_SESSION['user_session_id']))
		{
			$_SESSION['user_session_id'] = Security::tokenCode(8, true);
		}

		//echo "<br><br>"; printr($this->USER);
	}

	protected function render($view, $data=null, $return_html=false)
	{
		if (strpos($view, '/') === false) {
			$controller_name = lcfirst(get_class($this));
			$view = $controller_name."/".$view;
		}

		if ($this->USER !== null) {
            //Vraca prvu pronadjenu grupu kojoj korisnik pripada. Revidirati
			$this->USER->group_desc = $this->ion_auth->get_users_groups()->row()->description;
			$this->USER->group_name = $this->ion_auth->get_users_groups()->row()->name;
			$this->USER->group_id = $this->ion_auth->get_users_groups()->row()->id;
		}

		$layout_data['tab_title'] = $this->tab_title;
		$layout_data['page_title'] = $this->page_title;
		$layout_data['page_description'] = $this->page_description;
		$layout_data['USER']  = $this->USER;
		$layout_data['madames']  = $this->madames;
		$layout_data['scripts']  = $this->scripts;
		$layout_data['msg'] = Alert::getMessage();
		$layout_data['view']  = $view;
		$layout_data['data']  = $data;

		if ($this->layout === null)
		{
			return $this->load->view($view, array_merge($data, $layout_data), $return_html);
		}
		else
		{
			return $this->load->view($this->layout, $layout_data, $return_html);
		}
	}

	protected function model($full_name)
	{
		if(strpos($full_name,'/') !== false)
		{
			$model_name = explode("/", $full_name)[1];
		}
		else $model_name = $full_name;
        $this->load->model($full_name);
        return $this->{$model_name};
	}

	protected function setResponse($msg)
	{
		$this->response_data = array_merge($this->response_data, $msg);
	}

	protected function response($msg=null)
	{
		if($msg) $this->setResponse($msg);
		echo json_encode($this->response_data, false);
	}


}

#include 'MY_AdminController.php';
include 'Cli_Controller.php';


?>
