<?php

use Lib\Alert;

if (!isset($active_menu))
    $active_menu = '';
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="base_url" content="<?=base_url();?>">
        <meta http-equiv="expires" content="Mon, 26 Jul 1997 05:00:00 GMT"/> <meta http-equiv="pragma" content="no-cache" />

        <title><?= $tab_title ?></title>

        <!-- Bootstrap core CSS -->
        <link href="" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/fontawesome/css/all.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/grid.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/icomoon/style.css">
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>assets/css/shop-homepage.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/madames.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/croppie.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/madames-init.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/lib/moment.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/lib/countdown.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/script.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/lib/ajax.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/lib/vue.js"></script>
    </head>

    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
            <div class="container">
              <?php if ($this->ion_auth->logged_in()) : ?>
                  <a class="avatar_login_a navbar-toggler" href="<?php echo base_url(); ?>account/logout"><i class="fas fa-power-off" style="color:white;font-size:31px;margin-left:.05rem;margin-bottom:3px;margin-top: 3px;cursor:pointer;"></i><br/><p>Logout</p></a>
            <?php else: ?>
                <a class="avatar_login_a navbar-toggler" href="<?php echo base_url(); ?>account/login"><img src="<?php echo base_url(); ?>assets/images/CrniProfil.png" class="login_avatar"><br/><p>Login</p></a>
            <?php endif; ?>
                <a class="navbar-brand" href="/"><img src="<?php echo base_url(); ?>assets/images/LOGO.png" class="logo"></a>

              <span class="tog_sp">
                <button class="navbar-toggler toggler_nav" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                     <i class="fas fa-bars" style="color:white;font-size:19px;"></i>
                </button>
                <p class="menu_p">Menu</p>
              </span>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto" style="margin-top:20px;">

                        <li class="nav-item <?= activeMenu(['Home/index', 'girls/regions']) ?>" id="link1">
                            <a class="nav-link first" href="<?php echo base_url(); ?>">Girls</a>
                        </li>
                        <img src="<?php echo base_url(); ?>assets/images/line.png" class="line_img">
                        <li class="nav-item <?= activeMenu(['Clubs/index', 'clubs/regions']) ?>" id="link3">
                            <a class="nav-link" href="<?php echo base_url(); ?>Clubs">Clubs</a>
                        </li>
                        <img src="<?php echo base_url(); ?>assets/images/line.png" class="line_img">
                        <li class="nav-item <?= activeMenu('Contact/index') ?>" id="link4">
                            <a class="nav-link" href="<?php echo base_url(); ?>Contact">Contact</a>
                        </li>
                        <img src="<?php echo base_url(); ?>assets/images/line.png" class="line_img">
                        <li class="nav-item li_mar <?= activeMenu('About/index') ?>" id="link5">
                            <a class="nav-link" href="<?php echo base_url(); ?>About">About us</a>
                        </li>
                        <li style="border-left: 2px solid #999;"></li>
                        <?php if (!$this->ion_auth->logged_in()) : ?>
                            <li class="members_li nav-item <?= activeMenu(['Account/login', 'Girls/index', 'Account/register']) ?>" id="link6">
                                <a class="nav-link" style="font-weight:bold;" href="<?php echo base_url(); ?>account/login"><img src="<?php echo base_url(); ?>assets/images/CrniProfil.png" class="login_avatar_comp">Members</a>
                            </li>
                            <img src="<?php echo base_url(); ?>assets/images/line.png" class="line_img">
                        <?php endif; ?>
                        <li class="nav-item <?= activeMenu('Price/index') ?>" id="link7" style="border-right:0;">
                            <a class="nav-link last" href="<?php echo base_url(); ?>price">Price</a>
                        </li>
                        <?php if ($USER) : ?>
                          <?php if($USER->group_id == 2) : ?>
                          <img src="<?php echo base_url(); ?>assets/images/line.png" class="line_img">
                            <li class="nav-item
                            <?=
                            activeMenu([
                                'girls/profile',
                                'girls/prices',
                                'girls/bio',
                                'girls/services',
                                'girls/contact',
                            ])
                            ?>"
                                id="link6">

                                <a class="nav-link" style="font-weight:bold;" href="<?php echo base_url(); ?>girls/profile/<?php echo $USER->slug; ?>"><?php echo $USER->username; ?></a>
                                <span id="counter_messages"></span>
                            </li>
                          <?php else: ?>
                            <img src="<?php echo base_url(); ?>assets/images/line.png" class="line_img">
                              <li class="nav-item
                              <?=
                              activeMenu([
                                  'clubs/profile',
                              ])
                              ?>"
                                  id="link6">

                                  <a class="nav-link" style="font-weight:bold;" href="<?php echo base_url(); ?>clubs/profile/<?php echo $USER->slug; ?>"><?php echo $USER->username; ?></a>
                                  <span id="counter_messages"></span>
                              </li>
                            <?php endif;?>
                            <img src="<?php echo base_url(); ?>assets/images/line.png" class="line_img">
                            <span id="counter_messages"></span>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>account/logout">Logout</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
        </nav>
        <?php if($madames['loader']){ ?>
            <div id="m-loader" class="m-overlay">
                <div>
                    <img src="<?=base_url();?>assets/images/loading4.gif">
                </div>
            </div>
        <?php } ?>
