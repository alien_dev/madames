<!-- Page Content -->
  <div class="login-form login_container">
    <?php
     echo form_open('girls/login', ['id'=>'login']);
     ?>
        <div class="login_div_photo">
        <img class="login_avatar_photo" src="<?php echo base_url(); ?>assets/images/Plaviprofil.png">
      </div>
        <h2 class="text-center">Sign In</h2>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username" name="username" >
                <div id="error_username"></div>
            </div>

            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password" >
                <div id="error_password"></div>
            </div>

        <div class="form-group login_button">
            <button type="submit" class="btn btn-block aa">Log in</button>
        </div>
        <h5 class="h5_login">Or</h5>
        <a class="btn btn-block sign_up_button" href="<?php echo base_url(); ?>girls/register">Create account</a>
    <?php echo form_close(); ?>

    <p class="text-center small">You forget a password? <span>Click here</span></p>
</div>

<script >
    let V = new Validate("login");
        V.setRules({
            identity:{
                name: "Username",
                rule: "required|chars|max:64",
            },
            password:{
                name: "Password",
                rule: "required|chars|max:64"
            }
        });
        V.run();

</script>
