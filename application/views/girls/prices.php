<style type="text/css">
    .time-inputs{width: 100px}
    .time-label{text-align: right; padding-right: 3px;}
    .user-price{padding: 10px;}
    .user-price:hover{background-color: #ECECEC; border:1px solid lightgrey;}
</style>
<div class="row custom_profile_row">
    <div class="col-lg-4 order-lg-1 text-center img_div_pro">
        <img src="<?php echo base_url(); ?>assets/images/girls/<?= $slug; ?>.jpg" class="mx-auto img-fluid img-circle d-block img_profile" alt="avatar">
        <div class="row">
          <div class="profile_description">
              <p class="user-description"><?=$girl->description?></p>
          </div>
        </div>
    </div>
    <div class="col-lg-8 order-lg-2 table_profile_tab">
        <ul class="nav nav-tabs">
            <li class="nav-item nav_item_profile" id="scroll_to">
                <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Bio</a>
            </li>
        </ul>
        <div class="tab-content py-4">
            <h3 class="mb-3 profile_h3"><?php echo $girl->username; ?></h5>

            <h5 class="mb-3 h3pri">New price: </h5>

            <?php echo form_open("madames/handleform/", ['id' => 'price_form']); ?>
                <input type="hidden" name="_action"  value="save">
                <input type="hidden" name="_model"  value="usertoprice">
                <!-- Za multi form trenutno se definise callback sa redirect -->
                <!-- <input type="hidden" name="_redirect"  value="<?="girls/profile/" . $girl->slug ?>"> -->
                <input type="hidden" name="user_id"  value="<?php echo $girl->id; ?>">
                <input type="hidden" name="user_slug"  value="<?php echo $girl->slug; ?>">
                <div class="price_div">
                <table class="price-table">
                    <tr class="form_row">
                        <td>
                            <div class="form-group">
                                <div id="error_days"></div>
                                <input type="number" min="0" class="time-inputs" name="time" value="" placeholder="Time">
                            </div>

                        </td>
                        <td>
                            <div class="form-group" reference="target" style="margin-left:5px;margin-right:15px;">
                                <select name="time_type_id">
                                    <?php foreach ($times as $time) : ?>
                                        <option value="<?php echo $time->id; ?>"><?php echo $time->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </td>

                        <td>
                            <div class="form-group">
                                <div id="error_price"></div>
                                <input type="number" min="0" class="time-inputs" name="price" value="" placeholder="Price">
                            </div>
                        </td>
                        <td>
                            <div class="form-group" style="margin-left:5px;">
                                <select name="currency_id">
                                <?php foreach ($currencies as $currency) : ?>
                                    <option value="<?=$currency->id;?>"><?=$currency->iso;?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                </table>
                  <i class="fas fa-plus-circle" style="color:black;font-size:23px;" onclick="myFunction()"></i>

              </div>

                <br>
                <div class="div_btn_sbm">
                <button type="submit" class="btn btn-primary aa">Submit</button>
                <a href="<?php echo site_url('/girls/profile/' . $girl->slug); ?>" class="btn btn-cancel" style="border-radius:3px;">Cancel</a>
                  </div>
            </form>

        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/forms.js"></script>
<script>

    /* let V = new Validate("price_form");
        V.setRules({
        price:{
        name: "Price",
        rule: "required|number|max:11|min_num:1"
        },
        });
        V.run(); */

    function myFunction() {
        var itm = document.querySelector(".form_row");//mora form_row zbog funkcie multiForms
        var cln = itm.cloneNode(true);
        var table = document.getElementsByClassName('price-table')[0];
        table.appendChild(cln);
    }

    multiForms(
        'price_form', //id forme
        ['time', 'time_type_id', 'price', 'currency_id'], //polja za upis
        ['user_id', 'user_slug'], // dodatna polja (relaciona)
        function(result){
            if(result){
                window.location.replace("<?=base_url('girls/profile/' . $girl->slug);?>")
            }
        }
    );

    if ($(window).width() < 992) {
      $(function() {
          let elmnt = document.getElementById('scroll_to');
          elmnt.scrollIntoView(true);
      });
      }


</script>
