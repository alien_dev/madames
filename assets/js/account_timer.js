var created_on = document.getElementById('girl-created_on').value;
var account_paused_on = document.getElementById('girl-account_paused_on').value;
var user_id = document.getElementById('girl-id').value;
var user_status = document.getElementById('girl-status').value;

var account_settings = document.getElementsByClassName('account-settings')[0];
var msg = account_settings.querySelector('.account-settings-msg');
var dots = msg.querySelector(".dots");
var time_left = account_settings.querySelector('.account-time-left');
var status_msg = account_settings.querySelector('.info-item-value.status');
var enable = account_settings.querySelector('.account-option.enable-account');
var disable = account_settings.querySelector('.account-option.disable-account');
var play_btn = enable.querySelector('button');
var stop_btn = disable.querySelector('button');
var saldo = account_settings.querySelector('.saldo.timer');

var end_date = moment.unix(created_on).add(_m.account_periods.a, "days");
var begin = moment.unix(time_left.dataset.created_on);
var diff = end_date.diff(begin, 'seconds');
var duration = moment.duration(diff, 'seconds');
var expired_account = end_date.diff(moment()) < 0;
var limit_before_continue = null;
var paused = account_paused_on ? moment.unix(account_paused_on).format("YYYY-MM-DD HH:mm:ss") : null;

var timer = null;
var lbc = null;
//!!! Obavezno izbrisati upotrebu set funkcije ili mozda celu promenljivu zbog bezbednosti
var time_limit_to_continue = {
    value: 24,
    unit: 'hours',
    set: function(value, unit) {
        this.value = value;
        this.unit = unit;
        lbc.stop();
        paused = moment().format("YYYY-MM-DD HH:mm:ss");
        calculateTime();
    }
}

function calculateTime() {
    if (expired_account) {
        disable.style.display = 'none';
        enable.style.display = 'none';
        saldo.style.display = 'none';
        status_msg.innerText = 'Account expired';
        time_left.innerText = "Account expired";
    } else {
        if (user_status == _m.account_statuses.DISABLED) {
            enable.style.display = 'inline-block';
            disable.style.display = 'none';
            status_msg.classList.add('text-warning');
            status_msg.innerText = 'Disabled';
            if (paused && account_paused_on) {
                //Ima bug u proracunu vremena
                //countdown(end_date.format("YYYY-MM-DD"), ['months', 'days', 'hours', 'minutes', 'seconds']).get_remaning(paused);
                var paused_moment = moment.unix(account_paused_on);
                var paused_diff = end_date.diff(paused_moment, 'seconds');
                var paused_duration = moment.duration(paused_diff, 'seconds');
                timer = countdown(end_date.format("YYYY-MM-DD HH:mm:ss"), ['months', 'days', 'hours', 'minutes', 'seconds']).setTime({
                    months: paused_duration.months(),
                    days: paused_duration.days(),
                    hours: paused_duration.hours(),
                    minutes: paused_duration.minutes(),
                    seconds: paused_duration.seconds(),
                });
                limit_before_continue = moment(paused).add(time_limit_to_continue.value, time_limit_to_continue.unit);
                lbc = countdown(limit_before_continue.format("YYYY-MM-DD HH:mm:ss"), null, {
                    begin: function() {
                        enable.style.display = 'none';
                        msg.style.display = 'block';
                    },
                    interval: function(date) {
                        msg.innerText = "You can go back online in: " + date.leftTime;
                    },
                    end: function() {
                        enable.style.display = 'inline-block';
                        msg.style.display = 'none';
                    }
                });
            }
        } else {
            enable.style.display = 'none';
            status_msg.classList.add('text-success');
            status_msg.innerText = 'Online';
            if (!expired_account) {
                timer = countdown(end_date.format("YYYY-MM-DD HH:mm:ss"), ['months', 'days', 'hours', 'minutes', 'seconds']);
            }
        }
        time_left.innerText = end_date.fromNow() + " (" + end_date.format("DD MMMM YYYY HH:mm:ss") + ")"; //duration.asDays();
    }
}
calculateTime();
play_btn.onclick = function(e) {
    msg.innerText = 'Enabling account please wait ';
    msg.style.display = 'block';
    enable.style.display = 'none';
    var dots_loader = dotsLoader(msg);
    setTimeout(() => {
        disable.style.display = 'inline-block';
        status_msg.classList.remove('text-warning');
        status_msg.classList.add('text-success');
        status_msg.innerText = 'Online';
        msg.style.display = 'none';
        clearDotsLoader(dots_loader);
        timer = countdown(end_date, ['months', 'days', 'hours', 'minutes', 'seconds']);
        updateStatus(_m.account_statuses.ENABLED);
        account_paused_on = "";
    }, 3000);
}
stop_btn.onclick = function(e) {
    confirmAction({
        modal_title: "Pause account",
        modal_description: "After 24 hours, you will be able to activate your account again."
    }, function(modal) {
        disable.style.display = 'none';
        status_msg.classList.remove('text-success');
        status_msg.classList.add('text-warning');
        status_msg.innerText = 'Disabled';
        timer.stop();
        account_paused_on = moment().unix();
        paused = moment().format("YYYY-MM-DD HH:mm:ss");
        limit_before_continue = moment(paused).add(time_limit_to_continue.value, time_limit_to_continue.unit);
        msg.style.display = 'block';
        lbc = countdown(limit_before_continue.format("YYYY-MM-DD HH:mm:ss"), null, {
            interval: function(date) {
                msg.innerText = "You can go back online in: " + date.leftTime;
            },
            end: function() {
                enable.style.display = 'inline-block';
                msg.style.display = 'none';
            }
        });
        updateStatus(_m.account_statuses.DISABLED)
    })
}

function dotsLoader(el) {
    return {
        interval: setInterval(() => {
            el.innerHTML += ".";
        }, 500),
        el: el
    }
}

function clearDotsLoader(dots_loader) {
    clearInterval(dots_loader.interval);
    dots_loader.el.innerHTML = "";
}

function updateStatus(status) {
    AJAX.post({
            url: _m.base_url + 'madames/handleForm',
            data: {
                model_data: {
                    _action: 'update',
                    _model: 'girl_model',
                    //_redirect: _redirect ? _redirect.value : '',
                    status: status,
                    id: user_id
                }
            }
        })
        .then(function(res) {
            if (res.status == 'ok') {
                callback(true);
            } else {
                console.log(res);
            }
        })

    AJAX.post({
        url: _m.base_url + 'madames/handleForm',
        data: {
            model_data: {
                _action: 'update_or_insert',
                _model: 'UserSettings',
                //_redirect: _redirect ? _redirect.value : '',
                account_paused_at: status === _m.account_statuses.DISABLED ? moment().unix() : null,
                user_id: user_id
            }
        }
    })
}
