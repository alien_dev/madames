var countdown = function(endTime, elements, params = {}) {
    var d = new Date();
    var total_days = new Date(d.getFullYear(), d.getMonth() + 1, 0).getDate();
    var _second = 1000,
        _minute = _second * 60,
        _hour = _minute * 60,
        _day = _hour * 24,
        _month = _day * total_days,
        now = new Date(),
        endTime = new Date(endTime),
        data = null,
        timer;

    calculate = function() {
        //console.log(endTime);

        var now = new Date(),
            remaning = endTime.getTime() - now.getTime();
        if (isNaN(endTime)) {
            console.log('Invalid date/time');
            return;
        }

        if (remaning <= 0) {
            // clear time when time ends
            clearInterval(timer);
            //callback
            if (typeof params.end === 'function') {
                params.end();
            }
            console.log('Remaining time 0');
        } else {
            if (!timer && !params.hasOwnProperty('get_remaning')) {
                timer = setInterval(calculate, _second);
            }
            if (typeof params.begin === 'function') {
                params.begin();
            }
            data = calculateTime(remaning);
            for (x in data) {
                if (x == 'months') {
                    data[x] = data[x];
                } else {
                    data[x] = ('00' + data[x]).slice(-2);
                }
            }
            data.leftTime = data.hours + ":" + data.minutes + ":" + data.seconds;

            if (elements && elements.length) {
                for (x in elements) {
                    var x = elements[x];
                    document.getElementById(x).innerHTML = data[x];
                }
            }

            if (params.interval) {
                params.interval(data);
            }
        }
    };

    function calculateTime(remaning) {
        return {
            'months': Math.floor(remaning / _month),
            'days': Math.floor(remaning / _day),
            'hours': Math.floor((remaning % _day) / _hour),
            'minutes': Math.floor((remaning % _hour) / _minute),
            'seconds': Math.floor((remaning % _minute) / _second)
        }
    }

    calculate();

    return {
        getData: function() {
            return data;
        },
        stop: function() {
            clearInterval(timer);
        },
        get_remaning: function(time_from = null) {
            this.stop();
            var from = new Date();
            if (params) {
                if (params.time_from) {
                    from = new Date(params.time_from);
                }
            }
            if (time_from) {
                from = new Date(time_from);
            }
            var remaning = endTime.getTime() - from.getTime();
            if (remaning <= 0) {
                console.log('get_remaning: Remaining time 0');
            }
            data = calculateTime(from.getTime())
                //console.log("from " + from.getTime());
            if (elements.length) {
                for (x in elements) {
                    var x = elements[x];
                    if (x == 'months') {
                        data[x] = data[x];
                    } else {
                        data[x] = ('00' + data[x]).slice(-2);
                    }
                    document.getElementById(x).innerHTML = data[x];
                }
            }
        },
        setTime(time) {
            this.stop();
            if (elements.length) {
                for (x in elements) {
                    var x = elements[x];
                    document.getElementById(x).innerHTML = time[x];
                }
            }
        }
    };
}

var obj = function(params = null) {

    return {
        data: params,
        name: "HAAA"
    }
}