<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'Home';

/*$route['clubs'] = 'clubs/index';
$route['girls/update_bio'] = 'girls/update_bio';
$route['girls/update_services'] = 'girls/update_services';
$route['girls/register'] = 'girls/register';
$route['girls/login'] = 'girls/login';
$route['girls/logout'] = 'girls/logout';
$route['girls/upload_images'] = 'girls/upload_images';
$route['girls/update_profile_photo'] = 'girls/update_profile_photo';
#$route['girls/(:any)'] = 'girls/view_profile/$1';
$route['girls'] = 'girls/index';

$route['girls/regions/(:any)'] = 'girls/regions/$1';
$route['girls/regions/(:any)/(:any)'] = 'girls/regions/$1/$2';
$route['girls/regions/(:any)/(:any)/(:any)'] = 'girls/regions/$1/$2/$3';
$route['girls/regions_new/(:any)'] = 'girls/regions_new/$1';
$route['girls/regions_new/(:any)/(:any)'] = 'girls/regions_new/$1/$2';
#$route['(:any)'] = 'pages/view/$1';
#$route['pages/view'] = 'pages/view';
$route['girls/prices/(:any)'] = 'girls/prices/$1';
$route['girls/update_price/(:any)'] = 'girls/update_price/$1';
$route['girls/new_price/(:any)'] = 'girls/new_price/$1';
$route['girls/delete_price/(:any)/(:any)'] = 'girls/delete_price/$1/$2';*/
/*$route['pages/(:any)/(:any)/(:any)'] = 'pages/view/$1/$2/$3';
$route['pages/(:any)/(:any)'] = 'pages/view/$1/$2';
$route['pages/(:any)'] = 'pages/view/$1/';*/


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
