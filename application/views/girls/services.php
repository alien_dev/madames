<?php
  function checkedService($user_to_services, $service_id) {
    foreach ($user_to_services as $user_to_service){
      if ($service_id === $user_to_service['service_id']) {
        return "checked";
      }
    }
        return "";
  }
  function checkedStatus($user_to_services, $service_id) {
    foreach ($user_to_services as $user_to_service){
      if ($service_id === $user_to_service['service_id'] && $user_to_service['status'] == 1) {
        return "checked";
      }
    }
        return "";
  }
 ?>
     <div class="row custom_profile_row">
       <div class="col-lg-4 order-lg-1 text-center img_div_pro">
           <img src="<?php echo base_url(); ?>assets/images/girls/<?=$girl->slug;?>.jpg" class="mx-auto img-fluid img-circle d-block img_profile" alt="avatar">
           <div class="row">
             <div class="profile_description">
                 <p class="user-description"><?=$girl->description?></p>
             </div>
           </div>
       </div>
         <div class="col-lg-8 order-lg-2 table_profile_tab">
             <ul class="nav nav-tabs">
                 <li class="nav-item nav_item_profile" id="scroll_to">
                     <a href="" data-target="#messages" data-toggle="tab" class="nav-link active">Services</a>
                 </li>
             </ul>
             <div class="tab-content py-4">
               <h3 class="mb-3 profile_h3"><?php echo $girl->username;?></h5>
                   <p style="margin-bottom:20px;margin-left:15px;">Check on services: </p>
                   <?php echo form_open("girls/update_services/".$girl->slug); ?>
                   <?php foreach($services as $service) : ?>
                      <label>
                        <input style="margin-left:15px;" type="checkbox" class="a" name="service_id[]" value="<?php echo $service->id; ?>" <?=checkedService($user_to_services, $service->id);?> >
                        <?php echo $service->name; ?>
                      </label>
                      <br>
                    <?php endforeach; ?>

                    <input type="hidden" name="id"  value="<?php echo $girl->id; ?>">
                    <input type="hidden" name="username"  value="<?php echo $girl->username; ?>">
                    <div class="div_btn_sbm">
                   <button type="submit" class="btn btn-primary aa">Submit</button>
                      <a href="<?php echo site_url('/girls/profile/'.$girl->slug); ?>" class="btn btn-cancel" style="border-radius:3px;">Cancel</a>
                    </div>
                   </form>
         </div>
     </div>
    </div>
<script>
if ($(window).width() < 992) {
$(function() {
let elmnt = document.getElementById('scroll_to');
elmnt.scrollIntoView(true);
});
}
</script>
