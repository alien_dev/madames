function confirmDelete(title, url) {
    console.log(title);
    var modal = $('#modal');
    var modal_title = modal.find(".modal-title");
    var modal_text = modal.find(".modal-text");
    var modal_description = modal.find(".modal-description");
    var delete_yes = modal.find(".yes");
    var delete_no = modal.find(".no");

    modal_title.text("Delete " + title);
    modal_text.text("Are you sure?");
    modal_description.text("");

    //modal_text.text(text);
    delete_yes.attr("href", url);
}

function confirmAction(params, callback) {
    var modal = $('#modal');
    var modal_title = modal.find(".modal-title");
    var modal_text = modal.find(".modal-text");
    var modal_description = modal.find(".modal-description");

    var action_yes = modal.find(".yes");
    var action_no = modal.find(".no");
    action_yes.attr("href", "#");
    modal_text.text("Are you sure?");
    modal_description.text("");
    if (typeof params == "string") {
        modal_title.text(params);
    } else if (typeof params == "object") {
        if (params.modal_title) modal_title.text(params.modal_title);
        if (params.modal_text) modal_text.text(params.modal_text);
        if (params.modal_description) modal_description.text(params.modal_description);
    }
    modal.modal('show');
    action_yes.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        callback();
        modal.modal('toggle');
    });
}