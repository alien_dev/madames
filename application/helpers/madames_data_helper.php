<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('printr'))
{
	function printr($var){
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}	
}

function activeMenu($selected_item = '', $class = 'active')
{
	$CI =& get_instance();
	$called_controller = $CI->router->class;
	$called_method = $CI->router->method;
	$url = strtolower($called_controller.'/'.$called_method);
	#printr($url);die();
	if (is_array($selected_item)) 
	{
		foreach ($selected_item as $item) 
		{
			if (strtolower($item) == $url) 
			{
				return $class;
			}
		}
	}
	else
	{
		if (strtolower($selected_item) == $url) 
		{
			return $class;
		}
	}
	
	return '';
	/*if ($url == $selected_menu) {
		return $class;
	}
	return '';*/
}

function activeClass($item_a, $item_b, $class = "active-region")
{
	if ($item_a == $item_b) {
		return $class;
	}
	return '';
}


 ?>