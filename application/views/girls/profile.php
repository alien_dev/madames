<?php
use Carbon\Carbon;
$date_time_now = Carbon::now();
$begin_date = Carbon::createFromTimestamp($girl->created_on);
$end_date = $begin_date->copy()->addDays(30);
$expired = $date_time_now->greaterThanOrEqualTo($end_date);
$this->session->set_userdata('referred_from', current_url());
//printr($USER);
?>
<?php header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); header('Cache-Control: no-store, no-cache, must-revalidate'); header('Cache-Control: post-check=0, pre-check=0', FALSE); header('Pragma: no-cache'); ?>
<div class="row custom_profile_row">
    <div class="col-lg-4 order-lg-1 text-center img_div_pro">
        <img src="<?=(!empty($girl->profile_photo))? site_url('/assets/images/girls/').$girl->profile_photo :site_url('/assets/images/default.jpg');?>?nocache=<?=time()?>" class="mx-auto img-fluid img-circle d-block img_profile" alt="avatar">
        <?php if ($this->session->userdata('slug') == $girl->slug) : ?>
            <form action="<?= base_url(); ?>girls/update_profile_photo" class="profile_photo_edit" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <label class="fileUpload btn btn-primary bio_button filter_btn bor_btn">
                    <span class="upload_span">Upload your profile picture</span>
                    <input type="file" name="userfile" id="profile_photo" class="upload upload_profile">
                    <input type="hidden" name="cropped_b64" id="cropped_image">
                </label></br>
                <input class="btn btn-primary show_upload_profile" id="save_profile" style="margin:10px;" type="submit" name="" value="Save">
            </form>
        <?php endif; ?>
        <div class="row">
                <?php if ($profile_owner) { ?>
                    <div class="profile_description">
                        <textarea class="form-control edit-description js-auto-size" id="desc_autosize" rows="5" data-id="<?=$girl->id?>"><?=$girl->description;?></textarea>
                        <span class="user-description"><?=nl2br($girl->description);?></span>
                        <!-- <img class="edit-description-img" src="<?php echo base_url(); ?>assets/images/editRed.png" alt="" style="float:right;width:24px;height:24px;" id="edit_desc"> -->
                        <i class="far fa-edit edit-description-img" style="color:#990000;float:right;font-size:24px;" id="edit_desc"></i>
                    </div>
                <?php } else if (!empty($girl->description)){ ?>
                    <div class="profile_description">
                        <textarea class="form-control edit-description" rows="1" data-id="<?=$girl->id?>"><?=$girl->description?></textarea>
                        <span class="user-description"><?=nl2br($girl->description);?></span>
                    </div>
                <?php } ?>
        </div>
    </div>
    <div class="col-lg-8 order-lg-2 table_profile_tab" style="padding-left:10px;">
        <ul class="nav nav-tabs">
            <li class="nav-item nav_item_profile">
                <a href="" data-target="#profile" data-toggle="tab" class="nav-link active nav_phone">Bio</a>
            </li>
            <li class="nav-item nav_item_profile">
                <a href="#" data-target="#messages" data-toggle="tab" class="nav-link">Services</a>
            </li>
            <li class="nav-item nav_item_profile">
                <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Price</a>
            </li>
            <li class="nav-item nav_item_profile">
                <a href="" data-target="#contact" data-toggle="tab" class="nav-link">Contact</a>
            </li>
            <li class="nav-item nav_item_profile">
                <a href="" data-target="#gallery" data-toggle="tab" class="nav-link">Gallery</a>
            </li>
            <?php if($profile_owner): ?>
                <li class="nav-item nav_item_profile last_profile_tab">
                    <a href="" data-target="#settings" data-toggle="tab" class="nav-link" style="font-weight:600;"><i class="fas fa-cog" style="margin-right:3px;"></i><span class="sett_text">Settings</span></a>
                </li>
            <?php endif ?>
        </ul>
        <div class="tab-content py-4">
            <div class="tab-pane active" id="profile">
                <h3 class="mb-3 profile_h3"><?php echo $girl->username; ?></h3>
                <table class="table profile_table">
                    <tbody class="profile_table">
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">City</span></th>
                            <td><span class="mobile_td"><?php echo $girl->city; ?></span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Age</span></th>
                            <td><span class="mobile_td"><?php echo $girl->age; ?></span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Height</span></th>
                            <td><span class="mobile_td"><?php echo $girl->height; ?> cm</span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Weight</span></th>
                            <td><span class="mobile_td"><?php echo $girl->weight; ?> kg</span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Hair color</span></th>
                            <td><span class="mobile_td"><?php echo $girl->hair; ?></span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Service for</span></th>
                            <td><span class="mobile_td"><?php echo $girl->servicefor; ?></span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Type</span></th>
                            <td><span class="mobile_td"><?php echo $girl->type; ?></span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">TS</span></th>
                            <td><span class="mobile_td"><?php if($girl->ts == 0){
                              echo "No";
                            }else{
                              echo "Yes";
                            } ?></span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Orientation</span></th>
                            <td><span class="mobile_td"><?php echo $girl->orientation; ?></span></td>
                        </tr>
                        <?php if ($profile_owner) : ?>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Created on</span></th>
                            <td><?=Carbon::createFromTimestamp($girl->created_on)->format("d F Y");?></td>
                        </tr>
                        <?php endif; ?>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Mon - Fri</span></th>
                            <?php if (isset($girl->monfri_start)) : ?>
                            <td><?php echo $girl->monfri_start; ?> - <?php echo $girl->monfri_end; ?></td>
                            <?php else: ?>
                            <td>00 - 24 </td>
                            <?php endif; ?>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Weekend</span></th>
                            <?php if (isset($girl->weekend_start)) : ?>
                            <td><?php echo $girl->weekend_start; ?> - <?php echo $girl->weekend_end; ?></td>
                            <?php else: ?>
                            <td>00 - 24 </td>
                            <?php endif; ?>
                        </tr>
                    </tbody>
                </table>
                <?php if ($profile_owner) : ?>
                    <a class="btn btn-primary bio_button filter_btn fil_btn_pr bor_btn" href="<?php echo base_url(); ?>girls/bio/<?php echo $girl->slug; ?>">Edit your bio <i class="far fa-edit" style="margin-left:3px;"></i></a>
                <?php endif; ?>
                <?php if ($profile_owner) : ?>
                    <a class="btn btn-primary bio_button online_btn" id="onl" style="" href="#" data-toggle="modal" data-target="#boxModal"><span class="go_on"><span class="go_in_go">Go</span></span> <span class="on_on">online</span></a>
                    <div style="clear:both;"></div>
                <?php endif; ?>
            </div>
            <div class="tab-pane" id="messages">
                <h3 class="mb-3 profile_h3"><?php echo $girl->username; ?></h3>
                <div class="row">

                    <div class="col serv_col">
                        <?php foreach ($services as $service) : ?>
                            <?php foreach ($user_to_services_one as $user_to_service): ?>
                                <?php if ($user_to_service['service_id'] == $service->id): ?>
                                    <p class="break"><img class="checked_img" src="<?php echo base_url(); ?>assets/images/checked.png" alt=""><?php echo $service->name ?></p>
                                <?php endif ?>
                            <?php endforeach ?>

                        <?php endforeach; ?>
                        <?php if ($profile_owner) : ?>
                            <a class="btn btn-primary bio_button filter_btn fil_btn_pr bor_btn" style="margin-left:0px;" href="<?php echo base_url(); ?>girls/services/<?php echo $girl->slug; ?>">Edit your services <i class="far fa-edit" style="margin-left:3px;"></i></a>
                        <?php endif; ?>
                    </div>
                    <div class="col serv_col_two">
                      <?php foreach ($services as $service) : ?>
                          <?php foreach ($user_to_services_two as $user_to_service): ?>
                              <?php if ($user_to_service['service_id'] == $service->id): ?>
                                  <p class="break"><img class="checked_img" src="<?php echo base_url(); ?>assets/images/checked.png" alt=""><?php echo $service->name ?></p>
                              <?php endif ?>
                          <?php endforeach ?>

                      <?php endforeach; ?>
                    </div>
                </div>
                <!--/row-->
            </div>
            <div class="tab-pane" id="edit">
                <h3 class="mb-3 profile_h3"><?php echo $girl->username; ?></h3>
                <table class="table profile_table">
                    <tbody class="profile_table">
                        <?php foreach ($prices as $price): ?>
                            <tr class="tr_profile">
                                <th scope="row" class="th_profile th_price">
                                    <?php echo $price->time;?>
                                    <?php echo $price->time_name;?>
                                </th>
                                <td class="price_td"><?= $price->price; ?> <?=$price->currency;?></td>
                                <?php if ($USER && $USER->username == $girl->username): ?>
                                    <td>
                                        <button class="btn btn-sm madames-btn-fix btn-outline-danger price-btn-del"
                                                onclick="confirmDelete('price', '<?= base_url(); ?>madames/handleform/delete/usertoprice/<?= $price->id; ?>/girls.profile.<?= $girl->slug; ?>/')"
                                                data-toggle="modal" data-target="#modal">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach ?>

                    </tbody>
                </table>
                <?php if ($profile_owner) : ?>
                    <a class="btn btn-primary bio_button filter_btn fil_btn_pr bor_btn" href="<?php echo base_url(); ?>girls/prices/<?php echo $girl->slug; ?>">Edit your prices <i class="far fa-edit" style="margin-left:3px;"></i></a>
                <?php endif; ?>


            </div>
            <div class="tab-pane" id="contact">
                <h3 class="mb-3 profile_h3"><?php echo $girl->username; ?></h3>
                <table class="table profile_table">
                    <tbody class="profile_table">
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Phone</span></th>
                            <td><span class="mobile_td"><?php echo $girl->phone; ?></span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Address</span></th>
                            <td><span class="mobile_td"><?php echo $girl->address; ?></span></td>
                        </tr>
                        <tr class="tr_profile">
                            <th scope="row" class="th_profile"><span class="pro">Web</span></th>
                            <td><span class="mobile_td"><?php echo $girl->web; ?></span></td>
                        </tr>
                         <tr class="tr_profile">
                          <th scope="row" class="th_profile"><span class="pro">Club</span></th>
                          <td><span class="mobile_td"><?php echo $girl->club; ?></span></td>
                        </tr>
                    </tbody>
                </table>
                <?php if ($profile_owner) : ?>
                    <a class="btn btn-primary bio_button filter_btn fil_btn_pr bor_btn" href="<?php echo base_url(); ?>girls/contact/<?php echo $girl->slug; ?>">Edit your contact <i class="far fa-edit" style="margin-left:3px;"></i></a>
                <?php endif; ?>
            </div>
            <div class="tab-pane" id="gallery">
                <div class="row gallery-container">
                  <h6 style="margin-left:auto;margin-right:auto;font-weight:600;">Images</h6>
                    <div class="tz-gallery">
                        <div class="row gallery_row">
                            <?php if ($user_images): ?>
                                <?php foreach ($user_images as $image): ?>
                                    <?php
                                    if ($image == 'next.txt') {
                                        continue;
                                    }
                                    ?>
                                    <div class="col-sm-6 col-md-4 gallery_pic">
                                        <a class="lightbox" href="<?php echo base_url(); ?>assets/images/<?= $slug . '/' . $image; ?>" >
                                            <img class="gallery_size" src="<?php echo base_url(); ?>assets/images/<?= $slug . '/' . $image; ?>" alt="" onload="setAspectRatio(this,170)">
                                        </a>
                                        <?php if ($profile_owner): ?>
                                        </br>
                                            <a href="" onclick="deleteImg('<?= base_url(); ?>girls/deleteimg/<?= $image; ?>', event)" class="btn btn-danger btn-sm del_btn del_btn_gal">Delete</a>
                                    <?php endif ?>
                                    </div>
                                <?php endforeach ?>
                            <?php else: ?>
                                <p class="p_gallery">There are no images.</p>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <div>
                    <?php if ($profile_owner): ?>
                        <form action="<?= base_url(); ?>girls/upload_images" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <div id="image-holder"></div>
                            <label class="fileUpload btn btn-primary bio_button filter_btn hide_btn_img fil_btn_pr bor_btn">
                                <span class="upload_span">Upload your gallery picture</span>
                                <input type="file" name="images[]" id="images" class="upload upload_gallery_img">
                            </label></br>

                            <input class="btn btn-primary show_upload_gallery" id="images_save" style="margin:10px;margin-left:25px;" type="submit" name="" value="Share a picture">

                        </form>
                    <?php endif ?>
                </div>
                <hr/>
                <div class="video-sett">
                 <h6 style="text-align: center;font-weight:600;">Video</h6>


                <?php if ($video): ?>
                  <div class="row">
                  <video controls class="video">
                    <source src="<?= base_url(); ?>assets/videos/<?php echo $video;?>" type="video/mp4">
                  </video>
                  </div>
                  <?php if ($profile_owner): ?>
                      <a href="" onclick="deleteVid('<?= base_url(); ?>girls/deletevid/<?= $video; ?>', event)" style="margin-top:30px;margin-bottom:25px;margin-left:15px;" class="btn btn-danger btn-sm del_btn">Delete</a>
                  <?php endif ?>
                <?php else: ?>
                  <div style="padding-left:20px;margin-bottom:30px;margin-top:38px;">
                    There are no video.
                  </div>
                <?php endif ?>

                <div class="row">
                <?php if ($profile_owner): ?>
                    <form action="<?= base_url(); ?>girls/video_upload" enctype="multipart/form-data" method="post" accept-charset="utf-8" style="margin-left:5px;">
                        <label class="fileUpload btn btn-primary bio_button filter_btn fil_btn_pr bor_btn">
                            <span class="upload_span">Upload a video</span>
                            <input type="file" name="userfile" id="fileslika" class="upload upload_gallery_video" >
                        </label></br>
                        <input class="btn btn-primary show_upload_gallery_video" style="margin:25px" type="submit" id="button" name="" value="Share a video" />
                    </form>
                <?php endif ?>
              </div>
              </div>
            </div>
            <?php if($profile_owner): ?>
                <div class="tab-pane" id="settings">
                    <div class="settings-section">
                        <div class="settings-item" style="margin-top:3px;">
                            <span class="account-btn-desc" style="font-weight:bold;">Chat:</span>
                            <div style="margin-top:8px;margin-bottom:30px;">
                              <div style="display:inline-block;float:left;margin-right:5px;margin-left:5px;" class="chat-off">Off</div>
                            <label class="switch">
                              <input type="checkbox" <?= ($girl->chat_status == '1')? 'checked':'' ?> name="chat_status" id="chat_status" onclick="updateChatStatus(<?=$USER->id?>,this)">
                              <span class="slider round"></span>
                            </label>
                            <div style="display:inline-block;margin-left:3px;" class="chat-on">On</div>
                          </div>
                            <div style="clear:both">
                            </div>
                        </div>
                        <hr>
                        <div class="settings-item" style="padding-top:8px;">
                            <span class="account-btn-desc" style="font-weight:bold;">Account:</span>
                            <div class="account-settings">
                                <div class="account-info">
                                    <div class="info-item-title">Status: <span class="info-item-value status"><?=$girl->status?></span></div>
                                    <!-- <div class="info-item-title">Time left: <span class="info-item-value time"></span></div> -->
                                </div>
                                <div class="account-options" style="margin-top:10px;margin-bottom:22px;">
                                    <div class="account-option enable-account acc-def">
                                      <div class="account-btn-desc pause_text_two">Start</div>
                                        <button class="madames-btn-fix">
                                            <i class="far fa-play-circle"></i>
                                        </button>
                                    </div>
                                    <div class="account-option disable-account acc-def">
                                      <div class="account-btn-desc pause_text">Pause</div>
                                        <button class="madames-btn-fix">
                                            <i class="far fa-pause-circle"></i>
                                        </button>
                                    </div>
                                    <div class="saldo_btn">
                                      <span class="saldo" style="display:none;"><span style="margin-bottom:3px;border-bottom:1px solid #d9d9d9;">Account expires:</span>
                                      <span class="account-time-left" data-created_on="<?=$girl->created_on?>">00:00:00</span>
                                    </span>

                                    <span class="saldo timer"><span style="margin-bottom:3px;border-bottom:1px solid #d9d9d9;"></span>
                                        <span class="hrc">Time:</span>
                                        <span class="account-time-left">
                                            <span id="months" style="display:none;"></span>
                                            <span id="days" style="font-weight:bold;"></span><span style="color:black;">d</span>
                                            <span id="hours" style="font-weight:bold;"></span><span style="color:black;">:</span><span id="minutes" style="font-weight:bold;"></span><span style="color:black;">:</span><span id="seconds" style="font-weight:bold;"></span>
                                        </span>
                                    </span>

                                  </div>

                                    <!-- <div class="account-btn-desc pause_text">Start your time</div> -->
                                </div>
                                <small class="account-settings-msg" style="display:none">Enabling account please wait </small>


                            </div>

                            </div>
                            <hr>
                            <div class="settings-item chn-sett" style="margin-top:22px;margin-bottom:27px;">
                                <span class="account-btn-desc" style="font-weight:600;">Change your password:</span>
                                <form action="<?=base_url()?>account/changePassword" method="POST" class="chn_pass_form">
                                    <div class="form-group chn_pass" style="margin-top:10px;">
                                        <div class="input-group">
                                            <input type="password" class="form-control" placeholder="Old password" name="old_password" >
                                        </div>
                                    </div>
                                    <div class="form-group chn_pass">
                                        <div class="input-group">
                                            <input type="password" class="form-control" placeholder="New password" name="new_password" >
                                        </div>
                                    </div>
                                    <div class="form-group chn_pass">
                                        <div class="input-group">
                                            <input type="password" class="form-control" placeholder="Confirm new password" name="new_password_confirm" >
                                        </div>
                                    </div>
                                    <div class="form-group chn_pass">
                                        <button type="submit" class="btn btn-block aa bor_btn">Change</button>
                                    </div>
                                </form>
                          </div>
                          <hr style="margin-top:0;">
                          <div class="settings-item" style="margin-top:22px;margin-bottom:27px;">
                              <span class="account-btn-desc deact-btn-span" style="font-weight:600;">Deactivate your account:</span> <button class="btn deac-btn"  data-toggle="modal" data-target="#exampleModal"><span class="deac-wht">Deactivate</span><i class="far fa-stop-circle"></i></button>
                        </div>
                        <hr>
                        </div>

                    </div>


            <?php endif ?>
              </div>
        </div>
    </div>
    <div id="uploadimageModal" class="modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload & Crop Image</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10 text-center">
                            <div id="image_cropper" style="width:350px; margin-top:30px ; height:400px"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-crop" style="float:left;"><button type="button" class="btn btn-default" id="crop_close_btn" style="border-radius:rpx;" data-dismiss="modal">Close</button></div>
                    <button class="btn btn-success crop_image" id="crop_img_btn">Crop</button>
                </div>
            </div>
        </div>
    </div>
 <!-- href="<?php echo base_url(); ?>girls/deactivate/<?php echo $girl->slug; ?>"  -->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="text-align:center;">
        <h5 class="modal-title" style="padding-top:7px;margin-left:auto;margin-right:auto;" id="exampleModalLabel">Deactivate your account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-left:0;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-b" style="padding-top: 15px !important;padding-left:15px;">
        Please enter your password:
        <form action="<?=base_url()?>girls/deactivate/<?php echo $girl->slug; ?>" method="POST" class="chn_pass_form">
            <div class="form-group chn_pass" style="margin-top:10px;margin-left:0;">
                <div class="input-group">
                    <input type="password" class="form-control" placeholder="Password" name="deactivate_password" >
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="border-radius:3px;">Close</button>
        <button type="submit" class="btn btn-primary" style="border-radius:3px;">Deactivate</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="boxModal" tabindex="-1" role="dialog" aria-labelledby="boxModal" aria-hidden="true">
  <div class="modal-dialog box_modal" role="document">
    <div class="modal-content">
      <div class="modal-header" style="text-align: center;">
        <h5 class="modal_h5" id="boxModalLabel">Choose your box</h5>
        <button type="button" class="close" data-dismiss="modal" style="margin-left:0;" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body_box">
        <div class="row">
          <div class="col-sm">
            <div class="onedaydiv">
            <img class="onedaypaket" src="<?php echo base_url(); ?>assets/images/beloo.png" alt="">
          </div>
          <p class="text_mod">Simple box 1 day</p>
          <p class="text_mod">10.- CHF</p>
          </div>
          <div class="col-sm">
            <div class="onedaydiv">
            <img class="onedaypaket" src="<?php echo base_url(); ?>assets/images/crveno7.png" alt="">
          </div>
          <p class="text_mod">Classic box 7 days</p>
          <p class="text_mod">50.- CHF</p>
          </div>
          <div class="col-sm">
            <div class="onedaydiv">
            <img class="onedaypaket" src="<?php echo base_url(); ?>assets/images/tozla30.png" alt="">
          </div>
          <p class="text_mod">Premium box 30 days</p>
          <p class="text_mod">150.- CHF</p>
          </div>
        </div>
        <div class="row" style="margin-top:20px;">
          <div class="help_box">
            Thank you for using our services. </br>If you need help about payment <span class="he_sp">click here</span>.
          </div>
        </div>
      </div>
      <div class="modal-footer" style="margin-top:20px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="border-radius:3px;">Close</button>
      </div>
    </div>
  </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/croppie.js"></script>
<input type="hidden" id="girl-created_on" value="<?=$girl->created_on?>">
<input type="hidden" id="girl-account_paused_on" value="<?=$user_settings!=null?$user_settings->account_paused_at:""?>">
<input type="hidden" id="girl-id" value="<?=$girl->id?>">
<input type="hidden" id="girl-status" value="<?=$girl->status?>">
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script src="<?=base_url()?>assets/js/account_timer.js"></script>
<script src="<?=base_url()?>assets/js/ratio.js"></script>
<script src="<?=base_url()?>assets/js/autosize/dist/autosize.js"></script>
<script src="<?=base_url()?>assets/js/exif.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-load-image/2.12.2/load-image.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-load-image/2.12.2/load-image-scale.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-load-image/2.12.2/load-image-orientation.min.js"></script>
<script>
		autosize(document.querySelector('#desc_autosize'));
	</script>
<script>
    function deleteImg(url, e) {
        e.preventDefault();
        window.location.replace(url);
    }
    function deleteVid(url, e) {
        e.preventDefault();
        window.location.replace(url);
    }
    baguetteBox.run('.tz-gallery', {
      // preload: 2,
    });

    var profile_description = document.getElementsByClassName("profile_description")[0];
    var edit_desc = profile_description.querySelector("#edit_desc");
    var edit_img = profile_description.querySelector(".edit-description-img");
    if(edit_desc){ //Ako je ulogovan
        edit_desc.addEventListener('click', function(){
            var edit_description = document.getElementsByClassName('edit-description')[0];
            var user_description = document.getElementsByClassName('user-description')[0];
            edit_description.style.display = 'block';
            user_description.style.display = 'none';
            edit_img.style.display = 'none';

            edit_description.addEventListener('blur', function (event) {
                var model_id = this.dataset.id;
                AJAX.post({
                    url: base_url+"madames/handleForm/update/girl_model/"+model_id,
                    data: {
                        model_data: {
                            description: this.value
                        }
                    },
                    success: function(response){
                        edit_description.style.display = 'none';
                        user_description.style.display = 'block';
                        edit_img.style.display = 'block';
                        user_description.textContent = edit_description.value;
                        location.reload();
                    }
                })

            })

            // edit_description.addEventListener('keyup', function (event) {
            //   if (event.keyCode === 13) {
            //     var model_id = this.dataset.id;
            //     AJAX.post({
            //         url: base_url+"madames/handleForm/update/girl_model/"+model_id,
            //         data: {
            //             model_data: {
            //                 description: this.value
            //             }
            //         },
            //         success: function(response){
            //             edit_description.style.display = 'none';
            //             user_description.style.display = 'block';
            //             edit_img.style.display = 'block';
            //             user_description.textContent = edit_description.value;
            //         }
            //     })
            //   }
            // })
        });
    }


    var chat_status = document.querySelector("#chat_status");
    /* chat_status.addEventListener('change', function(e){
        console.log(this.value);
    }); */
    function updateChatStatus(model_id,chat_status){
        if(chat_status.checked === true){
          console.log('1');
          AJAX.post({
              url: base_url+"madames/handleForm/update/girl_model/"+model_id,
              data: {
                  model_data: {
                      chat_status: 1
                  }
              }
          })
        }else{
          console.log('2');
          AJAX.post({
            url: base_url+"madames/handleForm/update/girl_model/"+model_id,
            data: {
                model_data: {
                    chat_status: 0
                }
            }
        })
      }
    }


    var alert = document.querySelector('.alert.alert-success');
    if(alert){
        setTimeout(() => {
            var msg = $(alert);
            msg.slideToggle("slow", function(){
                $('.madames-msg').hide('slow');
            });

        }, 3000)

    }

    var textarea = document.querySelector('.edit-description');


</script>

<script>
// var imageSize = {
//         width: 500,
//         height: 900,
//         type: 'square'
// };
// var input_file = document.getElementById("profile_photo");
// var button_crop = document.getElementById("crop_img_btn");
// var button_close = document.getElementById("crop_close_btn");
// var image_cropper = document.getElementById("image_cropper");
// var crop = new Croppie(image_cropper, {
//     enableExif: true,
//     viewport: {
//       width:450,
//       height:750,
//       type:'square' //circle
//     },
//     boundary:{
//       width:550,
//       height:750
//     },
//     enableOrientation: true,
// });
//
// input_file.onchange = function(){
//     var reader = new FileReader();
//     reader.onload = function (event) {
//         crop.bind({
//         url: event.target.result
//     }).then(function(){
//         console.log('jQuery bind complete');
//     });
//     }
//     reader.readAsDataURL(this.files[0]);
//     $('#uploadimageModal').show();
//     $('#save_profile').hide();
// }
//
// button_crop.onclick = function(event){
//     crop.result({
//         type: 'canvas',
//         size: 'original',
//         format: 'png',
//         quality: 1
//     }).then(function(response){
//         $("#cropped_image").val(response);
//         $('#uploadimageModal').hide();
//         $('#save_profile').show();
//     })
// }
// button_close.onclick = function(e){
//     $("#uploadimageModal").hide();
// }
var input_file = document.getElementById("profile_photo");
var button_crop = document.getElementById("crop_img_btn");
var button_close = document.getElementById("crop_close_btn");
var image_cropper = document.getElementById("image_cropper");
var crop = new Croppie(image_cropper, {
    enableExif: true,
    viewport: {
      width:350,
      height:450,
      type:'square' //circle
    },
    boundary:{
      width:600,
      height:450
    }
});

input_file.onchange = function(){
    var reader = new FileReader();
    reader.onload = function (event) {
        crop.bind({
        url: event.target.result
    }).then(function(){
        console.log('jQuery bind complete');
    });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').show();
}

button_crop.onclick = function(event){
    crop.result({
        //type: 'canvas', //retruns b64
        type: 'blob',
        size: 'viewport',
        format: 'png',
        quality: 1
    }).then(function(response){
        //$("#cropped_image").val(response);
        // console.log(response);
        var fd = new FormData();
        fd.append('fname', 'test.txt');
        fd.append('cropped_image', response);
        $.ajax({
            type: 'POST',
            url: '<?=base_url()?>girls/storeTempImg',
            data: fd,
            processData: false,
            contentType: false
        }).done(function(data) {
            console.log(data);
        });
        $('#uploadimageModal').hide();
        $('#save_profile').show();
    })
}
button_close.onclick = function(e){
    $("#uploadimageModal").hide();
}

</script>

<script>

// $("#images").on('change', function () {
//
//     $(".hide_btn_img").hide();
//     var save = document.querySelector('.show_upload_gallery');
//     save.style.display = "inline-block";
//     var imgPath = $(this)[0].value;
//     var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
//
//     if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
//         if (typeof (FileReader) != "undefined") {
//
//             var image_holder = $("#image-holder");
//             image_holder.empty();
//
//             var reader = new FileReader();
//             reader.onload = function (e) {
//                 $("<img />", {
//                     "src": e.target.result,
//                     "class": "thumb-image",
//                     "orientation": 1
//                 }).appendTo(image_holder);
//             }
//             image_holder.show();
//             reader.readAsDataURL($(this)[0].files[0]);
//         } else {
//             alert("This browser does not support FileReader.");
//         }
//     } else {
//         alert("Pls select only images");
//     }
//
// });
document.getElementById('images').onchange = function (e) {
    $(".hide_btn_img").hide();
      var image = e.target.files[0];
      window.loadImage(image, function (img) {
          if (img.type === "error") {
              console.log("couldn't load image:", img);
          } else {
              window.EXIF.getData(image, function () {
                  console.log("load image done!");
                  var orientation = window.EXIF.getTag(this, "Orientation");
                  var canvas = window.loadImage.scale(img,
                      {orientation: orientation || 0, canvas: true, maxWidth: 200});
                  document.getElementById("image-holder").appendChild(canvas);
                  $("#images_save").show();
                  // or using jquery $("#container").append(canvas);
              });
          }
      });
  };

</script>
<script>
$("#fileslika").change(function() {
    var save = document.querySelector('.show_upload_gallery_video');
    console.log('uslo je');
    save.style.display = "inline-block";
    // var text = document.querySelector('.upload_span');
    // text.style.display = "none";
    // var show = document.querySelector('.upload_span_show');
    // show.style.display = "block";
});
</script>
<script>
  $( document ).ready(function() {
   var  p = document.getElementsByClassName('break');
    if(p[12]){
      $(p[12]).css("display", "inline-block");
    }
});

$( document ).ready(function() {
  setAspectRatio()
});
</script>
<!-- /.container -->
