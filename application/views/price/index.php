<div class="tab_container_price">
    <input id="tab1" type="radio" name="tabs" checked>
    <label class="label_tab" style="border-top-left-radius:6px;" for="tab1"><img class="price_girl_pic" src="<?php echo base_url(); ?>assets/images/girl.png" alt=""><span>Girls</span></label>

    <input id="tab2" type="radio" name="tabs">
    <label class="label_tab" style="border-top-right-radius:6px;" for="tab2"><img class="price_girl_pic" src="<?php echo base_url(); ?>assets/images/glass.png" alt=""><span>Clubs</span></label>


    <section id="content1" class="tab-content">
        <h3 style="margin-bottom:15px;margin-top:15px;">Price for girls</h3>
        <p style="margin-bottom:25px;">For every girl first month is for free to taste our site and our quality.</p>
        <p style="margin-bottom:30px;">After free month:</p>
        <table class="table table_price">
            <thead>
                <tr>
                    <th scope="col" class="th_price" style="border-right:3px solid #cccccc;"><span style="border-bottom:1px solid #b3b3b3;padding:8px;padding-bottom:11px;padding-left:11px;margin-left:auto;margin-right:auto;text-align: center;"> 1 Day &nbsp</span>&nbsp &nbsp &nbsp &nbsp</th>
                    <th scope="col" class="th_price" style="border-right:3px solid #cccccc;"><span style="border-bottom:1px solid #b3b3b3;padding:8px;padding-bottom:13px;margin-left:auto;margin-right:auto;text-align: center;"> 1 Week</span> &nbsp &nbsp &nbsp</th>
                    <th scope="col" class="th_price"><span style="border-bottom:1px solid #b3b3b3;padding:8px;padding-bottom:13px;margin-left:auto;margin-right:auto;text-align: center;">1 Month</span></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="th_price" style="border-right:3px solid #cccccc;">10.- CHF &nbsp &nbsp </td>
                    <td class="th_price" style="border-right:3px solid #cccccc;">&nbsp 50.- CHF &nbsp &nbsp &nbsp</td>
                    <td class="th_price">&nbsp 150.- CHF</td>
                </tr>

            </tbody>
        </table>
    </section>

    <section id="content2" class="tab-content">
        <h3 style="margin-bottom:15px;margin-top:15px;">Price for clubs</h3>
        <p style="margin-bottom:25px;">For every club promotion is for free, but can only set one photo and few informations about them.</p>
    </section>

</div>
