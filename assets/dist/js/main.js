webpackJsonp([1],{

/***/ "/2Uv":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("FZ+f")(false);
// imports


// module
exports.push([module.i, "\n.active-filter[data-v-0dfe1da2] {\n    background-color: #990000;\n    color: white !important;\n}\n.active-filter a[data-v-0dfe1da2]{\n    color: white !important;\n}\n.online-girls[data-v-0dfe1da2] {\n    max-height: 500px;\n    overflow: auto;\n}\n\n", ""]);

// exports


/***/ }),

/***/ "NHnr":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm.js
var vue_esm = __webpack_require__("7+uW");

// EXTERNAL MODULE: ./node_modules/babel-runtime/core-js/promise.js
var promise = __webpack_require__("//Fk");
var promise_default = /*#__PURE__*/__webpack_require__.n(promise);

// EXTERNAL MODULE: ./node_modules/babel-runtime/core-js/json/stringify.js
var stringify = __webpack_require__("mvHQ");
var stringify_default = /*#__PURE__*/__webpack_require__.n(stringify);

// EXTERNAL MODULE: ./node_modules/babel-runtime/helpers/typeof.js
var helpers_typeof = __webpack_require__("pFYg");
var typeof_default = /*#__PURE__*/__webpack_require__.n(helpers_typeof);

// CONCATENATED MODULE: ./src/components/ajax.js



// Tiny Ajax library
//export var AJAX = {
var ajax_AJAX = {
    createXHR: function createXHR() {
        var xhr;
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert(e.message);
                xhr = null;
            }
        } else {
            xhr = new XMLHttpRequest();
        }

        return xhr;
    },

    convertToPost: function convertToPost(data) {
        var postData = '';
        if (data.constructor.name == 'FormData') {
            return data;
        }
        for (var key in data) {
            if (typeof_default()(data[key]) === 'object') {
                postData += key + "=" + stringify_default()(data[key]) + "&";
            } else {
                postData += key + "=" + data[key] + "&";
            }
        }
        return postData.slice(0, -1);
    },

    //xhr: this.createXHR,
    XHR: function XHR() {
        var xhr = ajax_AJAX.createXHR();
        return xhr;
    },
    //Shows the percent progress while uploading files
    progress: function progress(param, callback) {
        //return new Promise(function(resolve, reject){
        var xhr = ajax_AJAX.createXHR();
        xhr.open("POST", param.url, true);
        // xhr default header accept: */* for multi upload
        //xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        if (xhr.readyState == 4) {
            //let msg = JSON.parse(xhr.responseText);
        }
        xhr.upload.addEventListener("progress", function (evt) {
            if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                //Do something with upload progress
                var _p = Math.round(percentComplete * 100);
                callback(_p);
            }
        }, false);

        xhr.onerror = function () {
            //reject(xhr.status);
            console.log("Ajax error = " + xhr.status);
        };
        if (param.data) {
            xhr.send(param.data);
        } else {
            xhr.send();
        }
        //});
    },
    // Ajax with post method
    post: function post(param) {
        var form_data = null;
        if (param.data) {
            form_data = ajax_AJAX.convertToPost(param.data);
        }
        if (param.csrf) {
            form_data += "&" + param.csrf + "=" + document.getElementsByName(param.csrf)[0].value;
        }

        return new promise_default.a(function (resolve, reject) {
            var resposne_data = null;
            var xhr = ajax_AJAX.createXHR();
            ajax_AJAX.XHR = xhr;
            xhr.open("POST", param.url, true);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.onload = function () {
                if (xhr.status == 200) {
                    if (param.responseType == 'text') {
                        resolve(xhr.responseText);
                    } else if (xhr.responseText != '') {
                        try {
                            var _resposne_data = JSON.parse(xhr.responseText);
                            resolve(_resposne_data);
                        } catch (e) {
                            console.log('Greskom tokom parsiranja response text-a u JSON ');
                            console.log('URL: ' + param.url);
                            console.log(e);
                            console.log('Response: ');
                            console.log(xhr.responseText);
                            resolve();
                        }
                    }
                    //Callback
                    if (param.success) {
                        param.success(resposne_data);
                    }
                }
            };
            xhr.onerror = function () {
                reject(xhr.status);
                console.log("Ajax error = " + xhr.status);
            };
            xhr.send(form_data);
        });
    }

    /*xhr.onreadystatechange = function()
    {
      if (xhr.readyState === 4)
      {
        alert(xhr.responseText);
      }
    }*/

};

/* harmony default export */ var ajax = (ajax_AJAX);
// EXTERNAL MODULE: ./node_modules/babel-runtime/helpers/defineProperty.js
var defineProperty = __webpack_require__("bOdI");
var defineProperty_default = /*#__PURE__*/__webpack_require__.n(defineProperty);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/filter_tags.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


//import {APP} from '../config'



/* harmony default export */ var filter_tags = ({

  name: 'filterTags',

  props: {
    tags: { type: Object, default: null }
  },
  //['table', 'inputLabel', 'visible', 'relation', 'paginate'],
  data: function data() {
    return {
      data: null,
      filters: []
    };
  },

  methods: {
    close: function close() {
      this.show = false;
    },
    removeFilter: function removeFilter(filter) {
      //this.removeTag(filter);
      this.$emit('removeFilter', filter);
    },
    triggerEvent: function triggerEvent(e, id) {
      this.$emit('listingEvent', { e: e, id: id });
    },

    //Nije portebna funkcija zato sto kroz event parent ponovo salje prop koji ce da pregazi postojeci
    removeTag: function removeTag(filter) {
      for (var key in this.filters) {
        if (this.filters[key].value !== 'undefined' && this.filters[key].value == filter.value) {
          vue_esm["a" /* default */].delete(this.filters, key);
        }
      }
    },

    showData: function showData() {}

  },
  computed: {},
  watch: {},
  mounted: function mounted() {
    var self = this;
    /* EventBus.$on('selected_filter', function (filter) {
      self.filters.push(filter);
    });
    EventBus.$on('deselect_filter', function (filter) {
      self.removeTag(filter);
    });
    EventBus.$on('reset_filter', function (filter) {
      console.log(filter);
      for(var key in self.filters)
      {
        if(self.filters[key].value !== 'undefined' && self.filters[key].value == filter.value)
        {
          Vue.delete(this.filters, key);
        }              
      }
    }); */
  },
  created: function created() {}
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-4e44d547","hasScoped":true,"transformToRequire":{"video":["src","poster"],"source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/filter_tags.vue
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{staticClass:"row filters_div_bottom"},[(_vm.tags.length>0)?_c('ul',{staticClass:"filter_lists_bottom"},[_vm._m(0),_vm._v(" "),_vm._l((_vm.tags),function(filter){return _c('li',[_c('button',{staticClass:"close filters_bottom_btn",attrs:{"type":"button","aria-label":"Close"},on:{"click":function($event){_vm.removeFilter(filter)}}},[_vm._v(_vm._s(filter.display_value)+"\n            "),_c('span',{attrs:{"aria-hidden":"true"}},[_vm._v("×")])])])})],2):_vm._e()])])}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('li',{staticClass:"filters_first_bottom"},[_c('div',{staticClass:"dropdown"},[_c('p',[_vm._v("You choose:")])])])}]
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ var components_filter_tags = (esExports);
// CONCATENATED MODULE: ./src/components/filter_tags.vue
function injectStyle (ssrContext) {
  __webpack_require__("W9kt")
}
var normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4e44d547"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  filter_tags,
  components_filter_tags,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ var src_components_filter_tags = (Component.exports);

// CONCATENATED MODULE: ./src/config.js
var APP = {
	//base_url : 'http://www.madames.ch/',
	base_url: 'http://localhost/ongit/project/madames/',
	lng: 'EN'
};
Object.defineProperty(APP, 'ajax_url', {
	get: function get() {
		return this.base_url + "ajax/";
	}
});


//export const BASE_URL = 'http://www.madames.ch/';
var BASE_URL = 'http://localhost/ongit/project/madames/';
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/listing.vue


var _methods;

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

//import {APP} from '../config'




/* harmony default export */ var listing = ({
    name: "listing",
    components: {
        filterTags: src_components_filter_tags
    },
    props: {},
    //['table', 'inputLabel', 'visible', 'relation', 'paginate'],
    data: function data() {
        return {
            _loader: null,
            base_url: window._m.base_url,
            response: "<strong>No results</strong>",
            search_input: null,
            responseStatus: false,
            pagination_start: 0,
            girls: null,
            online_girls: [],
            filters: {
                regions: [],
                ts: {
                    name: "TS"
                },
                age: {
                    name: "Age",
                    value: ["18-25", "25-35", "35-45", "45-55", "55+"]
                },
                services: {
                    name: "Services",
                    value: [] //Popunjava se u getData()
                },
                hairs: {
                    name: "Hair color",
                    value: []
                },
                types: {
                    name: "Type",
                    value: []
                },
                username: {
                    name: 'Username',
                    value: ''
                }
            },
            tags: [],
            selected_filters: {
                age: "",
                services: [],
                hairs: [],
                types: [],
                ts: "",
                order: "",
                region: "",
                username: ""
            },
            search_timer: null
        };
    },

    methods: (_methods = {
        close: function close() {
            console.log("test");
        },
        triggerEvent: function triggerEvent(e, id) {
            this.$emit("listingEvent", {
                e: e,
                id: id
            });
        },
        search: function search(e) {
            var _this = this;
            if (this.search_timer) {
                clearTimeout(this.search_timer);
                this.search_timer = null;
            }
            this.search_timer = setTimeout(function () {
                _this.selected_filters.username = _this.search_input;
                _this.renderView();
            }, 800);
        },
        remove: function remove() {},
        filterRegion: function filterRegion(value, event) {
            this._loader.style.display = 'block';
            var el = event.target;
            this.pagination_start = 0;
            if (el.classList.contains("active-region")) {
                el.classList.remove("active-region");
                this.selected_filters['region'] = "";
            } else {
                var parent = el.parentElement;
                var active_elements = parent.querySelectorAll(".active-region");
                if (active_elements.length > 0) {
                    active_elements[0].classList.remove("active-region");
                }
                el.classList.add("active-region");
                this.selected_filters['region'] = value;
            }
            this.renderView();
        },
        filterSingleSelection: function filterSingleSelection(filter_name, value, name, event) {
            this._loader.style.display = 'block';
            var el = event.target.parentElement;
            var check = el.querySelector('.fa-check-circle');
            this.pagination_start = 0;
            var filter = {
                name: filter_name,
                display_value: name,
                value: value,
                el: el
            };

            if (el.classList.contains("active-filter")) {
                el.classList.remove("active-filter");
                this.selected_filters[filter_name] = "";
                //EventBus.$emit("reset_filter", filter);
                //Ovde vracamo samo onaj filter u niz koji ima razlicit name od prosledjenog
                //(ukoliko je activ-filter nece vratiti izabrani filter tj. resetujemo tags)
                // zato sto je singleSelection
                this.tags = this.tags.filter(function (item) {
                    return item.name != filter_name;
                });
                check.classList.add('hidden');
            } else {
                var ul = el.parentElement;
                var active_elements = ul.querySelectorAll(".active-filter");
                if (active_elements.length > 0) {
                    var active_elements_check = active_elements[0].querySelector('.fa-check-circle');
                    active_elements[0].classList.remove("active-filter");
                    active_elements_check.classList.add('hidden');
                    this.tags = this.tags.filter(function (item) {
                        return item.name != filter_name;
                    });
                }
                el.classList.add("active-filter");
                if (name != "") this.tags.push(filter);
                this.selected_filters[filter_name] = value;
                check.classList.remove('hidden');
            }

            this.renderView();
        },
        filterMultipleSelection: function filterMultipleSelection(filter_name, value, name, event) {
            this._loader.style.display = 'block';
            var el = event.target.parentElement;
            var check = el.querySelector('.fa-check-circle');
            this.pagination_start = 0;
            var filter = {
                name: filter_name,
                display_value: name,
                value: value,
                el: el
            };

            if (el.classList.contains("active-filter")) {
                el.classList.remove("active-filter");
                this.selected_filters[filter_name] = this.selected_filters[filter_name].filter(function (item) {
                    return item !== value;
                });
                this.tags = this.tags.filter(function (item) {
                    return item.value !== value;
                });
                check.classList.add('hidden');
                this.renderView();
            } else {
                el.classList.add("active-filter");
                check.classList.remove('hidden');
                //if(typeof value == 'string') value = value.toLowerCase();
                this.selected_filters[filter_name].push(value);
                this.tags.push(filter);
                this.renderView();
            }
        },

        renderView: function renderView() {
            var self = this;
            if (self.selected_filters.services.length == 0) self.services = "";
            if (self.selected_filters.hairs.length == 0) self.hairs = "";

            AJAX.post({
                url: "ajax/getgirls/" + self.pagination_start,
                responseType: "text",
                data: {
                    age: self.selected_filters.age,
                    services: self.selected_filters.services,
                    hairs: self.selected_filters.hairs,
                    type: self.selected_filters.types,
                    ts: self.selected_filters.ts,
                    order: self.selected_filters.order,
                    region: self.selected_filters.region,
                    username: self.selected_filters.username
                }
            }).then(function (obj) {
                //console.log(obj);
                self.responseStatus = true;
                self.response = obj;
                self._loader.style.display = 'none';
            });
        }
    }, defineProperty_default()(_methods, "triggerEvent", function triggerEvent(event_name, data) {}), defineProperty_default()(_methods, "removeSelectedFilter", function removeSelectedFilter(filter) {
        this._loader.style.display = 'block';
        if (typeof this.selected_filters[filter.name] == "string") {
            this.selected_filters[filter.name] = '';
            this.tags = this.tags.filter(function (item) {
                return item.name != filter.name;
            });
        } else {
            this.selected_filters[filter.name] = this.selected_filters[filter.name].filter(function (item) {
                return item !== filter.value;
            });
            // Zbog mogucnosti podudaranja id-jeva morama proveru value-a da vezemo za njemu pripadajuci filter name
            this.tags = this.tags.filter(function (item) {
                if (item.name == filter.name && item.value == filter.value) {
                    return false;
                } else return true;
            });
        }
        filter.el.classList.remove("active-filter");
        filter.el.querySelector('.fa-check-circle').classList.add('hidden');
        this.renderView();
    }), defineProperty_default()(_methods, "chatAdapter", function chatAdapter(id, name, event) {
        event.preventDefault();
        chat(id, name, event);
    }), defineProperty_default()(_methods, "getData", function getData() {
        var self = this;
        AJAX.post({
            url: "ajax/getModels",
            data: {
                model_names: ["girl_model", "region", "service", "type", "hair"],
                model_pagination: { girl_model: { take: 12, start: 1 }, model_b: { take: 12, start: 0 } }
            }
        }).then(function (response) {
            //console.log(response);
            self.girls = response.girl_modell;
            self.online_girls = response.online_girls;
            self.filters.regions = response.region;
            self.filters.services.value = response.service;
            self.filters.hairs.value = response.hair;
            self.filters.types.value = response.type;
            //console.log(self.online_girls);
        });
    }), defineProperty_default()(_methods, "pag", function pag(e) {
        e.preventDefault();
        this.pagination_start = (e.target.dataset.ciPaginationPage - 1) * 12; //Mora -1 zato sto CI tako funkcionise
        this.renderView();
        window.scrollTo(0, 0); //$("html, body").animate({ scrollTop: 0 }, "slow");
    }), defineProperty_default()(_methods, "init", function init() {
        function customListCSS(list_status, container) {
            var list = $(container).find('.slide-down');
            if (list_status == 'open') {
                list.css('border-bottom-left-radius', '0');
                list.css('border-bottom-right-radius', '0');
            } else {
                list.css('border-bottom-left-radius', '8px');
                list.css('border-bottom-right-radius', '8px');
            }
        }
        slideDownList("regions", customListCSS);

        slideDownList("online-girls", customListCSS);

        slideDownList("filters", customListCSS);
    }), _methods),
    computed: {},
    watch: {},
    mounted: function mounted() {
        var _this2 = this;

        var self = this;
        this.renderView();
        EventBus.$on('pag', function (page) {
            return _this2.pag(page);
        }); //Event za paginaciju
        this.init();
    },
    created: function created() {
        this.getData();
        this._loader = document.getElementById("m-loader");
    }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-0dfe1da2","hasScoped":true,"transformToRequire":{"video":["src","poster"],"source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/listing.vue
var listing_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-lg-3 aside-search"},[_c('div',{staticClass:"list-group",attrs:{"id":"regions"}},[_vm._m(0),_vm._v(" "),_c('div',{staticClass:"slide-down-list"},_vm._l((_vm.filters.regions),function(region){return _c('a',{staticClass:"list-group-item ",attrs:{"href":_vm.base_url+'girls/regions/'+region.id},on:{"click":function($event){if($event.target !== $event.currentTarget){ return null; }$event.preventDefault();_vm.filterRegion(region.id, $event)}}},[_c('span',{staticClass:"icon-m-location"}),_vm._v("\r\n                    "+_vm._s(region.name)+"\r\n                ")])}))]),_vm._v(" "),_c('div',{staticClass:"aside-search"},[_c('div',{staticClass:"list-group online-girls",attrs:{"id":"online-girls"}},[_vm._m(1),_vm._v(" "),_c('div',{staticClass:"slide-down-list"},_vm._l((_vm.online_girls),function(online_girl){return _c('a',{staticClass:"list-group-item live_girl_a",attrs:{"href":"#"},on:{"click":function($event){_vm.chatAdapter(online_girl.id, online_girl.username, $event);}}},[_c('img',{staticClass:"live_girl_pic",attrs:{"src":'assets/images/'+ (online_girl.profile_photo ? 'girls/'+online_girl.profile_photo : 'default.jpg'),"alt":""}}),_vm._v(" "),_c('p',{staticStyle:{"float":"left"}},[_vm._v(_vm._s(online_girl.username))]),_vm._v(" "),_c('img',{staticStyle:{"float":"right","width":"18px","height":"23px","margin-top":"4px"},attrs:{"src":"assets/images/chat5.png"}}),_vm._v(" "),_c('div',{staticStyle:{"clear":"both"}})])}))])])]),_vm._v(" "),_c('div',{staticClass:"col-lg-9"},[_c('div',{staticClass:"filter_lists ",attrs:{"id":"filters"}},[_vm._m(2),_vm._v(" "),_c('div',{staticClass:"slide-down-list"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-2 col-sm-4 col-6"},[_c('div',{staticClass:"dropdown"},[_vm._m(3),_vm._v(" "),_c('ul',{staticClass:"dropdown-menu filt"},[_c('li',[_c('a',{attrs:{"href":"#"},on:{"click":function($event){if($event.target !== $event.currentTarget){ return null; }$event.preventDefault();_vm.filterSingleSelection('ts', 1, '', $event)}}},[_vm._v("Yes "),_c('i',{staticClass:"hidden far fa-check-circle"})])]),_vm._v(" "),_c('li',[_c('a',{attrs:{"href":"#"},on:{"click":function($event){if($event.target !== $event.currentTarget){ return null; }$event.preventDefault();_vm.filterSingleSelection('ts', 0, '', $event)}}},[_vm._v("No "),_c('i',{staticClass:"hidden far fa-check-circle"})])])])])]),_vm._v(" "),_c('div',{staticClass:"col-md-2 col-sm-4 col-6"},[_c('div',{staticClass:"dropdown"},[_vm._m(4),_vm._v(" "),_c('ul',{staticClass:"dropdown-menu filt "},_vm._l((_vm.filters.services.value),function(service){return _c('li',[_c('a',{attrs:{"href":"#"},on:{"click":function($event){_vm.filterMultipleSelection('services', service.id, service.name, $event)}}},[_vm._v(_vm._s(service.name)+" "),_c('i',{staticClass:"hidden far fa-check-circle"})])])}))])]),_vm._v(" "),_c('div',{staticClass:"col-md-2 col-sm-4 col-6"},[_c('div',{staticClass:"dropdown"},[_vm._m(5),_vm._v(" "),_c('ul',{staticClass:"dropdown-menu filt"},_vm._l((_vm.filters.age.value),function(age){return _c('li',[_c('a',{attrs:{"href":"#"},on:{"click":function($event){if($event.target !== $event.currentTarget){ return null; }$event.preventDefault();_vm.filterSingleSelection('age', age, age, $event)}}},[_vm._v(_vm._s(age)+" "),_c('i',{staticClass:"hidden far fa-check-circle"})])])}))])]),_vm._v(" "),_c('div',{staticClass:"col-md-2 col-sm-4 col-6"},[_c('div',{staticClass:"dropdown"},[_vm._m(6),_vm._v(" "),_c('ul',{staticClass:"dropdown-menu filt"},_vm._l((_vm.filters.hairs.value),function(hair){return _c('li',[_c('a',{attrs:{"href":"#"},on:{"click":function($event){if($event.target !== $event.currentTarget){ return null; }$event.preventDefault();_vm.filterMultipleSelection('hairs', hair.id, hair.name, $event)}}},[_vm._v(_vm._s(hair.name)+" "),_c('i',{staticClass:"hidden far fa-check-circle"})])])}))])]),_vm._v(" "),_c('div',{staticClass:"col-md-2 col-sm-4 col-6"},[_c('div',{staticClass:"dropdown"},[_vm._m(7),_vm._v(" "),_c('ul',{staticClass:"dropdown-menu filt"},_vm._l((_vm.filters.types.value),function(type){return _c('li',[_c('a',{attrs:{"href":"#"},on:{"click":function($event){if($event.target !== $event.currentTarget){ return null; }$event.preventDefault();_vm.filterMultipleSelection('types', type.id, type.name, $event)}}},[_vm._v(_vm._s(type.name)+" "),_c('i',{staticClass:"hidden far fa-check-circle"})])])}))])]),_vm._v(" "),_c('div',{staticClass:"col-md-2 col-sm-4 col-6 search-col"},[_c('div',{staticClass:"search-field"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.search_input),expression:"search_input"}],staticClass:"form-control input-sm search-input",attrs:{"type":"text","name":"keywords","placeholder":"Search"},domProps:{"value":(_vm.search_input)},on:{"keyup":_vm.search,"input":function($event){if($event.target.composing){ return; }_vm.search_input=$event.target.value}}})])])])])]),_vm._v(" "),_c('filter-tags',{attrs:{"tags":_vm.tags},on:{"removeFilter":function($event){_vm.removeSelectedFilter($event)}}}),_vm._v(" "),(_vm.responseStatus)?_c('div',{domProps:{"innerHTML":_vm._s(_vm.response)}}):_vm._e()],1)])])}
var listing_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('h1',{staticClass:"my-4 slide-down"},[_vm._v("\r\n                City/Regions\r\n                "),_c('div',{staticClass:"angle-down-div"},[_c('i',{staticClass:"fas fa-angle-down"})])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('h1',{staticClass:"my-4 slide-down"},[_vm._v("\r\n                    Live girls\r\n                    "),_c('div',{staticClass:"angle-down-div"},[_c('i',{staticClass:"fas fa-angle-down"})])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12"},[_c('div',{staticClass:"dropdown text-center slide-down"},[_c('p',[_vm._v("Filters")]),_vm._v(" "),_c('div',{staticClass:"angle-down-div"},[_c('i',{staticClass:"fas fa-angle-down"})])])])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{staticClass:"btn filters_btn",attrs:{"type":"button","data-toggle":"dropdown"}},[_vm._v("TS\r\n                            "),_c('i',{staticClass:"fas fa-chevron-circle-down"})])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{staticClass:"btn filters_btn",attrs:{"type":"button","data-toggle":"dropdown"}},[_vm._v("Services\r\n                            "),_c('i',{staticClass:"fas fa-chevron-circle-down"})])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{staticClass:"btn filters_btn",attrs:{"type":"button","data-toggle":"dropdown"}},[_vm._v("Age\r\n                            "),_c('i',{staticClass:"fas fa-chevron-circle-down"})])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{staticClass:"btn filters_btn",attrs:{"type":"button","data-toggle":"dropdown"}},[_vm._v("Hair\r\n                            "),_c('i',{staticClass:"fas fa-chevron-circle-down"})])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{staticClass:"btn filters_btn",attrs:{"type":"button","data-toggle":"dropdown"}},[_vm._v("Type\r\n                            "),_c('i',{staticClass:"fas fa-chevron-circle-down"})])}]
var listing_esExports = { render: listing_render, staticRenderFns: listing_staticRenderFns }
/* harmony default export */ var components_listing = (listing_esExports);
// CONCATENATED MODULE: ./src/components/listing.vue
function listing_injectStyle (ssrContext) {
  __webpack_require__("Ugm4")
}
var listing_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var listing___vue_template_functional__ = false
/* styles */
var listing___vue_styles__ = listing_injectStyle
/* scopeId */
var listing___vue_scopeId__ = "data-v-0dfe1da2"
/* moduleIdentifier (server only) */
var listing___vue_module_identifier__ = null
var listing_Component = listing_normalizeComponent(
  listing,
  components_listing,
  listing___vue_template_functional__,
  listing___vue_styles__,
  listing___vue_scopeId__,
  listing___vue_module_identifier__
)

/* harmony default export */ var src_components_listing = (listing_Component.exports);

// CONCATENATED MODULE: ./src/main.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventBus", function() { return EventBus; });
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.


// import App from './App'
// import router from './router'

var EventBus = new vue_esm["a" /* default */](); // Za komunikaciju izmedju komponenti (parent-child)
window.EventBus = EventBus;
vue_esm["a" /* default */].config.productionTip = false;
window.AJAX = ajax;
/* eslint-disable no-new */
/* new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
}) */
window._listing = src_components_listing;
new vue_esm["a" /* default */]({
    el: '#listing',
    components: {
        'listing': src_components_listing
    },
    //template: '<h3>Listing</h3>',
    data: {
        message: 'Hello Vue!'
    },
    methods: {}
    //render: h => h(listing), //ovo ce pregaziti ceo html u <div id='listing'></div>
    //render: Vue.compile('<div><span>{{ msg }}</span></div>').render
});
//var result = Vue.compile(html_string).render;

/***/ }),

/***/ "NRYH":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("FZ+f")(false);
// imports


// module
exports.push([module.i, "\n.filters_div_bottom[data-v-4e44d547]{\r\n  margin-left: 2px;\n}\n.filters_bottom_btn[data-v-4e44d547]{\r\n  background-color: #990000 !important;\r\n  color: white !important;\r\n  border-radius: 12px;\r\n  font-size: 12px !important;\r\n  padding: .200rem .500rem !important;\r\n  min-height: 30px !important;\r\n  opacity: 1 !important;\r\n  margin-right: 5px;\n}\n.filter_lists_bottom[data-v-4e44d547]{\r\n  list-style-type: none;\r\n  padding-left: 0;\r\n  border-radius: 5px;\r\n  padding-right: 20px;\r\n  margin-bottom: 0 !important;\r\n  margin-top: 5px;\n}\n.filters_first_bottom[data-v-4e44d547]{\r\n  color: black;\r\n  margin-right: 15px;\n}\n.filters_first_bottom p[data-v-4e44d547]{\r\n  margin-bottom: 0;\r\n  margin-top: 5px;\n}\n.filters_bottom_btn[data-v-4e44d547]::after{\r\nborder:0 !important;\n}\n.filters_bottom_btn span[data-v-4e44d547]{\r\nmargin-left: 2px;\n}\n.close[data-v-4e44d547]:hover{\r\n  background-color: #b30000 !important;\n}\r\n\r\n", ""]);

// exports


/***/ }),

/***/ "Ugm4":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("/2Uv");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("rjj0")("0666b4ea", content, true, {});

/***/ }),

/***/ "W9kt":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("NRYH");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("rjj0")("2c026ef8", content, true, {});

/***/ })

},["NHnr"]);