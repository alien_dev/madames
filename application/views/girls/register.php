<?php use Lib\Validate; ?>

<div class="tab_container_register">
    <input id="tab1" type="radio" name="tabs" checked>
    <label for="tab1"><i class="fa fa-code"></i><span>Girls</span></label>

    <input id="tab2" type="radio" name="tabs">
    <label for="tab2"><i class="fa fa-pencil-square-o"></i><span>Clubs</span></label>

    <section id="content1" class="tab-content register_form_tab">
        <?php #echo validation_errors(); ?>
        <div class="login-form register_form">
            <?php
            echo form_open_multipart('girls/store_user');
            ?>
            <h2 class="text-center">Sign up</h2>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control" placeholder="Username" name="username" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" class="form-control" placeholder="Password" name="password" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" class="form-control" placeholder="Confirm password" name="password_confirm" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="text" class="form-control" placeholder="Email" name="email" >
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="text" class="form-control" placeholder="Age" name="age" >
                </div>
            </div>
            <div class="form-group">
                <label for="ts">TS:</label>
                <select class="form-control" id="sel2"  name="ts">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </div>
            <div class="form-group">
                <label for="region_id">Regions:</label>
                <select class="form-control" id="sel1"  name="region_id">
                    <?php foreach ($regions as $region) : ?>
                        <option value="<?php echo $region->id; ?>"><?php echo $region->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Select Profile Image</div>
                <div class="panel-body" align="center">
                    <input type="file" name="userfile" id="profile_picture" accept="image/*" size="20" />
                    <input type="hidden" name="cropped_b64" id="cropped_image">
                    <br />
                    <div id="uploaded_image"></div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Sign up</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </section>

    <section id="content2" class="tab-content">
        <?php echo validation_errors(); ?>
        <div class="login-form register_form">
            <?php
            echo form_open_multipart('girls/register');
            ?>
            <h2 class="text-center">Sign up</h2>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control" placeholder="Club name" name="clubname" required="required">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" class="form-control" placeholder="Street" name="street" required="required">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="text" class="form-control" placeholder="Email" name="email" required="required">
                </div>
            </div>
            <div class="form-group">
                <label for="region_id">Regions:</label>
                <select class="form-control" id="sel1"  name="region_id">
                    <?php foreach ($regions as $region) : ?>
                        <option value="<?php echo $region->id; ?>"><?php echo $region->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Select Profile Image</div>
                <div class="panel-body" align="center">
                    <input type="file" name="upload_image" id="upload_image" accept="image/*" />
                    <br />
                    <div id="uploaded_image"></div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Sign up</button>
            </div>
            <?php echo form_close(); ?>
    </section>
</div>

<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_cropper" style="width:350px; margin-top:30px ; height:400px"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success crop_image" id="crop_img_btn">Crop</button>
                <button type="button" class="btn btn-default" id="crop_close_btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/croppie.js"></script>
<script>
var input_file = document.getElementById("profile_picture");
var button_crop = document.getElementById("crop_img_btn");
var button_close = document.getElementById("crop_close_btn");
var image_cropper = document.getElementById("image_cropper");
var crop = new Croppie(image_cropper, {
    enableExif: true,
    viewport: {
      width:150,
      height:300,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:350
    }
});

input_file.onchange = function(){
    var reader = new FileReader();
    reader.onload = function (event) {
        crop.bind({
        url: event.target.result
    }).then(function(){
        console.log('jQuery bind complete');
    });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').show();
}

button_crop.onclick = function(event){
    crop.result({
        type: 'canvas',
        size: 'viewport',
        format: 'jpeg'
    }).then(function(response){
        $("#cropped_image").val(response);
        $('#uploadimageModal').hide();
    })
}
button_close.onclick = function(e){
    $("#uploadimageModal").hide();
}


</script>
