
    <!-- Page Content -->
    <div class="container container-holder">

      <div class="row">

        <div class="col-lg-3 aside-search">


          <div class="list-group">
              <h1 class="my-4">City/Regions</h1>
            <?php foreach($regions as $region) : ?>
            <a class="list-group-item" href="<?php echo site_url('/girls/regions_new/'.$region['region_id']); ?>"><img class="sidebar_img" src="<?php echo base_url(); ?>assets/images/location.png" alt=""><?php echo $region['name']; ?></a>
            <?php endforeach; ?>
          </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9 new_girls_wrapper">

          <div class="row top-filters">


          <div class="row new_girls_row">
            <?php foreach($new_girls as $new_girl) : ?>
            <div class="col-lg-11 col-md-8 mb-14 new-girl-holder">
              <div class="card new_girl_card">
                <div class="row new_girls_row row_margin">
                <div class="img-holder new-girls col-md-7">
                  <a href="<?php echo site_url('/girls/view_profile/'.$new_girl['slug']); ?>">
                    <img class="big-img" class="card-img-top big_phone" src="<?php echo site_url('/assets/images/girls/'.$new_girl['slug']); ?>.jpg" alt="">
                    <img class="small-img small_img_first small_phone" class="card-img-top" src="<?php echo site_url('/assets/images/'.$new_girl['slug']); ?>/1.jpg" alt="">
                    <img class="small-img small_img_sec small_phone" class="card-img-top" src="<?php echo site_url('/assets/images/'.$new_girl['slug']); ?>/2.jpg" alt="">
                    <div class="cleaner"></div>
                  </a>
                </div>
                <div class="card-body col-md-5">
                  <h4 class="card-title new_girls_h4">
                    <a href="<?php echo site_url('/girls/view_profile/'.$new_girl['slug']); ?>"><?php echo $new_girl['username']; ?></a>
                  </h4>
                  <h6><b>City</b>: <?php echo $new_girl['name']; ?></h6>
                  <h6><b>Age</b>: <?php echo $new_girl['age']; ?></h6>
                  <h6><b>Phone</b>: <?php echo $new_girl['contact']; ?></h6>
                  <h6><b>Description</b>:</h6>
                </div>
                </div>
             </div>
            </div>
            <?php endforeach; ?>
          </div>
          <!-- /.row -->
          <?=$pagination_links?>
        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->
    </div>
    </div>
    </div>
    <!-- /.container -->
