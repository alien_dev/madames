<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Lib\Alert;
use Lib\Validate;
use Carbon\Carbon;

/**
 * Contact
 */
class Account extends MY_Controller {

    public $tab_title = "Madames | Account";

    public function __construct() {
        parent::__construct();
        //$group = $this->ion_auth->get_users_groups($user->id)->result();
    }

    public function login()
    {
        if ($this->ion_auth->logged_in()) {
            redirect('home');
        } else {
            $this->tab_title = 'Madames | Login';

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->input->post()) {
                $username = $this->input->post('username');
                //Get and encrypt password
                $password = $this->input->post('password');
                $remember = $this->input->post('remember');



                if ($this->form_validation->run() === TRUE) {
                    if ($this->ion_auth->login($this->input->post('username'), $this->input->post('password'), $remember)) {
                        Alert::setSuccess("Welcome");
                        $user_data = $this->ion_auth->user()->row();
                        $group_id = $this->ion_auth->get_users_groups()->row()->id;//Vraca prvu pronadjenu grupu kojoj korisnik pripada. Revidirati
                        $user_data_username = $user_data->username;
                        $user_data_phone = $user_data->phone;
                        $slug = url_title($user_data_phone);
                        $slug = trim($slug);
                        $slug = $user_data_username.str_replace(" ", "-", $slug);
                        $this->session->set_userdata((array) $user_data);

                        //if ($this->ion_auth->in_group('members')) // madames (ID: 2)
                        if ($this->ion_auth->in_group('members')) {
                            redirect('girls/profile/' .$slug);
                        } else if ($this->ion_auth->in_group(3)) {
                            redirect('clubs/profile/' .$slug);
                        }
                    } else {
                        //Set message
                        Alert::setError($this->ion_auth->errors());
                        #redirect('girls/login');
                        $this->render('account/login');
                    }
                } else {
                    Alert::setLocalError(validation_errors());
                    $this->render('account/login');
                }
            } else
                $this->render('account/login');
        }
    }

    public function login_club() {
        if ($this->ion_auth->logged_in()) {
            redirect('home');
        } else {
            $this->tab_title = 'Madames | Login';

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->input->post()) {
                $username = $this->input->post('username');
                //Get and encrypt password
                $password = $this->input->post('password');
                $remember = $this->input->post('remember');



                if ($this->form_validation->run() === TRUE) {
                    if ($this->ion_auth->login($this->input->post('username'), $this->input->post('password'), $remember)) {
                        Alert::setSuccess("Welcome");
                        $user_data = $this->ion_auth->user()->row();
                        $user_data_username = $user_data->username;
                        $user_data_phone = $user_data->phone;
                        $slug = url_title($user_data_phone);
                        $slug = trim($slug);
                        $slug = str_replace(" ", "-", $slug);
                        $this->session->set_userdata($user_data);
                        redirect('clubs/profile/' . $slug);
                    } else {
                        //Set message
                        Alert::setError($this->ion_auth->errors());
                        #redirect('girls/login');
                        $this->render('account/login');
                    }
                } else {
                    Alert::setLocalError(validation_errors());
                    $this->render('account/login');
                }
            } else
                $this->render('account/login');
        }
    }

    public function register() {
        if ($this->ion_auth->logged_in()) {
            redirect('home');
        } else {
            $region = $this->model('region');
            $data['title'] = 'Register';
            $data['regions'] = $region->all();
            $dt = Carbon::parse(1545514052);
            $this->render('register', $data);
        }
    }

    private function storeImage($imageb64, $image_name) {
        if (!empty($imageb64)) {
            $b64part = explode(";", $imageb64)[1];
            $image_data = explode(",", $b64part)[1];
            $image = base64_decode($image_data);
            file_put_contents($image_name, $image);
        }
    }

    public function storeTempImg()
    {
        if(isset($_FILES['cropped_image']))
        {
            $cropped_image = $_FILES['cropped_image'];
            $file_to_upload = $_FILES['cropped_image']['tmp_name'];
            $image_ext = explode("/",$cropped_image['type'])[1];
            $file_name = $_SESSION['user_session_id'].".png";
            $_SESSION['image_temp_name'] = $file_name;
            $_SESSION['image_ext'] = $image_ext;
            move_uploaded_file($file_to_upload, APPPATH.'runtime/temp/'.$file_name);
        }
    }
    public function moveImageFromTemp($name)
    {
        move_uploaded_file(APPPATH.'runtime/temp/'.$_SESSION['image_temp_name'], $name);
    }

    //TODO: prepakovati cuvanje podataka za madames i clubes tabele, revidirati celu logiku
    public function store_user() {
        $this->lang->load('auth', 'en');
        $this->lang->load('ion_auth', 'en');
        $this->lang->load('form_validation', 'en');
        $data['regions'] = $this->girl_model->get_regions();

        if (!isset($_FILES['userfile']) || $_FILES['userfile']['name'] === '') {
            $rules = [
                'field' => 'userfile',
                'label' => $this->lang->line('Image'),
                'rules' => 'required',
            ];
            Validate::appendRules($rules);
        }
        $group = $this->input->post('group_id');
        if ($group == 2) { //general user - madames
            $validate = Validate::form($this->girl_model->registerRules());
            ;
        } else if ($group == 3) { //clubs
//            Validate::appendRules([
//                [
//                    'field' => 'club_name',
//                    'label' => $this->lang->line('ClubName'),
//                    'rules' => 'required',
//                ],
//                [
//                    'field' => 'open_time',
//                    'label' => $this->lang->line('OpenTime'),
//                    'rules' => 'required',
//                ],
//            ]);
            $validate = Validate::form($this->club_model->registerRules());
        }



//         if ($validate)
//         {
//             $validate = Validate::form($this->girl_model->registerRules());;
//         }
//         else  //clubs
//         {
// //            Validate::appendRules([
// //                [
// //                    'field' => 'club_name',
// //                    'label' => $this->lang->line('ClubName'),
// //                    'rules' => 'required',
// //                ],
// //                [
// //                    'field' => 'open_time',
// //                    'label' => $this->lang->line('OpenTime'),
// //                    'rules' => 'required',
// //                ],
// //            ]);
//             $validate = Validate::form($this->club_model->registerRules());
//         }
        if ($validate) {
            if ($this->input->post('username')) {
                $username = $this->input->post('username');
            } else {
                $username = $this->input->post('club_name');
            }

            $password = $this->input->post('password');
            $phone = $this->input->post('phone');
            $email = $this->input->post('email');
            $cropped_b64 = $this->input->post('cropped_b64');
            $slug = $phone;
            $slug = str_replace(" ", "-", $slug);
            //$group = 2; //General user
            // $this->storeImage($cropped_b64, './assets/images/girls/'.$slug.'.jpg');

            $additional_data = array(
                'TS' => $this->input->post('ts'),
                'age' => $this->input->post('age'),
                'region_id' => $this->input->post('region_id'),
                'username' => $username,
                'phone' => $phone,
                'group_id' => $this->input->post('group_id'),
                'slug' => $slug = str_replace(" ", "-", $slug),
                'profile_photo' => $slug.'.png',
                'club' => $this->input->post('club_name'),
                'address' => $this->input->post('address'),
            );

            if ($this->ion_auth->register($username, $password, $email, $additional_data, [$group])) {
                //$this->storeImage($cropped_b64, './assets/images/girls/' . $slug . '.jpg');
                rename(APPPATH.'runtime/temp/'.$_SESSION['image_temp_name'], FCPATH.'assets/images/girls/' . $slug . '.png');
                unset($_SESSION['image_temp_name']);
                unset($_SESSION['image_ext']);
                //Upload profile photo
                /* $config = array(
                  'upload_path' => './assets/images/girls',
                  'allowed_types' => 'jpg|png',
                  'max_size' => '5048',
                  'max_height' => '2048',
                  'max_width' => '3076',
                  'file_name' => $slug,
                  );
                  $this->load->library('upload', $config);

                  if(!$this->upload->do_upload()){
                  $errors = array('error' => $this->upload->display_errors());
                  $profile_photo= 'noimage.jpg';
                  printr($errors); die();
                  } else {
                  $image = './assets/images/girls/'.$slug.'.jpg';
                  $config['image_library'] = 'gd2';
                  $config['source_image'] = $image;
                  $config['create_thumb'] = TRUE;
                  $config['maintain_ratio'] = FALSE;
                  $config['width']         = 185;
                  $config['height']       = 220;
                  $this->load->library('image_lib', $config);\
                  if ( ! $this->image_lib->resize())
                  {
                  echo $this->image_lib->display_errors();die();
                  }
                  } */
                if ($group == 2) { //general user - madames
                    /* $this->girl_model->save([
                      'TS' => $this->input->post('ts'),
                      'age' => $this->input->post('age'),
                      'region_id' => $this->input->post('region_id'),
                      'username' => $username,
                      'phone' => $phone,
                      'slug' => $slug = str_replace(" ", "-", $slug),
                      'profile_photo' => $slug.'.jpg'
                      ]); */
                }
                /* else if ($group == 3) //clubs
                  {
                  $this->club_model->save([
                  'club_name' => $this->input->post('club_name'),
                  'address' => $this->input->post('address'),
                  'phone' => $phone,
                  'created_on' => time(),
                  ]);
                  } */
                Alert::setSuccess("Welcome. You can log in now into your account.");
                redirect("account/login");
            } else {
                Alert::setError($this->ion_auth->errors());
                #redirect("girls/register");
            }
        } else {
            Alert::setError(validation_errors());
            #$this->render('register', $data);
            redirect("account/register");
        }
    }

    public function changePassword() {
        $this->lang->load('auth', 'en');
        $this->lang->load('ion_auth', 'en');
        $this->lang->load('form_validation', 'en');

        $rules = [
            [
                'field' => 'old_password',
                'label' => $this->lang->line('change_password_validation_old_password_label'),
                'rules' => 'required|char_plus|max_length[64]|min_length[4]',
            ],
            [
                'field' => 'new_password',
                'label' => $this->lang->line('change_password_validation_new_password_label'),
                'rules' => 'required|char_plus|max_length[64]|min_length[4]|matches[new_password_confirm]',
            ],
            [
                'field' => 'new_password_confirm',
                'label' => $this->lang->line('change_password_validation_new_password_confirm_label'),
                'rules' => 'required|char_plus|max_length[64]|min_length[4]',
            ],
        ];

        $validate = Validate::form($rules);

        if (!$validate) {
            Alert::setError(validation_errors());
            redirect(base_url() . "girls/profile/" . $this->USER->slug);
        } else {
            $identity = $this->session->userdata('identity');

            $change = $this->ion_auth->change_password($identity, $this->input->post('old_password'), $this->input->post('new_password'));

            if ($change) {
                //if the password was successfully changed
                //$this->logout();
                Alert::setSuccess("Password successfully changed.");
                redirect(base_url() . "girls/profile/" . $this->USER->slug);
            } else {
                Alert::setError($this->ion_auth->errors());
                redirect(base_url() . "girls/profile/" . $this->USER->slug);
            }
        }
    }

    public function logout() {
        //$this->girl_model->update_status($this->USER->id,0);
        $this->ion_auth->logout();
        redirect('account/login');
    }

}

/* End of file NewGirls.php */
/* Location: ./application/controllers/NewGirls.php */
