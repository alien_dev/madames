<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Lib\Alert;
/**
 * Contact
 */
class Contact extends MY_Controller
{
	public $tab_title = "Madames | Contact";
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

	public function index()
	{
        $this->render('index');
	}

	public function send() {
			$this->load->library('email');
			$config = array(
				'mailtype' => "text",
			);
			$this->email->initialize($config);

			$from = $this->input->post('email');
			// $to = $this->input->post('');
			$subject = $this->input->post('subject');
			$message = $this->input->post('message');

			$this->email->set_newline("\r\n");
			$this->email->from($from);
			$this->email->to('158f618b8f-66465c@inbox.mailtrap.io');
			$this->email->subject($subject);
			$this->email->message($message);

			if ($this->email->send()) {
					echo 'Your Email has successfully been sent.';
					Alert::setSuccess("Your Email has successfully been sent.");
					redirect(base_url()."Contact");
			} else {
					show_error($this->email->print_debugger());
			}
	}
}

/* End of file NewGirls.php */
/* Location: ./application/controllers/NewGirls.php */
