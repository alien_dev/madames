
<!-- Page Content -->
<div id="listing">

  <div>
    <div class="row">
    <div class="col-lg-3 aside-search">
        <div class="list-group" id="regions">
            <h1 class="my-4 slide-down">
                City / Regions
                <div class="angle-down-div">
                    <i class="fas fa-angle-down"></i>
                </div>
            </h1>
            <div class="slide-down-list">
                <li v-for="region in filters.regions"
                @click="filterRegion(region.id, $event)"
                class="list-group-item lis_li">
                    <!-- <img class="sidebar_img" :src="base_url+'assets/images/LokacijaM.png'" alt=""> -->
                    <span style="pointer-events: none;" class="icon-m-location"></span>
                    <div style="display:inline-block;pointer-events: none;" class="side_p_reg">
                    {{region.name}}
                    </div>
                </li>
            </div>
        </div>
        <!-- <slot name="sidebar"></slot> --><!-- <div slot="sidebar"> -->
        <div class="aside-search" style="margin-top:30px;">
            <div class="list-group online-girls" id="online-girls">
                <h1 class="my-4 slide-down" >
                    Live girls
                    <div class="angle-down-div">
                        <i class="fas fa-angle-down"></i>
                    </div>
                </h1>
                <div class="slide-down-list">
                    <li href="#"
                    v-for="online_girl in online_girls"
                    @click="chatAdapter(online_girl.id, online_girl.username, $event);" class="list-group-item live_girl_a">
                        <img class="live_girl_pic"
                        :src="'assets/images/'+ (online_girl.profile_photo ? 'girls/'+online_girl.profile_photo : 'default.jpg')" alt="">
                        <p style="float:left;">{{online_girl.username}}</p>
                        <img style="float:right;width:23px;height:23px;margin-top:4px;margin-right:5px;" class="" src="assets/images/live.png">
                        <div style="clear:both;"></div>
                    </li>
                </div>
            </div>
        </div>
    </div><!-- /.col-lg-3 -->

    <div class="col-lg-9">
        <div class="filter_lists " id="filters">
        <div class="row">
            <div class="col-md-12" >
               <div class="dropdown text-center slide-down">
                    <p>Filters</p>
                    <div class="angle-down-div">
                        <i class="fas fa-angle-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide-down-list" >
            <div class="row">
                <div class="col-md-2 col-sm-4 col-6 search-col mobile_fil">
                    <!-- <div class="search-field">
                        <input type="text" v-model="search_input" @keyup="search" style="" name="keywords" class="form-control input-sm search-input" placeholder="Search">

                    </div> -->
                    <div class="form-group has-search">
                      <span class="fa fa-search form-control-feedback"></span>
                      <input type="text" v-model="search_input" @keyup="search" name="keywords" class="form-control" placeholder="Search">
                    </div>
                </div>

                <div class="col-md-2 col-sm-4 col-6 mobile_fil ser">
                    <div class="dropdown" style="padding-top:2px;">
                      <span class="btn filters_btn" data-toggle="dropdown">
                      <span class="fil_tx">Services
                      </span><i class="fas fa-chevron-circle-down strelica_mob"></i></span>
                        <ul class="dropdown-menu filt ">
                            <li v-for="service in filters.services.value" @click.self.prevent="filterMultipleSelection('services', service.id, service.name, $event)">
                                {{service.name}} <i class="hidden far fa-check-circle"></i>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2 col-sm-4 col-6 mobile_fil" style="margin-top:2px;">
                    <div class="dropdown">
                      <span class="btn filters_btn" data-toggle="dropdown">
                      <span class="fil_tx">TS
                      </span><i class="fas fa-chevron-circle-down strelica_mob"></i></span>
                        <ul class="dropdown-menu filt">
                            <li @click.self.prevent="filterSingleSelection('ts', 1, 'TS - Yes', $event)">
                                Yes <i class="hidden far fa-check-circle"></i>
                            </li>
                            <li @click.self.prevent="filterSingleSelection('ts', 0, 'TS - No', $event)">
                                No <i class="hidden far fa-check-circle"></i>
                            </li>
                        </ul>
                    </div>
                </div>



                <div class="col-md-2 col-sm-4 col-6 mobile_fil" style="margin-top:2px;">
                    <div class="dropdown">
                      <span class="btn filters_btn" data-toggle="dropdown">
                      <span class="fil_tx">Age
                      </span><i class="fas fa-chevron-circle-down strelica_mob"></i></span>
                        <ul class="dropdown-menu filt">
                            <li v-for="age in filters.age.value" @click.self.prevent="filterSingleSelection('age', age, age, $event)">
                                {{age}} <i class="hidden far fa-check-circle"></i>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- <div class="col-md-2 col-sm-4 col-6">
                    <div class="dropdown">
                        <button class="btn filters_btn" type="button" data-toggle="dropdown">Services for
                            <i class="fas fa-chevron-circle-down"></i></button>
                        <ul class="dropdown-menu filt">
                            <li><a href="#">Man</a></li>
                            <li><a href="#">Couple</a></li>
                            <li><a href="#">Girl</a></li>
                            <li><a href="#">Orgy</a></li>
                        </ul>
                    </div>
                </div> -->

                <!-- <div class="col-md-2 col-sm-4 col-6" style="margin-top:1px;">
                    <div class="dropdown">
                        <button class="btn filters_btn" type="button" data-toggle="dropdown">Hair
                            <i class="fas fa-chevron-circle-down"></i></button>
                        <ul class="dropdown-menu filt">
                            <li v-for="hair in filters.hairs.value" @click.self.prevent="filterMultipleSelection('hairs', hair.id, hair.name, $event)">
                                {{hair.name}} <i class="hidden far fa-check-circle"></i>
                            </li>
                        </ul>
                    </div>
                </div> -->

                <div class="col-md-2 col-sm-4 col-6 mobile_fil" style="margin-top:2px;">
                    <div class="dropdown">
                      <span class="btn filters_btn" data-toggle="dropdown">
                      <span class="fil_tx">Type
                      </span><i class="fas fa-chevron-circle-down strelica_mob"></i></span>
                        <ul class="dropdown-menu filt">
                            <li v-for="type in filters.types.value" @click.self.prevent="filterMultipleSelection('types', type.id, type.name, $event)">
                                {{type.name}} <i class="hidden far fa-check-circle"></i>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2 col-sm-4 col-6 mobile_fil" style="margin-top:2px;">
                    <div class="dropdown">
                      <span class="btn filters_btn" data-toggle="dropdown">
                      <span class="fil_tx">Sort by
                      </span><i class="fas fa-chevron-circle-down strelica_mob"></i></span>
                        <ul class="dropdown-menu filt">
                            <li @click.self.prevent="filterSingleSelection('order', {'created_on':'desc'}, 'Newes first', $event)">
                                Newest <i class="hidden far fa-check-circle"></i>
                            </li>
                            <li @click.self.prevent="filterSingleSelection('order', {'created_on':'asc'}, 'Oldest first', $event)">
                                Older <i class="hidden far fa-check-circle"></i>
                            </li>
                            <li @click.self.prevent="filterSingleSelection('order', {'age':'asc'}, 'Youngest first', $event)">
                                Age - Youngest <i class="hidden far fa-check-circle"></i>
                            </li>
                            <li @click.self.prevent="filterSingleSelection('order', {'age':'desc'}, 'Youngest first', $event)">
                                Age - Oldest <i class="hidden far fa-check-circle"></i>
                            </li>
                            <li @click.self.prevent="filterSingleSelection('order', {'username':'asc'}, 'Alphabetic', $event)">
                                Username <i class="hidden far fa-check-circle"></i>
                            </li>
                        </ul>
                    </div>
                </div>



            </div><!-- row -->
        </div><!-- list -->
    </div>
    <filter-tags @remove-filter-tag="removeSelectedFilter($event)" :tags="tags"></filter-tags>

    <div v-if="responseStatus" v-html="response"></div><!-- views/ajax/get_girls.php -->

    </div> <!-- col-9 -->
    </div><!-- /.row -->
</div>

</div>

<div class="modal fade" id="chatSessionModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enter username:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php
         echo form_open('home/guest', ['id'=>'guest_session']);
         ?>
                <div class="alert alert-danger" id="error_msg" style="display:none"></div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" name="guest_username" id="guest_username">
                </div>

                <div class="checkbox">
                  <label> <input type="checkbox" value="" id="age"> I have more than 18 years</label>
                </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-cancel" style="border-radius: 3px;" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary aa" onclick="createChatSession()">Start chating</button>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>

<!-- Filter tags template -->
<script type="text/x-template" id="filter-tags">
  	<div>
		<div class="row filters_div_bottom">
			<ul class="filter_lists_bottom" v-if="tags.length>0">
				<li class="filters_first_bottom">
				<div class="dropdown">
					<p>You choose:</p>
				</div>
				</li>

				<li v-for="filter in tags">
				<button type="button" class="close filters_bottom_btn" aria-label="Close" @click="removeFilter(filter)">{{filter.display_value}}
					<span aria-hidden="true">&times;</span>
				</button>
				</li>
			</ul>
		</div>
	</div>
</script>

<style>
    .active-filter {
        background-color: #990000;
        color: white !important;
    }
    .active-filter a{
        color: white !important;
    }
    .online-girls {
        max-height: 500px;
        overflow: auto;
    }

	.filters_div_bottom{
		margin-left: 2px;
	}
	.filters_bottom_btn{
		background-color: #990000 !important;
		color: white !important;
		border-radius: 6px;
		font-size: 13px !important;
		padding: .200rem .500rem !important;
		min-height: 30px !important;
		opacity: 1 !important;
		margin-right: 5px;
	}
	.filter_lists_bottom{
		list-style-type: none;
		padding-left: 0;
		border-radius: 6px;
		padding-right: 20px;
		margin-bottom: 0 !important;
		margin-top: 5px;
	}
	.filters_first_bottom{
		color: black;
		margin-right: 15px;
	}
	.filters_first_bottom p{
		margin-bottom: 0;
		margin-top: 5px;
	}
	.filters_bottom_btn::after{
		border:0 !important;
	}
	.filters_bottom_btn span{
		margin-left: 2px;
	}
	.close:hover{
		background-color: #b30000 !important;
	}
    .filt {
        cursor: pointer;
    }

</style>
<script src="<?= base_url(); ?>assets/js/listing.js"></script>
<script src="assets/js/talkjs.js"></script>
<script>
$(".search-input").click(function() {
  $(".lupa").hide();
});
$(".search-input").focusout(function() {
  $(".lupa").show();
});
</script>
