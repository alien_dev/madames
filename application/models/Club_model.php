<?php

class Club_model extends CI_Model {

    public $table_name = 'users';

    public function table($value = '') {
        $this->db->select("
  			users.id as id, users.username, users.address, users.web, users.region_id,
  			users.slug, users.phone, users.email, users.profile_photo,
  			regions.name as city,
  		");
        $this->db->from($this->table_name);
        $this->db->join('regions', 'regions.id = users.region_id');
        $this->db->group_by("users.id"); //Druga alternativa za duplikate koja omogucava koriscenje agregatnih funkcija
    }

    public function get_clubs() {
        $group_id = 3;
        $this->db->join('regions', 'regions.id = users.region_id');
        $this->db->join('users_groups', 'users.id = users_groups.user_id');
        $this->db->join('groups', 'groups.id = users_groups.group_id');
        $query = $this->db->where('users_groups.group_id', $group_id)->get('users');
        return $query->result_array();
    }

    public function get_club_by_slug($slug) {
        $this->table();
        $group_id = 3;
        $this->db->join('users_groups', 'users.id = users_groups.user_id');
        $this->db->join('groups', 'groups.id = users_groups.group_id');
        $array = array('slug' => $slug, 'users_groups.group_id' => $group_id);
        $query = $this->db->where($array)->get();
        return $query->row();
    }

    public function create_club() {
        $slug = url_title($this->input->post('username'));
        $data = array(
            'username' => $this->input->post('username'),
            'slug' => $slug,
            'region_id' => $this->input->post('region_id'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password')
        );
        return $this->db->insert('clubs', $data);
    }

    public function get_regions() {
        $this->db->order_by('name');
        $query = $this->db->get('regions');
        return $query->result_array();
    }

    public function get_clubs_by_region($region_id, $take, $start) {
        $this->db->limit((int) $take, (int) $start);
        $group_id = 3;
        // $this->db->order_by('users.user_id','DESC');
        $this->db->join('regions', 'regions.id = users.region_id');
        $this->db->join('users_groups', 'users.id = users_groups.user_id');
        $this->db->join('groups', 'groups.id = users_groups.group_id');
        $query = $this->db->where('users_groups.group_id', $group_id)->get_where('users', array('users.region_id' => $region_id));
        return $query->result_array();
    }

    public function get_clubs_paginate($take, $start) {
        $this->db->limit((int) $take, (int) $start);
        $this->db->join('regions', 'regions.id = clubs.region_id');
        $query = $this->db->get('clubs');
        return $query->result_array();
    }

    public function total_clubs_by_region($region_id) {
        $this->db->join('regions', 'regions.id = clubs.region_id');
        $query = $this->db->get_where('clubs', array('clubs.region_id' => $region_id));
        return $query->num_rows();
    }

    public function get_region($id) {
        $query = $this->db->get_where('regions', array('region_id' => $id));
        return $query->row();
    }

    public function registerRules($passwords = TRUE) {
        $rules = [
            [
                'field' => 'username',
                'label' => 'Club name',
                'rules' => 'required|char_plus|max_length[64]|min_length[4]',
            /* 'errors'=> [
              'required' => $this->lang->line('form_validation_required'),
              'max_length' => $this->lang->line('form_validation_max_length'),
              'min_length' => $this->lang->line('form_validation_min_length'),
              'char_plus' => $this->lang->line('form_validation_char_plus'),
              #'is_unique' => $this->lang->line('form_validation_is_unique')
              ] */ # Genericke poruke nisu obavezne da se navode u errors
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|max_length[64]|valid_email|is_unique[clubs.email]',
            ],
            [
                'field' => 'password',
                'label' => $this->lang->line('create_user_validation_password_label'),
                'rules' => 'required|char_plus|max_length[64]|min_length[4]|matches[password_confirm]',
            ],
            [
                'field' => 'password_confirm',
                'label' => $this->lang->line('create_user_validation_password_confirm_label'),
                'rules' => 'required|char_plus|max_length[64]',
            ],
            [
                'field' => 'region_id',
                'label' => $this->lang->line('Region'),
                'rules' => 'required|numeric|max_length[9]',
            ],
            [
                'field' => 'cropped_b64',
                'label' => 'Cropped image',
                'rules' => 'max_length[1000000]', //Dodata karatkere dozvoljene u b64
            ],
            [
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'required|max_length[64]|numeric|is_unique[clubs.phone]',
            ],
        ];

        return $rules;
    }

    public function get_club_profile_image($slug) {
        $images_path = 'assets/images/girls/';
        $default_image = 'assets/images/club.jpg';
        $image_types = explode("|", $this->config->item('allowed_image_upload_types'));
        $club = $this->get_club_by_slug($slug);
        foreach ($image_types as $type) {
            $image = $images_path . $slug . '.' . $type;
            if (file_exists($image)) {
                return $image;
            }
        }
        return $default_image;
    }

}

?>
