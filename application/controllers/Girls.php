<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Lib\Validate;
use Lib\Security;
use Lib\Alert;

class Girls extends MY_Controller
{

    //variable for storing error message
    private $error;
    //variable for storing success message
    private $success;

    public function __construct()
    {
        parent::__construct();
        Security::unprotected([
            'girls/store_user',
            'girls/regions',
            'girls/regions_new',
            'girls/profile'
        ]);
        Security::authCheck();

        $this->load->model('region');
        $this->load->model('hair');
        $this->load->model('type');
        $this->load->model('service');
        $this->load->model('working_time_weekend');
        $this->load->model('working_time_mon_fri');
    }

    public function profile($slug){
        $data['girl'] = $this->girl_model->get_girl_by_slug($slug);
        // die($this->girl_model->get_girl_by_slug($slug));
        $data['services'] = $this->girl_model->get_services_for_girl($slug);
        $data['user_images'] = $this->girl_model->get_user_images($slug);
        $data['video'] = $this->girl_model->get_video($slug);
        // die($this->girl_model->get_video($slug));
        $data['slug'] = $slug;
        $data['types'] = $this->girl_model->get_types();
        $data['hairs'] = $this->girl_model->get_hairs();
        $data['work_time_weekend'] = $this->working_time_weekend->all();
        $data['work_time_mon_fri'] = $this->working_time_mon_fri->all();
        $data['services'] = $this->service->all();
        $data['user_to_services_one'] = $this->girl_model->get_first_twelve_users_to_services($data['girl']->id);
        $data['user_to_services_two'] = $this->girl_model->get_second_twelve_users_to_services($data['girl']->id);
        $data['prices'] = $this->model('UserToPrice')->searchModels(['user_slug'],$slug);
        $data['times'] = $this->model('time');
        $data['profile_owner'] = ($this->USER && $this->USER->slug == $slug) ? true : false;
        $data['user_settings'] = $this->model('UserSettings')->find(['user_id' => $data['girl']->id]);

        if(empty($data['girl'])){
            show_404();
        }
        $user_nickname = $data['girl']->username;
        $this->render('profile', $data);
    }

    public function regions($id, $paginate_start=0, $paginate_take=12){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'girls/regions/'.$id;
        $config['total_rows'] = $this->girl_model->total_girls_by_region($id);
        $config['per_page'] = $paginate_take;
        $config['full_tag_open'] = '<div class="pagination">'; //Wraper oko paginacije
        $config['full_tag_close'] = '</div>';
        $config['attributes'] = array('class' => 'pagination-links');
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();

        $data['girls'] = $this->girl_model->get_girls_by_region($id, $config["per_page"], $paginate_start);
        $data['new_girls'] = $this->girl_model->get_new_girls_by_region($id, $id, $config["per_page"], $paginate_start);
        $data['regions'] = $this->region->all();
        $data['region_id'] = $id;

        $this->render('home/index', $data);
    }

    public function regions_new($id, $paginate_start=0, $paginate_take=10)
    {
        $this->load->library('pagination');
        $config['base_url'] = base_url().'girls/regions_new/'.$id;
        $config['total_rows'] = $this->girl_model->total_girls_by_region($id);
        $config['per_page'] = $paginate_take;
        $config['full_tag_open'] = '<div class="pagination">'; //Wraper oko paginacije
        $config['full_tag_close'] = '</div>';
        $config['attributes'] = array('class' => 'pagination-links');
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();

        $data['new_girls'] = $this->girl_model->get_new_girls_by_region($id, $config["per_page"], $paginate_start);
        $data['regions'] = $this->region->all();
        $data['lives'] = $this->girl_model->findByAttributes(['chat_status'=>1]);
        $data['region_id'] = $id;

        $this->render('newgirls/index', $data);
    }
       // Check username exists
    public function check_username_exists($username){
        $this->form_validation->set_message('check_username_exists','That username is taken. Please choose another username');
        if($this->girl_model->check_username_exists($username)){
            return true;
        }else{
            return false;
        }
    }

       // Check email exists
    public function check_email_exists($email)
    {
        $this->form_validation->set_message('check_email_exists','That email is taken. Please choose another username');
        if($this->girl_model->check_email_exists($email)){
            return true;
        }else{
            return false;
        }
    }

    public function phone_number_format()
    {
        $data['girls'] = $this->girl_model->get_girls();
        $contact = $girls['contact'];
    }

    private function storeImage($imageb64, $image_name)
    {
        $b64part = explode(";",$imageb64)[1];
        $image_data = explode(",",$b64part)[1];
        $image = base64_decode($image_data);
        file_put_contents($image_name, $image);
    }

    public function storeTempImg()
    {
        if(isset($_FILES['cropped_image']))
        {
            $cropped_image = $_FILES['cropped_image'];
            $file_to_upload = $_FILES['cropped_image']['tmp_name'];
            $image_ext = explode("/",$cropped_image['type'])[1];
            $file_name = $_SESSION['user_session_id'].".png";
            $_SESSION['image_temp_name'] = $file_name;
            $_SESSION['image_ext'] = $image_ext;
            move_uploaded_file($file_to_upload, APPPATH.'runtime/temp/'.$file_name);
        }
    }
    public function moveImageFromTemp($name)
    {
        move_uploaded_file(APPPATH.'runtime/temp/'.$_SESSION['image_temp_name'], $name);
    }
    public function update_profile_photo()
    {
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $girl = $this->girl_model->get_girl_by_slug($_SESSION['slug']);
        $actual_profile_photo ='assets/images/girls/'.$_SESSION['slug'].'.png';
        $cropped_b64 = $this->input->post('cropped_b64');
        if(file_exists($actual_profile_photo)){
            unlink($actual_profile_photo);
        }
               //Upload profile photo
        // $config = array(
        //     'upload_path' => './assets/images/girls',
        //     'allowed_types' => 'jpg|png',
        //     'max_size' => '204800',
        //     'max_height' => '102400',
        //     'max_width' => '102400',
        //     'file_name' => $_SESSION['username'],
        // );

        rename(APPPATH.'runtime/temp/'.$_SESSION['image_temp_name'], FCPATH.'assets/images/girls/' . $_SESSION['slug'] .'.png');
        unset($_SESSION['image_temp_name']);
        unset($_SESSION['image_ext']);
        clearstatcache();
        $this->output->delete_cache('http://localhost/ongit/project/madames/assets/images/girls/'.$_SESSION['slug'].'.png');
        redirect(base_url().'girls/profile/'.$_SESSION['slug'], 'refresh');
        // //$this->upload->initialize($config);
        //
        // if(!$this->upload->do_upload()){
        //     $errors = array('error' => $this->upload->display_errors());
        //     $profile_photo= 'noimage.jpg';
        // }else{
        //     clearstatcache();
        //     $this->output->delete_cache('http://localhost/ongit/project/madames/assets/images/girls/Milica.jpg');
        //     redirect(base_url().'girls/profile/'.$_SESSION['username'], 'refresh');
        // }
    }

    public function bio($slug){
        $region = $this->model('region');
        $data['girl'] = $this->girl_model->get_girl_by_slug($slug);
        if($data['girl']->id !== $this->USER->id) show_404(); //Ukoliko ulogovani korisnik pristuma tudjoj formi
        $data['types'] = $this->type->all();
        $data['work_time_weekend'] = $this->working_time_weekend->all();
        $data['work_time_mon_fri'] = $this->working_time_mon_fri->all();
        $data['working_hours'] = $this->girl_model->get_working_time_hours();
        $data['servicefor'] = $this->girl_model->get_servicesfor();
        $data['orientations'] = $this->girl_model->get_orientation();
        $data['hairs'] = $this->hair->all();
        $data['regions'] = $region->all();
        $data['slug'] = $slug;
        if(empty($data)){
            show_404();
        }
        $user_nickname = $data['girl']->username;
        $this->render('bio', $data);
    }

    public function contact($slug){
        $region = $this->model('region');
        $data['girl'] = $this->girl_model->get_girl_by_slug($slug);
        if($data['girl']->id !== $this->USER->id) show_404(); //Ukoliko ulogovani korisnik pristuma tudjoj formi
        $data['types'] = $this->type->all();
        $data['hairs'] = $this->hair->all();
        $data['regions'] = $region->all();
        $data['slug'] = $slug;
        if(empty($data)){
            show_404();
        }
        $user_nickname = $data['girl']->username;
        $this->render('contact', $data);
    }

    public function update_bio($slug){
        $this->girl_model->update_bio();
        redirect('girls/profile/'.$slug);
    }

    public function update_contact($slug){
        $this->girl_model->update_contact();
        redirect('girls/profile/'.$slug);
    }

    public function update_services($slug){
        $this->girl_model->update_service($slug);
        redirect('girls/profile/'.$slug);
    }

    public function services($slug){
        $data['girl'] = $this->girl_model->get_girl_by_slug($slug);
        $data['regions'] = $this->region->all();
        $data['services'] = $this->service->all();
        $data['user_to_services'] = $this->girl_model->get_users_to_services($data['girl']->id);

        if(empty($data)){
         show_404();
        }
        $user_nickname = $data['girl']->username;
        $this->render('services', $data);
    }

    public function prices($slug)
    {
        $data['girl'] = $this->girl_model->get_girl_by_slug($slug);
        $data['regions'] = $this->girl_model->get_regions();
        $data['slug'] = $slug;
        //$data['prices'] = $this->model('UserToPrice')->searchModels(['user_slug'],$slug);
        $data['times'] = $this->model('time')->all();
        $data['currencies'] = $this->model('currency')->all();

        if(empty($data)){
            show_404();
        }
        $user_nickname = $data['girl']->username;

        $this->render('prices', $data);

    }

    public function deactivate($slug)
    {
      $this->lang->load('auth', 'en');
      $this->lang->load('ion_auth', 'en');
      $this->lang->load('form_validation', 'en');
      $password = $this->input->post('deactivate_password');

      $rules = [
          [
              'field' => 'deactivate_password',
              'label' => $this->lang->line('confirm_password_label'),
              'rules' => 'required|char_plus|max_length[64]|min_length[4]',
          ],
      ];

      $validate = Validate::form($rules);

      if (!$validate)
      {
          Alert::setError(validation_errors());
          redirect(base_url()."girls/profile/".$slug);
      }else{
        if($this->girl_model->deactivate_girl($slug,$password)){
          $this->ion_auth->logout();
          redirect('/');
        }else{
          Alert::setError($this->ion_auth->errors());
          redirect(base_url()."girls/profile/".$slug);
        }
      }
    }

    public function upload_images()
    {
        $this->load->library('image_lib');
        $url = $_SERVER['HTTP_REFERER'];
        // $this->printr($_FILES);
        if (!isset($_FILES['images']) || $_FILES['images']['name'][0] == '') {
            redirect($url);
        }

        /* php.ini
          upload_max_filesize = 1000M ;1GB
          post_max_size = 1000M
        */
        //ini_set('upload_max_filesize','20M');

        $images_path                    = 'assets/images/'.$_SESSION['slug'].'/';
        $config['allowed_types']        = $this->config->item('allowed_image_upload_types'); # config.php
        $config['max_size']             = 100000000;//100MB
        $config['max_width']            = 8000;
        $config['max_height']           = 8000;

        $this->load->library('upload');
        $this->load->helper('directory');
        $this->load->helper('file');

        if (!file_exists($images_path)) {
            mkdir($images_path, 0777, true);
        }

        $errors = false;
        $files = $_FILES;
        $files_cnt = count($_FILES['images']['name']);
        $user_folder = directory_map('./'.$images_path);


        // check EXIF and autorotate if needed
        // $this->load->library('image_autorotate', array('filepath' => $full_path));

        for ($i=0; $i < $files_cnt; $i++) {

            if (!empty($user_folder)) {
                $next = read_file($images_path.'/next.txt');
                $config['file_name'] = $next;
                write_file('./'.$images_path.'next.txt', $next+1);
            } else{
                $config['file_name'] = ($i+1);
                write_file('./'.$images_path.'next.txt', ($i+2));
            }

            $config['upload_path'] = $images_path;
            $_FILES['images']['name'] = $files['images']['name'][$i];
            $_FILES['images']['type'] = $files['images']['type'][$i];
            $_FILES['images']['tmp_name'] = $files['images']['tmp_name'][$i];
            $_FILES['images']['error'] = $files['images']['error'][$i];
            $_FILES['images']['size'] = $files['images']['size'][$i];
            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload('images'))
            {
                // $this->printr($this->upload->display_errors());
            }
            else
            {
            $erros = false;
            $imageinfo = $this->upload->data();
            $full_path = $imageinfo['full_path'];

            // check EXIF and autorotate if needed
            $this->load->library('image_autorotate', array('filepath' => $full_path));
              /*$data['upload_data'] = $this->upload->data();
              printr($data['upload_data']);*/
            $config['source_image'] = $full_path;
            $this->cropAuto($full_path);

            }
        }
        if (!$erros) {
            redirect(base_url().'girls/profile/'.$_SESSION['slug']);
        }
    }
    public function cropAuto($img)
    {
     $this->_img = imagecropauto($img, IMG_CROP_DEFAULT);
     return $this;
   }
    public function deleteimg($img)
    {
        unlink('assets/images/'.$_SESSION['slug'].'/'.$img);
        redirect(base_url().'girls/profile/'.$_SESSION['slug']);
    }
    public function deletevid($vid)
    {
      unlink('assets/videos/'.$vid);
        redirect(base_url().'girls/profile/'.$_SESSION['slug']);
    }

    public function test($value='')
    {
        $this->lang->load('auth', 'en');
        $this->lang->load('ion_auth', 'en');
        $this->lang->load('form_validation', 'en');
        $rule = [
            [
                'field' => 'first_name',
                'label' => 'lang:index_fname_th',
                'rules' => 'required|max_length[64]|alpha_space',
                'errors'=> [
                    'required' => lang('form_validation_required'),
                    'alpha_space' => $this->lang->line('form_validation_alpha'),
                    'max_length' => $this->lang->line('form_validation_max_length')
                ]
            ],
        ];
        $data = [
            'first_name' => ''
        ];
        Validate::form($rule, $data);

        printr(validation_errors());
        printr(form_error('first_name'));
        printr(Validate::$errors);
    }
    public function video_upload()
    {
      $actual_video ='assets/videos/'.$_SESSION['slug'].'.mp4';
      if(file_exists($actual_video)){
      if(file_exists($actual_video)){
          unlink($actual_video);
          clearstatcache();
      }
    }else{
      $actual_video ='assets/videos/'.$_SESSION['slug'].'.mov';
      if(file_exists($actual_video)){
          unlink($actual_video);
          clearstatcache();
      }
    }
             //Upload profile photo
      $config = array(
          'upload_path' => './assets/videos',
          'allowed_types' => 'mp4|mov',
          'max_size' => '10000000000000000000000000000000',
          'file_name' => $_SESSION['slug'],
      );

      $this->load->library('upload', $config);
      //$this->upload->initialize($config);

      if(!$this->upload->do_upload()){
          echo $this->upload->display_errors();
      }else{
        clearstatcache();
        redirect(base_url().'girls/profile/'.$_SESSION['slug'], 'refresh');
      }

    }

}



?>
