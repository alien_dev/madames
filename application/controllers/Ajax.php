<?php
use Lib\Security;
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Home
 */
class Ajax extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
         }
        //CSRF provera
    }

    public function getGirls($paginate_start=0, $paginate_take=12)
    {
        $search_params = [
            'age' => $this->input->post('age'),
            'services_id' => json_decode($this->input->post('services')),
            'hairs' => json_decode($this->input->post('hairs')),
            'type' => json_decode($this->input->post('type')),
            'ts' => json_decode($this->input->post('ts')),
            'order' => json_decode($this->input->post('order')),
            'order_age' => $this->input->post('order_age'),
            'order_username' => $this->input->post('order_username'),
            'region' => $this->input->post('region'),
            'scope' => 'users_groups.group_id = 2',
            'username' => $this->input->post('username')
        ];
        $this->load->library('pagination');
        $this->load->model('service');
        $this->load->model('region');

        $config['base_url'] = base_url().'ajax/getgirls';
        $config['per_page'] = $paginate_take;
        $config['total_rows'] = count($this->girl_model->search_girls($search_params));
        $config['full_tag_open'] = '<div class="pagination">'; //Wraper oko paginacije
        $config['full_tag_close'] = '</div>';
        $config['attributes'] = array('class' => 'pagination-links', 'onclick' => "EventBus.\$emit('pag', event)");
        $this->pagination->initialize($config);

        #printr($search_params);
        #printr($this->db->last_query());
        $data['girls'] = $this->girl_model->search_girls($search_params, $paginate_take, $paginate_start);
        $data['services'] = $this->service->all();
        $data['regions'] = $this->region->all();
        $data['pagination_links'] = $this->pagination->create_links();
        $this->layout = null;
        echo $this->render('get_girls', $data, true);
    }

    public function getModels() //Ovu metodu poziva vue kako bi ucitao potrebne modele
    {
        $model_names = json_decode($this->input->post('model_names'));
        $model_pagination = json_decode($this->input->post('model_pagination'));
        $models = [];

        foreach ($model_names as $model_name)
        {
            $model = $this->model($model_name);
            if (isset($model_pagination->{$model_name}))
            {
                $pagination = $model_pagination->{$model_name};
                $models[$model_name] = $model->all($pagination->take, $pagination->start);
            }
            else
            {
                $models[$model_name] = $model->all();
            }
        }

        $models['online_girls'] = $this->girl_model->findByAttributes(['chat_status'=>1]);

        echo json_encode($models);
    }

    public function setGuestChatSession()
    {
        $username = $this->input->post('username');
        $chat_session = [
            'chat_id' => Security::tokenCode(4, true),
            'chat_name' => $username,
            'configuration' => 'guest'
        ];
        $this->session->set_userdata($chat_session);
        echo json_encode($username);
    }

    public function getUserChatSession()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group('members'))
        {
            if($this->USER->chat_status == 1)
            {
                $chat_session = [
                    'id' => $this->USER->id,
                    'name' => $this->USER->username,
                    'configuration' => 'members'
                ];
            }
            else
            {
                $chat_session = '';
            }

        }
        else
        {
            if(isset($this->session->chat_id))
            {
                $chat_session = [
                    'id' => $this->session->chat_id,
                    'name' => $this->session->chat_name,
                    'configuration' => 'guest'
                ];
            }
            else
            {
                echo '';
                return;
            }
        }
        echo json_encode($chat_session);
    }

}

?>
