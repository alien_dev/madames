<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contact
 */
class Activate extends MY_Controller
{
	public $tab_title = "Madames | Activate";
	public function __construct()
    {
        parent::__construct();

    }

	public function index()
	{
        $this->load->view('activate/index');
	}
}
