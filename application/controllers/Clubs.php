<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clubs extends MY_Controller
{
    public $tab_title = "Madames | Clubs";

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->load->library('pagination');
        $this->load->model('region');
        $data['clubs'] = $this->club_model->get_clubs();
        $data['regions'] = $this->region->all();
        $data['pagination_links'] = $this->pagination->create_links();

        $data['regions'] = $this->region->all();

        $this->render('index', $data);
    }

    public function profile($slug = NULL){
        $data['club'] = $this->club_model->get_club_by_slug($slug);
        $data['regions'] = $this->girl_model->get_regions();
        $data['slug'] = $slug;
        $data['times'] = $this->model('time');
        $data['profile_owner'] = ($this->USER && $this->USER->slug == $slug) ? true : false;

        if(empty($data['club'])){
            show_404();
        }
        $user_nickname = $data['club']->username;
        $this->render('profile', $data);
    }

    public function regions($id, $paginate_start=0, $paginate_take=12){
        $this->load->library('pagination');
        $this->load->model('region');
        $config['base_url'] = base_url().'clubs/regions/'.$id;
        $config['total_rows'] = $this->club_model->total_clubs_by_region($id);
        $config['per_page'] = $paginate_take;
        $config['full_tag_open'] = '<div class="pagination">'; //Wraper oko paginacije
        $config['full_tag_close'] = '</div>';
        $config['attributes'] = array('class' => 'pagination-links');
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();

        $data['clubs'] = $this->club_model->get_clubs_by_region($id, $config["per_page"], $paginate_start);
        $data['regions'] = $this->region->all();
        $data['region_id'] = $id;

        $this->render('clubs/index', $data);
    }

 // public function profile($user_nickname = NULL){
 //   $data['girl'] = $this->girl_model->get_girls($user_nickname);
 //
 //   $data['title'] = $data['girl']['user_nickname'];
 //
 //   $this->load->view('templates/header');
 //   $this->load->view('pages/profile', $data);
 //   $this->load->view('templates/footer');
 // }
}



?>
