<?php 
namespace Lib;

/**
* 		
*/
class Security 
{
	protected static $CI = null;
	protected static $error = null;
	protected static $message = null;
	protected static $except = ['auth'];
	protected static $protect = [];
	protected static $login_page = 'account/login';
	public $test = "PBK";

	function __construct()
	{
		# code...
	}
	
	function __invoke(){
		return false;
	}
	public static function init()
	{
	    if (self::$CI === null) 
	    {
	      self::$CI = &get_instance();
			}
			self::$except = array_merge(self::$except, self::$CI->config->item('auth_except'));
	}

	public static function authCheck($params=null)
	{
		self::init();
		$called_controller = strtolower(self::$CI->router->class);
		$called_method = strtolower(self::$CI->router->method); 
		#printr($called_controller.' '.$called_method);
		/* printr($called_controller);
		printr(self::$except);
		die(); */
		#var_dump(self::$except);die();

		if($params)
		{
			if(isset($params['login_page']))
			{
				self::$login_page = $params['login_page'];
			}
			if(isset($params['unprotected']))
			{
				self::unprotected($params['unprotected']);
			}
			if(isset($params['protect']))
			{
				self::protect($params['protect']);
			}
		}

		if (
				!self::$CI->ion_auth->logged_in() && 
				!(
					in_array($called_controller, self::$except) || 
					in_array($called_controller.'/'.$called_method, self::$except)
				) 
			) 
		{
			redirect(base_url().self::$login_page);
		}
		
		if (
			!self::$CI->ion_auth->logged_in() && 
			(
				in_array($called_controller, self::$protect) || 
				in_array($called_controller.'/'.$called_method, self::$protect)
			) 
		) 
		{
			redirect(base_url().self::$login_page);
		}
	}

	public static function isLoggedIn()
	{
		self::init();
		if(!self::$CI->ion_auth->logged_in())
		{
			redirect(base_url().self::$login_page);
		}
	}

	public static function unprotected($methods=null)
	{
		if(!empty($methods))
		{
			foreach($methods as $method)
			{
				self::$except[] = strtolower($method);
			}
			
		}
		#printr(self::$except); die();
	}

	public static function protect($action)
	{
		self::$protect = $action;
	}

	public static function minAuthLvl($group)
	{	
		self::init();
		if (!self::$CI->ion_auth->in_group($group))
		{
			redirect(base_url().'home');
		};
	}

	public static function checkAuthLvl($group)
	{	
		self::init();
		if (self::$CI->ion_auth->in_group($group))
		{
			return true;
		}
		return false;
	}
	/**
	 * Creates csrf token and sets the session
	 * @return string 
	 */
	public static function csrf()
	{
		$csrf_token = bin2hex(openssl_random_pseudo_bytes(16));
		$_SESSION['csrf_token'] = $csrf_token;
		//$_SESSION['array'] = " ";
		//$_SESSION['array'] .= " ".$csrf_token;
		return $csrf_token;
	}

	/**
	 * Checking for csrf token and removin session if true
	 * @param  String  $value csrf token code
	 * @return boolean     
	 */
	public static function csrfCheck(string $value)
	{
		if (!isset($_SESSION['csrf_token']) && empty($_SESSION['csrf_token'])) {
			return false;
		}
		if ($_SESSION['csrf_token'] != $value) {
			return false;
		}

		unset($_SESSION['csrf_token']);
		return true;
	}

	private static function back()
	{
		die('<script>
					window.history.back(); 
					alter("Teken manipulation");
				</script>');
	}

	public static function csrfReset($value='')
	{
		if (isset($_SESSION['csrf_token']) && !empty($_SESSION['csrf_token'])) {
			unset($_SESSION['csrf_token']);
		}
	}

	private static function crypto_rand_secure($min, $max)
		{
	    $range = $max - $min;
	    if ($range < 1) return $min; // not so random...
	    $log = ceil(log($range, 2));
	    $bytes = (int) ($log / 8) + 1; // length in bytes
	    $bits = (int) $log + 1; // length in bits
	    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
	    do {
	        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
	        $rnd = $rnd & $filter; // discard irrelevant bits
	    } while ($rnd > $range);
	    return $min + $rnd;
		}

	public static function tokenCode($length=32, $time=false, $mark='')
	{	
			
	    $token = "";
	    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
	    $codeAlphabet.= "01234567890123456789";
	    $max = strlen($codeAlphabet); // edited

	    for ($i=0; $i < $length; $i++) {
	        $token .= $codeAlphabet[self::crypto_rand_secure(0, $max-1)];
		}
		$token .= $mark;
		if($time)
		{
			return md5(time()).$token;
		} return $token;
	}
}

# Hashtag (#) u src atributu img taga salje dupli zahtev koji overajduje csrf token
 ?>