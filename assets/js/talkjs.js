(function(t, a, l, k, j, s) {
    s = a.createElement('script');
    s.async = 1;
    s.src = "https://cdn.talkjs.com/talk.js";
    a.head.appendChild(s);
    k = t.Promise;
    t.Talk = {
        v: 1,
        ready: {
            then: function(f) {
                if (k) return new k(function(r, e) { l.push([f, r, e]) });
                l
                    .push([f])
            },
            catch: function() { return k && new k() },
            c: l
        }
    };
})(window, document, []);

function initChat(callback) {
    Talk.ready.then(function() {
        AJAX.post({
            url: 'ajax/getUserChatSession',
            /* success: function () {
            self.block = false;
            } */
        }).then(function(obj) {
            if (obj !== '') {
                window.me = new Talk.User({
                    "id": obj.id,
                    "name": obj.name,
                    "welcomeMessage": "Hey, let's have a chat!",
                    "configuration": obj.configuration
                })
                window.talkSession = new Talk.Session({
                    //appId: "tYPVcgP5",
                    appId: "t90nvds4",
                    me: me
                });
                //Proverti da li se ovaj event veze vise puta
                window.talkSession.unreads.on('change', function(conversationsIds) {
                    var msg_cnt = document.getElementById("counter_messages");
                    if (msg_cnt) //Samo ulogovani korisnici trenutno vide broj poruka
                    {
                        msg_cnt.innerHTML = conversationsIds.length;
                    }
                    var id = conversationsIds[0].lastMessage.sender.id;
                    var name = conversationsIds[0].lastMessage.sender.name;
                    chat(id, name);
                });
                if(callback) callbakc();
            }
        });

    });

}

function chat(id, name, e) {
    if (typeof window.me == 'undefined' || window.me == null) {
        $("#chatSessionModal").data('other',{id: id, name: name});
        $("#chatSessionModal").modal();
    } else {
        var other = new Talk.User({
            id: id,
            name: name,
            configuration: me.configuration
        });

        var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(window.me, other));
        conversation.setParticipant(window.me);
        conversation.setParticipant(other);
        var popup = talkSession.createPopup(conversation, { keepOpen: false });
        popup.mount({ show: false });
        popup.show();
    }
};

function createChatSession() {
    var errors = '';
    var modal = document.getElementById('chatSessionModal');
    var error_msg = modal.querySelector('#error_msg');
    var guest_username = modal.querySelector('#guest_username');
    var age = modal.querySelector('#age');

    if (guest_username.value == '') {
        errors += '<div>Username field is empty.</div>';
    }
    if (!age.checked) {
        errors += '<div>Confirm your age.</div>';
    }
    if (errors != '') {
        error_msg.innerHTML = errors;
        error_msg.style.display = 'block';
    } else {
        error_msg.innerHTML = '';
        error_msg.style.display = 'none';
        AJAX.post({
                url: 'ajax/setGuestChatSession',
                data: {
                    username: guest_username.value
                }
            })
            .then(function(obj) {
                $("#chatSessionModal").modal('toggle');
                var other = $("#chatSessionModal").data('other');console.log(other);
                initChat();
                //setTimeout(function(){chat(other.id, other.name, null)}, 300);
            })
    }

}

window.addEventListener('load', function() {
    initChat();
});