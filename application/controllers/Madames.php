<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Lib\Security;
use Lib\Alert;
/**
 * Kontroler za sistemske akcije
 */
class Madames extends MY_Controller 
{
    public function handleForm($_action='',$_model='',$_model_id='',$_redirect='')
    {        
        if($this->input->is_ajax_request())
        {
            $data = json_decode($this->input->post('model_data'), true);         
        }
        else
        {
            $data = $this->input->post();
        }

        $action   = !empty($data['_action'])   ? $data['_action']              : $_action;
        $model    = !empty($data['_model'])    ? $this->model($data['_model']) : $this->model($_model);
        $redirect = !empty($data['_redirect']) ? $data['_redirect']            : $_redirect;       
        unset($data['_action']);
        unset($data['_model']);
        unset($data['_redirect']);
        if(strpos($redirect,'.') !== false)
        {
            $redirect = str_replace('.', '/', $redirect);
        }
        if($action=='save')
        {
            $result = $model->save($data);
            if($result)
            {
                //if($this->input->is_ajax_request())echo $result;
                if($redirect) redirect($redirect);
                return $result;
            }           
        }
        else if ($action=='update')
        {
            $id = !empty($data['id']) ? $data['id'] : $_model_id;
            $user_id = !empty($data['user_id']) ? $data['user_id'] : null;
            unset($data['id']);
            unset($data['user_id']);
            //if($this->USER && ($this->USER->id == $id || $this->USER->id == $user_id))
            //{
                $model->update($id, $data);
            //}else die("ERROR");        
            if($redirect) redirect($redirect);
        }
        else if ($action=='update_or_insert')
        {
            $id = !empty($data['id']) ? $data['id'] : $_model_id;
            $user_id = !empty($data['user_id']) ? $data['user_id'] : null;
            unset($data['id']);
            
            $user_settings = $model->findByAttributes(['user_id' => $user_id]);
            if(empty($user_settings))
            {
                $model->save($data);
            }
            else 
            {
                unset($data['user_id']);
                $model->update(['user_id' => $user_id], $data);
            }
            //var_dump($user_settings);die();
            //if($this->USER && ($this->USER->id == $id || $this->USER->id == $user_id))
            //{
                
            //}else die("ERROR");        
            if($redirect) redirect($redirect);
        }
        else if ($action=='delete')
        {
            $id = !empty($data['id']) ? $data['id'] : $_model_id;
            //$user_id = !empty($data['user_id']) ? $data['user_id'] : null;
            //if($this->USER && ($this->USER->id == $id || $this->USER->id == $user_id))
            //{
                $model->delete($id);
            //}
            if($redirect) redirect(base_url().$redirect);
        }
        else 
        {
            Alert::setError("Nepoznata akcija za upravljanje forme");
            redirect(base_url().'home');
        }
    }
}


?>