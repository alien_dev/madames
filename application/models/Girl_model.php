<?php

class Girl_model extends MY_Model {

    public $table_name = "users";
    public $num_rows = 0;

    public function table($value = '') {
        $this->db->select("
			users.id as id, users.username, users.age, users.height, users.weight, users.region_id,
			users.slug, users.phone, users.ts, address, web, club, profile_photo, users.created_on, users.status,
			regions.name as city, hairs.name as hair, types.name as type,work_time_mon_fri.start as monfri_start,work_time_mon_fri.end as monfri_end,
      work_time_weekend.start as weekend_start,work_time_weekend.end as weekend_end, users.description, chat_status, orientation.name as orientation,
      servicesfor.name as servicefor,users_to_services.status as service_status
		");
        $this->db->from($this->table_name);
        $this->db->join('regions', 'regions.id = users.region_id');
        $this->db->join('users_to_services', 'users.id = users_to_services.user_id', 'LEFT');
        $this->db->join('services', 'users_to_services.service_id = services.id', 'LEFT');
        $this->db->join('hairs', 'hairs.id = users.hair_id', 'LEFT');
        $this->db->join('types', 'types.id = users.type_id', 'LEFT');
        $this->db->join('work_time_mon_fri', 'users.id = work_time_mon_fri.user_id', 'LEFT');
        $this->db->join('work_time_weekend', 'users.id = work_time_weekend.user_id', 'LEFT');
        $this->db->join('orientation', 'orientation.id = users.orientation_id', 'LEFT');
        $this->db->join('servicesfor', 'servicesfor.id = users.servicefor_id', 'LEFT');
        $this->db->group_by("users.id"); //Druga alternativa za duplikate koja omogucava koriscenje agregatnih funkcija
    }

    public function userSettings($id) {
        $user_settings = $this->getModel('UserSettings');
        return $user_settings->findByAttributes(['user_id' => $id]);
    }

    public function get_girls() {
        $group_id = 2;
        $this->db->join('regions', 'regions.id = users.region_id');
        $query = $this->db->where('group_id', $group_id)->get('users');
        return $query->result_array();
    }

    public function get_girls_paginate($take, $start) {
        $group_id = 2;
        $this->db->limit((int) $take, (int) $start);
        $this->db->join('regions', 'regions.id = users.region_id');
        $this->db->where('group_id', $group_id);
        $this->db->from('users');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function total_girls($value = '') {
        $group_id = 2;
        $this->db->join('regions', 'regions.id = users.region_id');
        return $this->db->where('group_id', $group_id)->get('users')->num_rows();
    }

    public function get_new_girls() {
        $group_id = 2;
        $this->db->join('regions', 'regions.id = users.region_id');
        $this->db->where('group_id', $group_id);
        $this->db->from('users');
        $this->db->order_by("created_on", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_new_girls_paginate($take, $start) {
        $group_id = 2;
        $this->db->limit((int) $take, (int) $start);
        $this->db->join('regions', 'regions.id = users.region_id');
        $this->db->where('group_id', $group_id);
        $this->db->from('users');
        $this->db->order_by("created_on", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_girl_by_slug($slug) {
        $this->table();
        $query = $this->db->where('users.slug', $slug)->get();
        return $query->row();
    }

    public function deactivate_girl($slug, $password) {

        $query = $this->db->select('slug , password')
                ->where('slug', $slug)
                ->limit(1)
                ->get($this->table());

        if ($query->num_rows() !== 1) {
            $this->ion_auth->trigger_events(array('deactivate_profile', 'deactivate_profile_unsuccessful'));
            $this->ion_auth->set_error('deactivate profile unsuccessful');
            return FALSE;
        }

        $user = $query->row();

        $old_password_matches = $this->ion_auth->hash_password_db($user->id, $password);

        if ($old_password_matches === TRUE) {
            $this->db->delete('users', array('slug' => $slug));
            $this->ion_auth->trigger_events(array('deactivate_profile', 'deactivate_profile_successful'));
            $this->ion_auth->set_message('profile_deactivate_successful');
            return TRUE;
        } else {
            $this->ion_auth->trigger_events(array('deactivate_profile', 'deactivate_profile_unsuccessful'));
            $this->ion_auth->set_error('deactivate profile unsuccessful');
            return FALSE;
        }
    }

    public function get_logged_girl($user_id) {
        $query = $this->db->where('id', $user_id)->get('users');
        return $query->row();
    }

    public function register($profile_photo) {
        $slug = url_title($this->input->post('username'));
        $slug = trim($slug);
        $slug = str_replace(" ", "-", $slug);
        $data = array(
            'username' => $this->input->post('username'),
            'slug' => $slug,
            'region_id' => $this->input->post('region_id'),
            'TS' => $this->input->post('ts'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'profile_photo' => $profile_photo
        );
        return $this->db->insert('users', $data);
    }

    public function login($username, $password) {
        //validate
        $this->db->where('username', $username);
        $this->db->where('password', $password);

        $result = $this->db->get('users');
        if ($result->num_rows() == 1) {
            return $result->row(0)->user_id;
        } else {
            return false;
        }
    }

    public function update_status($id, $status) {
        $data = [
            'status' => $status
        ];
        $this->update($id, $data);
    }

    public function get_regions() {
        $this->db->order_by('name');
        $query = $this->db->get('regions');
        return $query->result_array();
    }

    public function get_types() {
        $query = $this->db->get('types');
        return $query->result_array();
    }

    public function get_hairs() {
        $query = $this->db->get('hairs');
        return $query->result_array();
    }

    public function get_girls_by_region($region_id, $take, $start) {
        $this->db->limit((int) $take, (int) $start);
        // $this->db->order_by('users.user_id','DESC');
        $this->db->join('regions', 'regions.id = users.region_id');
        $query = $this->db->get_where('users', array('users.region_id' => $region_id));
        return $query->result_array();
    }

    public function total_girls_by_region($region_id) {
        $this->db->join('regions', 'regions.id = users.region_id');
        $query = $this->db->get_where('users', array('users.region_id' => $region_id));
        return $query->num_rows();
    }

    public function get_new_girls_by_region($region_id, $take, $start) {
        $this->db->limit((int) $take, (int) $start);
        $this->db->order_by('users.id', 'DESC');
        $this->db->join('regions', 'regions.id = users.region_id');
        $query = $this->db->get_where('users', array('users.region_id' => $region_id));
        return $query->result_array();
    }

    public function get_region($id) {
        $query = $this->db->get_where('regions', array('id' => $id));
        return $query->row();
    }

    public function check_username_exists($username) {
        $query = $this->db->get_where('users', array('username' => $username));
        if (empty($query->row_array())) {
            return true;
        } else {
            return false;
        }
    }

    public function check_email_exists($email) {
        $query = $this->db->get_where('users', array('email' => $email));
        if (empty($query->row_array())) {
            return true;
        } else {
            return false;
        }
    }

    public function update_bio() {
        $data = [
            'region_id' => $this->input->post('region_id'),
            'username' => $this->input->post('username'),
            'age' => $this->input->post('age'),
            'height' => $this->input->post('height'),
            'weight' => $this->input->post('weight'),
            'web' => $this->input->post('web'),
            'orientation_id' => $this->input->post('orientation'),
            'servicefor_id' => $this->input->post('servicefor'),
            'hair_id' => $this->input->post('hair'),
            'type_id' => $this->input->post('type')
        ];
        $this->update($this->input->post('id'), $data);

        $this->db->where('user_id', $this->input->post('id'));
        $this->db->delete('work_time_weekend');

        $weekend_data = [
                    'start' => $this->input->post('weekend_start'),
                    'end' => $this->input->post('weekend_end'),
                    'user_id' => $this->input->post('id'),
        ];
        $this->db->insert('work_time_weekend', $weekend_data);

        $this->db->where('user_id', $this->input->post('id'));
        $this->db->delete('work_time_mon_fri');
        $monfri_data = [
                    'start' => $this->input->post('monfri_start'),
                    'end' => $this->input->post('monfri_end'),
                    'user_id' => $this->input->post('id'),
        ];
        $this->db->insert('work_time_mon_fri', $monfri_data);
    }

    public function get_working_time_hours() {
        $query = $this->db->get('counter_working_hours');
        return $query->result_array();
    }

    public function update_contact() {
        $data = [
            'address' => $this->input->post('address'),
            'web' => $this->input->post('web'),
            'club' => $this->input->post('club')
        ];
        return $this->update($this->input->post('id'), $data);
    }

    public function get_services_for_girl($slug) {
        $this->db->join('users', 'users.id = users_to_services.user_id');
        $this->db->join('services', 'services.id = users_to_services.service_id');
        $query = $this->db->get_where('users_to_services', array('user_slug' => $slug));
        return $query->result_array();
    }

    public function get_services() {
        $query = $this->db->get('services');
        return $query->result_array();
    }

    public function get_servicesfor() {
        $query = $this->db->get('servicesfor');
        return $query->result_array();
    }

    public function get_orientation() {
        $query = $this->db->get('orientation');
        return $query->result_array();
    }

    public function get_first_twelve_users_to_services($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->limit(12, 0);
        return $this->db->get('users_to_services')->result_array();
    }

    public function get_users_to_services($user_id) {
        $this->db->where('user_id', $user_id);
        return $this->db->get('users_to_services')->result_array();
    }

    public function get_second_twelve_users_to_services($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->limit(12, 12);
        return $this->db->get('users_to_services')->result_array();
    }

    public function update_service($slug) {
        $service_ids = $this->input->post('service_id');
        $user_id = $this->input->post('id');
        $status = $this->input->post('service_status');
        $message_data = [];

        $db_services = $this->get_services();
        if (!empty($service_ids)) {
            foreach ($service_ids as $service_id) {
                $data = [
                    'user_id' => $user_id,
                    'service_id' => $service_id,
                    'user_slug' => $slug,
                ];
                $message_data[] = $data;
            }
            $this->db->where('user_id', $user_id);
            $this->db->delete('users_to_services');

            return $this->db->insert_Batch('users_to_services', $message_data);
        }
    }

    public function search_girls($search_params, $take = null, $start = null) {
        unset($search_params['search']);
        #printr($search_params);
        $where = [];
        if (isset($search_params['services_id']) && !empty($search_params['services_id'])) {
            $where[] = ' services.id IN (' . implode(',', $search_params['services_id']) . ') ';
        }
        if (isset($search_params['age']) && !empty($search_params['age'])) {
            if(strpos($search_params['age'], '-') !== false)
            {
                $age = explode('-', $search_params['age']);
                $where[] = " users.age BETWEEN '" . $age[0] . "' AND '" . $age[1] . "'";
            }
            else
            {
                $where[] = " users.age >= " . $search_params['age'];
            }
            
        }
        if (isset($search_params['type']) && !empty($search_params['type'])) {
            $where[] = ' users.type_id IN ("' . implode('","', $search_params['type']) . '")';
        }
        if (isset($search_params['ts'])) {
            $where[] = ' users.ts = ' . $search_params['ts'];
        }
        if (isset($search_params['hairs']) && !empty($search_params['hairs'])) {
            $where[] = ' users.hair_id IN ("' . implode('","', $search_params['hairs']) . '")';
        }
        if (isset($search_params['region']) && !empty($search_params['region'])) {
            $where[] = ' regions.id = ' . $search_params['region'];
        }
        if (isset($search_params['order']) && !empty($search_params['order'])) {
            foreach($search_params['order'] as $filed_name => $value)
            {
                $this->db->order_by("users.$filed_name", $value);
            }
        }
        if (isset($search_params['order_age']) && !empty($search_params['order_age'])) {
            $this->db->order_by("users.age", $search_params['order_age']);
        }
        if (isset($search_params['order_username']) && !empty($search_params['order_username'])) {
            $this->db->order_by("users.username", $search_params['order_username']);
        }
        if (isset($search_params['scope']) && !empty($search_params['scope'])) {
            $where[] = $search_params['scope'];
        }
        $this->table();
        $where = implode(" AND ", $where);
        //Ako je prosledjen parametar za pretragu po username, onda se gaze svi ostali parametri
        if (!empty($search_params['username'])) {
            $where = " users.username LIKE '%{$search_params['username']}%' ";
        }
        #$this->db->distinct("users.id");//Opcioni parametar. // Zbog order by prijavljuje gresku, zato group by koristiti
        $this->db->join('users_groups', 'users.id = users_groups.user_id');
        $this->db->join('groups', 'groups.id = users_groups.group_id');
        if (!empty($where))
            $this->db->where($where);
        if ($take != null && $start != null)
            $this->db->limit((int) $take, (int) $start); //Pre limita treba paginaciji vratiti ukupan broj rezultat

        $this->db->order_by('users.id', 'DESC');
        return $this->db->get()->result_array();
    }

    public function update_price() {
        $data = array(
            #'slug' => $this->input->post('username'),
            'minutes' => $this->input->post('minutes'),
            'hours' => $this->input->post('hours'),
            'days' => $this->input->post('days'),
            'price' => $this->input->post('price'),
        );
        $this->db->where('price_id', $this->input->post('price_id'));
        $user_price = $this->db->get('prices')->num_rows();

        if ($user_price > 0) {
            $this->db->where('price_id', $this->input->post('price_id'));
            return $this->db->update('prices', $data);
        } else {
            #error record not found
        }
    }

    public function get_user_images($slug) {
        $this->load->helper('directory');
        $images_path = 'assets/images/' . $slug;
        $user_directory = directory_map('./' . $images_path);
        if (!empty($user_directory)) {
            return $user_directory;
        }
        return null;
    }

    public function get_user_two_images($slug) {
        $all_images = $this->get_user_images($slug);
        $first_two_images = [];
        if (!empty($all_images)) {
            $first_two_images = array_slice($all_images, 0, 2);
            foreach ($first_two_images as $image) {
                if ($image === "next.txt") {
                    continue;
                }
                return $first_two_images;
            }
        }
    }
    public function get_video($slug){
      $this->load->helper('directory');
      $video = 'assets/videos/' . $slug . '.mp4';
      if(file_exists($video)){
      $video_cut = $slug . '.mp4';
      if (file_exists($video)) {
          return $video_cut;
      }
      return false;
    }else{
      $video = 'assets/videos/' . $slug . '.mov';
      $video_cut = $slug . '.mov';
      if (file_exists($video)) {
          return $video_cut;
      }
      return false;
    }
    }

    public function get_user_profile_image($slug) {
        $images_path = 'assets/images/girls/';
        $default_image = 'assets/images/default.jpg';
        $image_types = explode("|", $this->config->item('allowed_image_upload_types'));
        $girl = $this->get_girl_by_slug($slug);
        foreach ($image_types as $type) {
            $image = $images_path . $slug . '.' . $type;
            if (file_exists($image)) {
                return $image;
            }
        }
        return $default_image;
    }

    public function registerRules($passwords = TRUE) {
        $rules = [
            [
                'field' => 'username',
                'label' => $this->lang->line('Username'),
                'rules' => 'required|char_plus|max_length[16]|min_length[4]',
            /* 'errors'=> [
              'required' => $this->lang->line('form_validation_required'),
              'max_length' => $this->lang->line('form_validation_max_length'),
              'min_length' => $this->lang->line('form_validation_min_length'),
              'char_plus' => $this->lang->line('form_validation_char_plus'),
              #'is_unique' => $this->lang->line('form_validation_is_unique')
              ] */ # Genericke poruke nisu obavezne da se navode u errors
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|max_length[64]|valid_email|is_unique[users.email]',
            ],
            [
                'field' => 'password',
                'label' => $this->lang->line('create_user_validation_password_label'),
                'rules' => 'required|char_plus|max_length[64]|min_length[4]|matches[password_confirm]',
            ],
            [
                'field' => 'password_confirm',
                'label' => $this->lang->line('create_user_validation_password_confirm_label'),
                'rules' => 'required|char_plus|max_length[64]',
            ],
            [
                'field' => 'age',
                'label' => 'Age',
                'rules' => 'required|numeric|greater_than_equal_to[18]',
                'errors' => [
                    'greater_than_equal_to' => 'You must be at least {param} years old'
                ]
            ],
            [
                'field' => 'ts',
                'label' => $this->lang->line('Ts'),
                'rules' => 'required|numeric|max_length[1]',
            ],
            [
                'field' => 'region_id',
                'label' => $this->lang->line('Region'),
                'rules' => 'required|numeric|max_length[9]',
            ],
            [
                'field' => 'cropped_b64',
                'label' => 'Cropped image',
                'rules' => 'max_length[1000000]', //Dodata karatkere dozvoljene u b64
            ],
            [
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'required|max_length[64]|numeric|is_unique[users.phone]',
            ],
        ];

        return $rules;
    }

}

?>
