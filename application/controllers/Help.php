<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contact
 */
class Help extends MY_Controller
{
	public $tab_title = "Madames | Help";
	public function __construct()
    {
        parent::__construct();

    }

	public function index()
	{
        $this->render('index');
	}
}
