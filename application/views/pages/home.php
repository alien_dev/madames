
    <!-- Page Content -->
    <div class="container container-holder">

      <div class="row">

        <div class="col-lg-3 aside-search">


          <div class="list-group">
              <h1 class="my-4">City/Regions</h1>
            <?php foreach($regions as $region) : ?>
            <a class="list-group-item" href="<?php echo site_url('/girls/regions/'.$region['region_id']); ?>"><img class="sidebar_img" src="<?php echo base_url(); ?>assets/images/LokacijaM.png" alt=""><?php echo $region['name']; ?></a>
            <?php endforeach; ?>
          </div>
          <div class="aside-search sidebar_text">


            <div class="list-group-text">
                <h1 class="my-4">Be in our team</h1>
               <p class="sidebar_p"> To be in our team and make profile is for free, but if you want to make your profile online check in Price tab. </p>
            </div>

          </div>
        </div>

        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

          <div class="row top-filters filters_add">
            <div id="filter-panel" class="collapse filter-panel">
           <div class="panel panel-default">
               <div class="panel-body">
                   <form class="form-inline" role="form" action="<?=base_url();?>pages/view" method="POST">

                              <div class="row filter_search_ad">

                                <div class="col-lg-3 col-md-6 mb-3">
                                <p>TS:
                                  <select name="ts" class='form-select'>
                                  <option value="" selected>any</option>
                                  <option value="1">Yes</option>
                                  <option value="2">No</option>
                                </select></p>
                              </div>

                                    <div class="col-lg-3 col-md-6 mb-3">
                                      <p>Services:
                                      <select name="service_id" class='form-select'>
                                        <option value="" selected>any</option>
                                        <?php foreach ($services as $s): ?>
                                          <option value="<?=$s['service_id'];?>"><?=$s['service_name'];?>
                                        <?php endforeach ?>
                                      </select></p>
                                  </div>

                                  <div class="col-lg-3 col-md-6 mb-3">
                                  <p>Age:
                                  <select name="age" class='form-select'>
                                    <option value="" selected>any</option>
                                    <option value="18-25">18-25</option>
                                    <option value="26-35">26-35</option>
                                    <option value="36-45">36-45</option>
                                    <option value="45-55">45-55</option>
                                    <option value="55+">55+</option>
                                  </select></p>
                                </div>

                                <div class="col-lg-3 col-md-6 mb-3">
                                <p>Hair:
                                <select name="hair_color" class='form-select'>
                                  <option value="" selected>any</option>
                                  <option value="1">Blonde</option>
                                  <option value="2">Red</option>
                                  <option value="3">Brunete</option>
                                </select></p>
                              </div>

                              <div class="col-lg-3 col-md-6 mb-3">
                              <p>Type:
                              <select name="type" class='form-select'>
                                <option value="" selected>any</option>
                                <option value="1">European</option>
                                <option value="2">Latina</option>
                                <option value="3">Ebony</option>
                                <option value="4">Arabian</option>
                                <option value="5">Indian</option>
                              </select></p>
                            </div>

                                  <div class="col-lg-3 col-md-6 mb-3">
                                        <p>Service for:
                                    <select class='form-select'>
                                      <option value="" selected>any</option>
                                      <option value="1">Man</option>
                                      <option value="2">Women</option>
                                      <option value="3">Couple</option>
                                      <option value="4">Gays</option>
                                      <option value="5">Two man</option>
                                    </select></p>

                             </div>
                           </div>
                  <input type="hidden" name="search" value="search">
                  <div class="pull-right">
                   <button type="submit" class="btn btn-primary right_b" data-toggle="collapse" data-target="#filter-panel">
                       <span class="glyphicon glyphicon-cog"></span> Search
                   </button>
                 </div>
                   </form>
               </div>
           </div>
       </div>
       <button type="button" class="btn btn-primary filter_btn bio_button" data-toggle="collapse" data-target="#filter-panel">
           <span class="glyphicon glyphicon-cog"></span> Advanced Search
       </button>
    </div>

          <div class="row">
            <?php foreach($girls as $girl) : ?>
            <div class="col-lg-3 col-md-6 mb-3 girl_card_phone">
              <div class="card home_card">
                <div class="img-holder">
                  <a href="<?php echo site_url('/girls/view_profile/'.$girl['slug']); ?>">
                    <span><?php echo $girl['name']; ?></span>
                    <img class="card-img-top" src="<?php echo site_url('/assets/images/girls/'.$girl['slug']); ?>.jpg" alt="">
                  </a>
                </div>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="<?php echo site_url('/girls/view_profile/'.$girl['slug']); ?>"><?php echo $girl['username']; ?>, <?php echo $girl['age']; ?></a>
                    <img class="dot_girl" src="<?php echo base_url(); ?>assets/images/green.png">
                  </h4>
                </div>
             </div>
            </div>
          <?php endforeach; ?>
          </div>
          <?=$pagination_links?>

          <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
