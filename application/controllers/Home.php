<?php
use Lib\Security;
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Home
 */
class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index($paginate_start=0, $paginate_take=12)
    {
        $data['lives'] = $this->girl_model->findByAttributes(['chat_status'=>1]);
        $this->render('index', $data);
    }

    private function pagination_style()
    {
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';

        $config['first_link'] = 'First Page';
        $config['first_tag_open'] = '<span class="btn btn-default">';
        $config['first_tag_close'] = '</span>';

        $config['last_link'] = 'Last Page';
        $config['last_tag_open'] = '<span class="lastlink">';
        $config['last_tag_close'] = '</span>';

        $config['next_link'] = 'Next Page';
        $config['next_tag_open'] = '<span class="nextlink btn btn-default">';
        $config['next_tag_close'] = '</span>';

        $config['prev_link'] = 'Prev Page';
        $config['prev_tag_open'] = '<span class="prevlink">';
        $config['prev_tag_close'] = '</span>';

        $config['cur_tag_open'] = '<span class="curlink">';
        $config['cur_tag_close'] = '</span>';

        $config['num_tag_open'] = '<span class="numlink">';
        $config['num_tag_close'] = '</span>';

        return $config;
    }

    public function savegirls()
    {
        $faker = Faker\Factory::create();
        /* $this->girl_model->save([
            'username' => 'Madam3',
            'password' => '1122',
            'email' => 'madam3@mail.com',
            'age' => 41,

        ]); */

        /* $this->girl_model->update(12,[
            'password' => '112233'
        ]); */

    }
    public function guest()
    {
      $guest_id = Security::tokenCode(4, true);
      $username = $this->input->post('guest_username');
      $guest_data = array(
         'guest_id' => $guest_id,
         'guest_username' => $username,
         'guest_logged' => true,
     );
     $this->session->set_userdata($guest_data);
    }

}

?>
