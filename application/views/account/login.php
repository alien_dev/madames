<?php use Lib\Validate; ?>

<div class="tab_container_register">
    <input id="tab1" type="radio" name="tabs" checked>
    <label class="label_tab" for="tab1" style="border-top-left-radius:6px;"><img class="price_girl_pic" src="<?php echo base_url(); ?>assets/images/girl.png" alt=""><span>Girls</span></label>

    <input id="tab2" type="radio" name="tabs">
    <label class="label_tab" for="tab2" style="border-top-right-radius:6px;"><img class="price_girl_pic" src="<?php echo base_url(); ?>assets/images/glass.png" alt=""><span>Clubs</span></label>

    <section id="content1" class="tab-content">
      <div class="login-form register_form form-w-r">
        <?php
         echo form_open('account/login', ['id'=>'login']);
         ?>
            <div class="login_div_photo">
            <img class="login_avatar_photo" src="<?php echo base_url(); ?>assets/images/CrniProfil.png">
          </div>
            <h2 class="text-center">Sign In</h2>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Phone or Email" name="username" >
                    <div id="error_username"></div>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" >
                    <div id="error_password"></div>
                </div>

            <div class="form-group login_button">
                <button type="submit" class="btn btn-block aa bor_btn">Log in</button>
            </div>
            <h5 class="h5_login">or</h5>
            <a class="btn btn-block sign_up_button bor_btn" href="<?php echo base_url(); ?>account/register/1">Create account</a>
        <?php echo form_close(); ?>

        <p class="text-center small">You forget a password? <span style="color:black">Click here</span></p>
      </div>
    </section>

    <section id="content2" class="tab-content">
      <div class="login-form register_form form-w-r">
        <?php
         echo form_open('account/login_club', ['id'=>'login_club']);
         ?>
            <div class="login_div_photo">
            <img class="login_avatar_photo" src="<?php echo base_url(); ?>assets/images/CrniProfil.png">
          </div>
            <h2 class="text-center">Sign In</h2>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Phone or Email" name="username" >
                    <div id="error_username"></div>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" >
                    <div id="error_password"></div>
                </div>

            <div class="form-group login_button">
                <button type="submit" class="btn btn-block aa bor_btn">Log in</button>
            </div>
            <h5 class="h5_login">or</h5>
            <a class="btn btn-block sign_up_button bor_btn" id="club_login" href="<?php echo base_url(); ?>account/register/2">Create account</a>
        <?php echo form_close(); ?>

        <p class="text-center small">You forget a password? <span style="color:black">Click here</span></p>
      </div>
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/validate.js"></script>
<script >
    let V = new Validate("login");
    V.setRules({
        username:{
            name: "Username",
            rule: "required|chars|max:64",
        },
        password:{
            name: "Password",
            rule: "required|chars|max:64"
        },
    });
    V.run();

    let V = new Validate("login_club");
    V.setRules({
        username:{
            name: "Username",
            rule: "required|chars|max:64",
        },
        password:{
            name: "Password",
            rule: "required|chars|max:64"
        },
    });
    V.run();

</script>
