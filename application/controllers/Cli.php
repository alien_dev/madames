<?php
class Cli extends Cli_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function migrate()
    {
        $this->load->library('migration');
        if (!$this->migration->latest()) {
            show_error($this->migration->error_string());
        }
    }

    public function fillUsers($total = 100)
    {
        echo "Loading Faker...\n";
        $faker = Faker\Factory::create();
        echo "Loading Models...\n";
        $hairs = $this->model('hair')->all();
        $types = $this->model('type')->all();
        $service = $this->model('service');
        $all_services = $service->all();
        $regions = $this->model('region')->all();
        echo "Inserting girls...\n";
        for ($i = 0; $i < $total; $i++) {
            $random_hair = array_rand($hairs);
            $random_type = array_rand($types);
            $random_region = array_rand($regions);
            $radndo_services = array_rand($all_services, 3);
            $hair_id = $hairs[$random_hair]->id;
            $type_id = $types[$random_type]->id;
            $region_id = $regions[$random_region]->id;

            $username = $faker->username('female');

            $slug = trim($username);
            $slug = str_replace(" ", "-", $slug);

            $girl_id = $this->girl_model->save([
                'ip_address' => "::1",
                'active' => 1,
                'username' => $username,
                'password' => $this->ion_auth_model->hash_password('1122'),
                'email' => $username . '@mail.com',
                'age' => rand(18, 60),
                'created_on' => time(),
                'first_name' => $faker->firstname('female'),
                'first_name' => $faker->lastname('female'),
                'phone' => $faker->phoneNumber,
                'height' => rand(140, 200),
                'weight' => rand(50, 110),
                'hair_id' => $hair_id,
                'type_id' => $type_id,
                'region_id' => $region_id,
                'slug' => $slug,
                'profile_photo' => $faker->imageUrl(180, 220),
                'address' => $faker->address,
            ]);
            foreach ($radndo_services as $rnd_service) {
                $this->db->insert('users_to_services', [
                    'user_id' => $girl_id,
                    'service_id' => $all_services[$rnd_service]->id,
                    'user_slug' => $slug,
                ]);
            }
            echo "Name: {$faker->firstname('female')} {$faker->lastname('female')}\n";
            echo "Username: {$username}\n";
            echo "Password: 1122\n";
            echo "--------------------------------------------\n";
        }
        echo "Girls saved in database.\n";
    }

    public function test($psw = '1122')
    {
        $faker = Faker\Factory::create();
        /* for ($i=0; $i < 10; $i++) {
        echo $faker->phoneNumber." ".$faker->firstname('female')."\n";
        } */
        /* $hairs = $this->model('hair')->all();
        $random_hair = array_rand($hairs);
        printr($hairs[$random_hair]); */

        //echo $faker->imageUrl(180,220);
        echo "Password: \n".$psw."\n";
        echo "Hash: \n";
        echo $this->ion_auth_model->hash_password($psw);
    }
}
