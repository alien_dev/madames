<div class="modal fade" id="modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header header_con">
        <h4 class="modal-title title_con">Delete </h4>
        <button type="button" class="close close_con" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p class="modal-description desc_con"></p>
        <p class="modal-text desc_con">Are you sure?</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-sm btn-danger yes">Yes</a>
        <button type="button" class="btn btn-sm btn-default no" data-dismiss="modal">No</button>
      </div>
    </div>

  </div>
</div>
