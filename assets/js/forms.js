function multiForms(form_id, form_fields_names, additional_fields_names = null, callback) {
    var price_form = document.getElementById(form_id);

    price_form.onsubmit = function(e) {
        e.preventDefault();
        var data = [];
        var form_fields = {};
        var additional_fields = {};
        var _action = this.querySelector('[name="_action"]');
        var _model = this.querySelector('[name="_model"]');
        var _redirect = this.querySelector('[name="_redirect"]');
        var form_rows = this.querySelectorAll('.form_row');

        if (additional_fields_names) {
            for (let field_key in additional_fields_names) {
                let field_name = additional_fields_names[field_key];
                let field = this.querySelector('[name="' + field_name + '"]');
                additional_fields[field_name] = field.value;
            }
        }

        for (let row_key in form_rows) {
            let form_row = form_rows[row_key];
            if (typeof form_row == 'object') {
                for (let field_key in form_fields_names) {
                    let field_name = form_fields_names[field_key];
                    let field = form_row.querySelector('[name="' + field_name + '"]');
                    form_fields[field_name] = field.value;
                }
                Object.assign(form_fields, additional_fields);
                data.push(form_fields);
                form_fields = {};
            }
        }

        console.log(data);

        AJAX.post({
                url: this.action,
                data: {
                    model_data: {
                        _action: _action ? _action.value : '',
                        _model: _model ? _model.value : '',
                        _redirect: _redirect ? _redirect.value : '',
                        multi_forms: data
                    }
                }
            })
            .then(function(res) {
                if(res.status == 'ok'){
                    callback(true);
                }
                else {
                    console.log(res);
                }
            })
    }
}