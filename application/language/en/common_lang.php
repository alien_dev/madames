<?php
/*** Zajedničko ***/
$lang["Categories"] = "CATEGORIES"; 
$lang["Language"] = "LANGUAGE"; 
$lang["Home"] = "Home"; 
$lang["ForBuyers"] = "For Buyers"; 
$lang["ForSellers"] = "For Sellers"; 
$lang["Contact"] = "Contact"; 
$lang["About"] = "About"; 
$lang["Address"] = "Address"; 
$lang["ContactEmail"] = "ContactEmail"; 
$lang["Login"] = "Login"; 
$lang["Registration"] = "REGISTRATION"; 
$lang["Faq"] = "Faq"; 
$lang["TermsAndConditions"] = "Terms and conditions"; 
$lang["Username"] = "Username"; 
$lang["Password"] = "Password"; 
$lang["ConfirmPassword"] = "Confirm Password"; 
$lang["Email"] = "Email"; 
$lang["Ts"] = "TS"; 
$lang["Region"] = "Region";
$lang["Image"] = "Image";

$lang["Jan"] = "January"; 
$lang["Feb"] = "February"; 
$lang["Mar"] = "March"; 
$lang["Apr"] = "April"; 
$lang["May"] = "May"; 
$lang["Jun"] = "June"; 
$lang["Jul"] = "July"; 
$lang["Avg"] = "August"; 
$lang["Sep"] = "September"; 
$lang["Oct"] = "October"; 
$lang["Nov"] = "November"; 
$lang["Dec"] = "December"; 
?>