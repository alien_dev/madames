// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ajax from './components/ajax'
// import App from './App'
// import router from './router'
import listing from './components/listing'
export const EventBus = new Vue(); // Za komunikaciju izmedju komponenti (parent-child)
window.EventBus = EventBus;
Vue.config.productionTip = false
window.AJAX = ajax;
/* eslint-disable no-new */
/* new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
}) */
window._listing = listing;
new Vue({
    el: '#listing',
    components: {
        'listing': listing
    },
    //template: '<h3>Listing</h3>',
    data: {
        message: 'Hello Vue!'
    },
    methods: {},
    //render: h => h(listing), //ovo ce pregaziti ceo html u <div id='listing'></div>
    //render: Vue.compile('<div><span>{{ msg }}</span></div>').render
})
//var result = Vue.compile(html_string).render;
